<?php

use CSGOADVANCE\core\Environment;
use CSGOADVANCE\core\Providers;
use CSGOADVANCE\core\Routes;

ob_start('ob_gzhandler');

session_start();

require_once 'vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false
    ],
];

define('USER_INVENTORY_CACHE_DIR', dirname(__FILE__) . '/cache/inventory/');
define('MESSAGES_CACHE_DIR', dirname(__FILE__) . '/cache/messages/');
define('UPGRADES_CACHE_DIR', dirname(__FILE__) . '/cache/upgrades/');
define('USER_BUFFER_WITHDRAWS_DIR', dirname(__FILE__) . '/bot/buffers/withdraw/');
define('USER_BUFFER_DEPOSIT_DIR', dirname(__FILE__) . '/bot/buffers/deposit/');
define('MAILS_DIR', dirname(__FILE__) . '/mails/');

$container = new \Slim\Container($configuration);

$application = new Slim\App($container);

$provider = new Providers();
$provider->run($application->getContainer());

$routes = new Routes();
$routes->run($application);

$application->run();

ob_end_flush();
