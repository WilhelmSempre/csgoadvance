<?php

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Providers;
use Slim\App;

/**
 * Class DatabaseManagerTest
 */
class DatabaseManagerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var DatabaseManager
     */
    public $databaseManager;

    /**
     * DatabaseManagerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $application = new App();

        $providers = new Providers();
        $container = $providers->run($application->getContainer());

        $this->databaseManager = $container->get('database');
    }

    /**
     *
     */
    public function testGetConnection()
    {
        if ($this->databaseManager->getManager()) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
