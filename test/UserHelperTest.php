<?php

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\SteamManager;
use CSGOADVANCE\core\Providers;
use CSGOADVANCE\core\TestFixtures;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Helpers\UserHelper;
use Interop\Container\ContainerInterface;
use Slim\App;

/**
 * Class UserHelperTest
 */
class UserHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var DatabaseManager
     */
    public $databaseManager;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Item
     */
    public $item;

    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * @var SteamManager
     */
    public $steamManager;

    /**
     * @var SecurityManager
     */
    public $securityManager;

    /**
     * UserHelperTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $application = new App();

        $providers = new Providers();
        $container = $providers->run($application->getContainer());

        $fixtures = new TestFixtures();
        $fixtures = $fixtures->generate($container);

        $this->securityManager = $container->get('security');
        $this->steamManager = $container->get('steam');
        $this->databaseManager = $container->get('database');

        $this->item = $fixtures['item'];

        $this->container = $container;
    }

    /**
     *
     */
    public function testAddUserAction()
    {
        $userHelper = new UserHelper($this->databaseManager);

        if (!$userHelper->ifUserExists([
            'steamID' => 76561198060996512
        ])) {
            $userSteamData = $this->steamManager->getUserData(76561198060996512);
            $userHelper->addUser($userSteamData);
        }
        $this->assertTrue(true);
    }

    /**
     * @depends testAddUserAction
     */
    public function testUpdateTradeLinkAction()
    {
        $tradeLink = 'https://steamcommunity.com/tradeoffer/new/?partner=67775546&token=xcFccdfk';

        $userHelper = new UserHelper($this->databaseManager);

        /** @var User $user */
        $this->user = $userHelper->getUser([
            'steamID' => 76561198060996512
        ]);

        $userHelper->validateTradelink($tradeLink)->updateTradeLink($this->user, $tradeLink);
        $this->assertTrue(true);
    }

    /**
     *
     */
    public function testIfUserExists()
    {
        $userHelper = new UserHelper($this->databaseManager);

        if ($userHelper->ifUserExists([
            'steamID' => 76561198060996512
        ])) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     * @expectedException Exception
     */
    public function testIfLinkIsNotFromSteam()
    {
        $userHelper = new UserHelper($this->databaseManager);
        $tradeLink = 'https://google.pl';
        $userHelper->validateTradelink($tradeLink);
    }

    /**
     * @expectedException Exception
     */
    public function testIfLinkIsCorrect()
    {
        $userHelper = new UserHelper($this->databaseManager);
        $tradeLink = 'https://steamcommunity.com/tradeoffer/new/?partner=677546&token=xcFcc';
        $userHelper->validateTradelink($tradeLink);
    }

    /**
     * @expectedException Exception
     */
    public function testIfLinkIsEmpty()
    {
        $userHelper = new UserHelper($this->databaseManager);
        $tradeLink = '';
        $userHelper->validateTradelink($tradeLink);
    }

    /**
     *
     */
    public function testIfUserHasTradelink()
    {
        $userHelper = new UserHelper($this->databaseManager);

        /** @var User $user */
        $this->user = $userHelper->getUser([
            'steamID' => 76561198060996512
        ]);

        $result = $userHelper->ifUserHasTradelink($this->user);

        if ($result) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     *
     */
    public function testAddMultiply()
    {
        $userHelper = new UserHelper($this->databaseManager);

        $multiplies = [
            '1.5' => '0.4',
            '2'=> '0.3',
            '4' => '0.2',
            '10' => '0.1',
            '20' => '0.05'
        ];

        /** @var User $user */
        $this->user = $userHelper->getUser([
            'steamID' => 76561198060996512
        ]);

        $multiplyParams = [
            'user' => $this->user,
            'multiplies' => $multiplies
        ];

        $userHelper->addUserMultiply($multiplyParams);
        $this->assertTrue(true);
    }

    /**
     *
     */
    public function testIfUserHasMultiply()
    {
        $userHelper = new UserHelper($this->databaseManager);

        $multiplies = [
            '1.5' => '0.4',
            '2'=> '0.3',
            '4' => '0.2',
            '10' => '0.1',
            '20' => '0.05'
        ];

        /** @var User $user */
        $this->user = $userHelper->getUser([
            'steamID' => 76561198060996512
        ]);

        $multiplyParams = [
            'user' => $this->user,
            'multiplies' => $multiplies
        ];

        $multiplies = $userHelper->ifUserHasMultiply($multiplyParams);

        if (count($multiplies) > 0) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
