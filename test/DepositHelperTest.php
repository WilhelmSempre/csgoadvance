<?php

namespace test;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\core\Providers;
use CSGOADVANCE\core\TestFixtures;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Helpers\DepositHelper;
use Interop\Container\ContainerInterface;
use PHPUnit_Framework_TestCase;
use Slim\App;

/**
 * Class DepositHelperTest
 * @package test
 */
class DepositHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var User
     */
    public $user;

    /**
     * @var Item
     */
    public $item;

    /**
     * @var SecurityManager
     */
    public $securityManager;

    /**
     * @var DatabaseManager
     */
    public $databaseManager;

    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * UserHelperTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $application = new App();

        $providers = new Providers();
        $container = $providers->run($application->getContainer());

        $fixtures = new TestFixtures();
        $fixtures = $fixtures->generate($container);

        $this->securityManager = $container->get('security');
        $this->databaseManager = $container->get('database');

        $this->user = $fixtures['user'];
        $this->item = $fixtures['item'];

        $this->container = $container;
    }

    /**
     *
     */
    public function testAddDeposit()
    {
        $depositHelper = new DepositHelper($this->container, $this->databaseManager);

        $userID = $this->user->getId();
        $marketName = $this->item->getMarketName();
        $tradeID = 123456789;

        $token = $userID . $marketName . $tradeID;
        $token = $this->securityManager->generateSecurityToken($token, 'now');

        $depositHelper->addDeposit([
            'user' => $this->user,
            'trade' => $tradeID,
            'item' => $this->item,
            'token' => $token
        ]);

        $this->assertTrue(true);
    }
}
