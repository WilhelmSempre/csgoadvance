<?php

namespace test;

use CSGOADVANCE\core\Managers\SteamManager;
use CSGOADVANCE\core\Providers;
use Interop\Container\ContainerInterface;
use PHPUnit_Framework_TestCase;
use Slim\App;

/**
 * Class SteamManagerTest
 */
class SteamManagerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var SteamManager
     */
    public $steamManager;

    /**
     * SteamManagerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $application = new App();

        $providers = new Providers();
        $container = $providers->run($application->getContainer());

        $this->steamManager = $container->get('steam');
    }

    /**
     *
     */
    public function testGetSteamApi()
    {
        if ($this->steamManager->getApiKey()) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
