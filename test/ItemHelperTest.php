<?php

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\core\Providers;
use CSGOADVANCE\core\TestFixtures;
use CSGOADVANCE\src\Helpers\ItemHelper;
use Slim\App;

/**
 * Class ItemHelperTest
 */
class ItemHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var DatabaseManager
     */
    public $databaseManager;

    /**
     * @var SecurityManager
     */
    public $securityManager;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Item
     */
    public $item;

    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * UserHelperTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $application = new App();

        $providers = new Providers();
        $container = $providers->run($application->getContainer());

        $fixtures = new TestFixtures();
        $fixtures = $fixtures->generate($container);

        $this->securityManager = $container->get('security');
        $this->databaseManager = $container->get('database');

        $this->user = $fixtures['user'];
        $this->item = $fixtures['item'];

        $this->container = $container;
    }

    /**
     *
     */
    public function testAddItem()
    {
        $token = $this->securityManager->generateSecurityToken('Chroma 3 Case', 'now');

        $itemHelper = new ItemHelper($this->databaseManager);

        $itemHelper->addItem([
            'name' => 'Chroma 3 Case',
            'market_name' => 'Chroma 3 Case',
            'icon' => sprintf('http://steamcommunity-a.akamaihd.net/economy/image/%s', '-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXU5A1PIYQNqhpOSV-fRPasw8rsUFJ5KBFZv668FFYynaSdJGhE74y0wNWIw_OlNuvXkDpSuZQmi--SrN-h3gey-Uo6YWmlIoCLMlhplhFFvwI'),
            'description' => '<br>Container Series #141<br> <br>Contains one of the following:<br>Dual Berettas | Ventilators<br>G3SG1 | Orange Crash<br>M249 | Spectre<br>MP9 | Bioleak<br>P2000 | Oceanic<br>Sawed-Off | Fubar<br>SG 553 | Atlas<br>CZ75-Auto | Red Astor<br>Galil AR | Firefight<br>SSG 08 | Ghost Crusader<br>Tec-9 | Re-Entry<br>XM1014 | Black Tie<br>AUG | Fleet Flock<br>P250 | Asiimov<br>UMP-45 | Primal Saber<br>PP-Bizon | Judgement of Anubis<br>M4A1-S | Chantico\'s Fire<br>or an Exceedingly Rare Special Item!<br> <br><br>',
            'price' => 0.03,
            'token' => $token
        ]);

        $this->assertTrue(true);
    }

    /**
     *
     */
    public function testIfItemExists()
    {
        $itemHelper = new ItemHelper($this->databaseManager);

        if ($itemHelper->ifItemExists([
            'market_name' => 'Chroma 3 Case'
        ])) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
