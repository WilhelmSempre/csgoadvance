<?php

use CSGOADVANCE\core\Providers;
use CSGOADVANCE\core\Managers\BotManager;

require_once 'vendor/autoload.php';

$application = new Slim\App();

$providers = new Providers();
$container = $providers->run($application->getContainer());

/** @var BotManager $botManager */
$botManager = $container->get('bot');

$bots = $botManager->getBots();
echo base64_encode(json_encode($bots));