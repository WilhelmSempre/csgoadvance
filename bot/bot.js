/**
 *
 */
module.exports = function () {

	'use strict';

	/** Init variables **/
	var io = [], steam = [], steamUser = [], steamID = [], steamTrade = [], trade = [], client = [], logger = [],
        offer = [], manager = [], steamMobileConfirmation = [], config = [], steamTradeManager = [], steamCommunity = [],
        community = [],
        steamTradeOffers = [], deasync, yaml, fs, winston, configOfSteamFile, connected = [], clientsList = {},
		configOfBotFile, configOfSteam, configOfBot, steamTotp = [], steamGuard = [], events;

	/** Load dependencies **/
	winston = require('winston');
	deasync = require('deasync');
	yaml = require('js-yaml');
	fs = require('fs');
	events = require('events');

	/**
	 *
	 * @constructor
	 */
	var Bot = function () {

        /**
         * @param bot
         * @param index
         * @returns {Bot}
         */
		this.createServer = function (bot) {
            io.push(require('socket.io')(bot.port));
			return this;
		};

        /**
         * @param socket
         * @param bot
         * @returns {Bot}
         */
		this.userEvents = function(socket, bot) {
            (function (socket, bot) {
                socket.on('userLeftGame', function (userSteamID) {
                    if (typeof clientsList[userSteamID] !== 'undefined') {
                        delete clientsList[userSteamID];
                    }

                    socket.leave(userSteamID);
                }).on('userJoinGame', function (userSteamID) {
                    if (typeof clientsList[userSteamID] === 'undefined') {
                        clientsList[userSteamID] = userSteamID;
                    } else {
                        delete clientsList[userSteamID];
                        clientsList[userSteamID] = userSteamID;
                    }

                    socket.join(userSteamID);
                });
            })(socket, bot);

            return this;
        };

        /**
         * @param bots
         * @returns {Bot}
         */
		this.connectSocket = function (bots) {
		    for (var index in bots) {
                this.createServer(bots[index]);

                if (typeof io[index] !== 'undefined') {

                    /** Initialize connection **/
                    logger.info('[BOT] Server ' + bots[index].label + ' connected!');

                    /** Listen user connection **/
                    (function (self, bot) {
                        io[bot].sockets.on('connection', function (socket) {

                            self.userBeginTrade(socket, bots)
                                .loadBotInventoryEvent(socket)
                                .userEvents(socket, bot)
                                .gameEvents(socket, bot)
                                .statisticsEvents(socket, bot)
                                .messengerEvents(socket, bot);
                        }).on('reconnection', function (socket) {

                            self.userBeginTrade(socket, bots)
                                .loadBotInventoryEvent(socket)
                                .userEvents(socket, bot)
                                .gameEvents(socket, bot)
                                .statisticsEvents(socket, bot)
                                .messengerEvents(socket, bot);
                        });
                    })(this, index);

                    this.steamInitialize(bots[index], index);
                }
            }

			return this;
		};

		/**
		 * @param url
		 * @returns {{}}
		 */
		this.parseURLParams = function (url) {
			var queryStart = url.indexOf('?') + 1,
				queryEnd = url.indexOf('#') + 1 || url.length + 1,
				query = url.slice(queryStart, queryEnd - 1),
				pairs = query.replace(/\+/g, ' ').split('&'),
				parms = {}, i, n, v, nv;

			if (query === url || query === '') return;

			for (i = 0; i < pairs.length; i++) {
				nv = pairs[i].split('=', 2);
				n = decodeURIComponent(nv[0]);
				v = decodeURIComponent(nv[1]);

				if (!parms.hasOwnProperty(n)) parms[n] = [];
				parms[n].push(nv.length === 2 ? v : null);
			}
			
			return parms;
		};

        /**
         * @param bot
         * @param index
         * @returns {Bot}
         */
		this.tradeEvents = function (bot, index) {
			logger.info('[BOT] Setting trade events in ' + bot.label + '!');

            (function (bot) {
                /** Manager events **/
                manager[index].on('pollFailure', function (error) {
                    logger.error('Error polling for trade offers in ' + bot.label + ': ' + error);
                }).on('newOffer', function (trade) {
                    logger.warn('[BOT] Incoming trade request from  ' + trade[index].partner.getSteam3RenderedID() + 'in ' + bot.label);

                    /** Decline incoming request **/
                    trade.decline(function (error) {
                        logger.warn('[BOT] Trade declined from ' + trade[index].partner.getSteam3RenderedID() + 'in ' + bot.label);
                    });
                }).on('pollData', function (pollData) {
                    /** Write poll data **/
                    fs.writeFile('bot/polls/' + (parseInt(index) + 1) + '/polldata.json', JSON.stringify(pollData), function() {});
                }).on('sentOfferChanged', function (trade) {
                    var userSteamID = new steamID[index]('[U:1:' + trade.partner.accountid.toString() + ']').getSteamID64().toString();
                    var market_names = [];

                    if (trade.state === 3) {

                        if (trade.itemsToReceive.length > 0) {
                            logger.info('[BOT] Bot transfers to user inventory!');

                            for (var item in trade.itemsToReceive) {
                                market_names.push(trade.itemsToReceive[item].market_name);
                            }

                            if (connected[index]) {
                                if (typeof clientsList[userSteamID] !== 'undefined') {
                                    io[index].sockets.in(userSteamID).emit('initializeTrade', {
                                        trade_id: trade.id,
                                        market_names: market_names
                                    });
                                } else {
                                    fs.writeFile('bot/buffers/deposit/' + userSteamID + '.txt', JSON.stringify({
                                        mode: 'initializeTrade',
                                        trade_id: trade.id,
                                        market_names: market_names,
                                        steam_id: userSteamID,
                                        message: trade.message
                                    }), function(err) {
                                        if (err) {
                                            return logger.error('[BOT] Buffer failed: ' + err);
                                        }

                                        console.log('[BOT] Buffer saved for user ' + userSteamID);
                                    });
                                }
                            }
                        } else if (trade.itemsToGive.length > 0) {
                            logger.info('[BOT] Bot transfers from user withdraw!');

                            for (var item in trade.itemsToGive) {
                                market_names.push(trade.itemsToGive[item].market_name);
                            }

                            if (connected[index]) {
                                if (typeof clientsList[userSteamID] !== 'undefined') {
                                    io[index].sockets.in(userSteamID).emit('initializeWithdraw', {
                                        trade_id: trade.id,
                                        market_names: market_names,
                                        steam_id: userSteamID,
                                        message: trade.message
                                    });
                                } else {
                                    fs.writeFile('bot/buffers/withdraw/' + userSteamID + '.txt', JSON.stringify({
                                        mode: 'initializeWithdraw',
                                        trade_id: trade.id,
                                        market_names: market_names,
                                        steam_id: userSteamID,
                                        message: trade.message
                                    }), function(err) {
                                        if (err) {
                                            return logger.error('[BOT] Buffer failed: ' + err);
                                        }

                                        console.log('[BOT] Buffer saved for user ' + userSteamID);
                                    });
                                }
                            }
                        }

                        logger.info('[BOT] User or bot (' + bot.label  + ')  accepts trade (' + trade.id + ')');
                    } else if (trade.state === 7) {
                        if (trade.itemsToReceive.length > 0) {
                            if (typeof clientsList[userSteamID] !== 'undefined') {
                                io[index].sockets.in(userSteamID).emit('cancelTrade', {
                                    trade_id: trade.id,
                                    message: trade.message
                                });
                            } else {
                                fs.writeFile('bot/buffers/deposit/' + userSteamID + '.txt', JSON.stringify({
                                    mode: 'cancelTrade',
                                    steam_id: userSteamID,
                                    message: trade.message
                                }), function(err) {
                                    if (err) {
                                        return logger.error('[BOT] Buffer failed: ' + err);
                                    }

                                    console.log('[BOT] Buffer saved for user ' + userSteamID);
                                });
                            }
                        } else if (trade.itemsToGive.length > 0) {
                            if (connected[index]) {
                                if (typeof clientsList[userSteamID] !== 'undefined') {
                                    io[index].sockets.in(clientsList[userSteamID]).emit('cancelWithdraw', {
                                        trade_id: trade.id,
                                        message: trade.message
                                    });
                                } else {
                                    fs.writeFile('bot/buffers/withdraw/' + userSteamID + '.txt', JSON.stringify({
                                        mode: 'cancelWithdraw',
                                        steam_id: userSteamID,
                                        message: trade.message
                                    }), function(err) {
                                        if (err) {
                                            return logger.error('[BOT] Buffer failed: ' + err);
                                        }

                                        console.log('[BOT] Buffer saved for user ' + userSteamID);
                                    });
                                }
                            }
                        }

                        logger.info('[BOT] User or bot (' + bot.label  + ') declines trade (' + trade.id + ')');
                    } else if (trade.state === 2) {
                        logger.info('[BOT] User or bot (' + bot.label  + ') activates trade (' + trade.id + ')');
                    }
                }).on('receivedOfferChanged', function (trade) {
                    var userSteamID = new steamID[index]('[U:1:' + trade.partner.accountid.toString() + ']').getSteamID64().toString();
                    var market_names = [];

                    if (trade.state === 3) {
                        if (trade.itemsToGive.length > 0) {
                            logger.info('[BOT] Bot transfers from user withdraw!');

                            for (var item in trade.itemsToGive) {
                                market_names.push(trade.itemsToGive[item].market_name);
                            }

                            if (connected[index]) {
                                if (typeof clientsList[userSteamID] !== 'undefined') {
                                    io[index].sockets.in(userSteamID).emit('initializeWithdraw', {
                                        trade_id: trade.id,
                                        market_names: market_names,
                                        steam_id: userSteamID,
                                        message: trade.message
                                    });
                                } else {
                                    fs.writeFile('bot/buffers/withdraw/' + userSteamID + '.txt', JSON.stringify({
                                        mode: 'initializeWithdraw',
                                        steam_id: userSteamID,
                                        message: trade.message
                                    }), function(err) {
                                        if (err) {
                                            return logger.error('[BOT] Buffer failed: ' + err);
                                        }

                                        console.log('[BOT] Buffer saved for user ' + userSteamID);
                                    });
                                }
                            }
                        }

                        logger.info('[BOT] User or bot accepts trade (' + trade.id + ')');
                    } else if (trade.state === 7) {
                        if (trade.itemsToGive.length > 0) {
                            if (connected[index]) {
                                if (typeof clientsList[userSteamID] !== 'undefined') {
                                    io[index].sockets.in(userSteamID).emit('cancelWithdraw', {
                                        trade_id: trade.id,
                                        message: trade.message
                                    });
                                } else {
                                    fs.writeFile('bot/buffers/withdraw/' + userSteamID + '.txt', JSON.stringify({
                                        mode: 'cancelWithdraw',
                                        steam_id: userSteamID,
                                        message: trade.message
                                    }), function(err) {
                                        if (err) {
                                            return logger.error('[BOT] Buffer failed: ' + err);
                                        }

                                        console.log('[BOT] Buffer saved for user ' + userSteamID);
                                    });
                                }
                            }
                        }

                        logger.info('[BOT] User or bot (' + bot.label  + ') declines trade (' + trade.id + ')');
                    } else if (trade.state === 2) {
                        logger.info('[BOT] User or bot activates trade (' + trade.id + ')');
                    }
                }).getOffers(3, function(error, sentTrade, recivedTrade) {
                    for (var index in sentTrade) {
                        if (sentTrade[index].state === 6) {
                            sentTrade[index].decline(function (error) {
                                if (!error) {
                                    logger.warn('[BOT] Trade declined from ' + sentTrade[index].partner.getSteam3RenderedID() + ' in ' + bot.label);
                                }
                            });
                        }
                    }

                    for (var index in recivedTrade) {
                        if (recivedTrade[index].state === 6) {
                            recivedTrade[index].decline(function (error) {
                                if (!error) {
                                    logger.warn('[BOT] Trade declined from ' + recivedTrade[index].partner.getSteam3RenderedID() + ' in ' + bot.label);
                                }
                            });
                        }
                    }
                });
            })(bot);

			return this;
		};

        /**
         * @param tradeID
         * @param socket
         * @param index
         * @returns boolean
         */
		this.initConfirmation = function (tradeID, socket, index) {
		    var trade = [];

            (function(bot, self) {
                trade[bot] = manager[bot].getOffer(tradeID, function(error, trade) {

                    if (error) {
                        logger.error(error);
                    }

                    /** Initialize confirmation script **/
                    (function (self, trade, bot) {

                        community[bot].acceptConfirmationForObject(configOfBot.bots[bot].config.identity_secret, tradeID, function(err) {
                            if (err) {
                                logger.error('Trade (' + tradeID + ') not confirmed by mobile authenticator in BOT ' + (parseInt(bot) + 1) + '!');
                                socket.emit('botMessage', {
                                    type: 'error',
                                    message: 'Mobile confirmation not accepted!'
                                });

                                /** Decline request **/
                                trade.decline(function (error) {
                                    io[index].sockets.to(userSteamID).emit('cancelTrade');

                                    logger.warn('[BOT] Trade declined for ' + trade.partner.getSteam3RenderedID() + ' in BOT ' + (parseInt(bot) + 1));
                                });
                            } else {
                                logger.info('Trade (' + tradeID + ') confirmed by mobile authenticator! in BOT ' + (parseInt(bot) + 1));

                                socket.emit('botMessage', {
                                    type: 'success',
                                    message: '<a target="_blank" href="https://steamcommunity.com/tradeoffer/' + tradeID + '">Mobile confirmation accepted!. Go to trade now!</a>'
                                });
                            }
                        });
                    })(self, trade, bot);
                });
            })(index, this);
		};

        /**
         * @param socket
         * @param bots
         * @returns {Bot}
         */
		this.userBeginTrade = function (socket, bots) {
			(function (self) {
				var tradeID = [];
                var marketNames = [];

				/** User trade request **/
				socket.on('userTradeRequest', function (data) {
                    var filteredURLData = self.parseURLParams(data.userTradeLink);
                    var assetIDs = [];

                    marketNames = data.market_names;

                    for (var index in connected) {
                        if (connected[index]) {
                            offer[index].loadPartnerInventory({
                                partnerAccountId: filteredURLData.partner[0],
                                appId: 730,
                                contextId: 2,
                                language: 'en'
                            }, function (error, items) {
                                if (error) {

                                } else {
                                    for (var name in marketNames) {
                                        var itemsPush = [];
                                        for (var item in items) {
                                            if (marketNames[name] === items[item].market_name) {
                                                if (itemsPush.indexOf(marketNames[name]) === -1) {
                                                    if (assetIDs.indexOf(items[item].id) === -1) {
                                                        assetIDs.push(items[item].id);
                                                        itemsPush.push(marketNames[name]);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    var countItems = {};

                                    for (var index in connected) {
                                        if (connected[index]) {
                                            (function (index) {
                                                offer[index].loadMyInventory({
                                                    appId: 730,
                                                    contextId: 2,
                                                    language: 'en'
                                                }, function (error, items) {
                                                    if (items) {
                                                        countItems[index] = {
                                                            length: items.length
                                                        };
                                                    } else {
                                                        countItems[index] = {
                                                            length: 0
                                                        };
                                                    }
                                                });
                                            })(index);
                                        }
                                    }

                                    setTimeout(function () {
                                        var countIndex = 0;
                                        var tradeItems = [];
                                        var itemsToSend = [];

                                        for (var index in connected) {
                                            if (connected[index]) {
                                                countItems[countIndex]['index'] = index;
                                            }

                                            countIndex++;
                                        }

                                        for (var assetID in assetIDs) {
                                            var index = 0;

                                            while (index <= countItems[0].length) {
                                                if (connected[index]) {
                                                    if (countItems[index].length < bots[index].limit) {
                                                        if (typeof tradeItems[index] === 'undefined') {
                                                            tradeItems[countItems[index].index] = [];
                                                        }

                                                        tradeItems[countItems[index].index].push({
                                                            'appid': 730,
                                                            'contextid': 2,
                                                            'amount': 1,
                                                            'assetid': assetIDs[assetID]
                                                        });

                                                        itemsToSend.push(assetIDs[assetID]);

                                                        countItems[index].length++;
                                                        break;
                                                    }
                                                }

                                                index++;
                                            }
                                        }

                                        if (tradeItems.length > 0) {
                                            for (var index in tradeItems) {
                                                if (connected[index]) {

                                                    /** Create offer */
                                                    offer[index].makeOffer({
                                                        partnerAccountId: filteredURLData.partner[0],
                                                        accessToken: filteredURLData.token[0],
                                                        itemsFromMe: [],
                                                        itemsFromThem: tradeItems[index],
                                                        message: configOfBot.bots[index].config.trade_message
                                                    }, function (error, response) {
                                                        if (error) {
                                                            logger.error(error);
                                                            socket.emit('botMessage', {
                                                                type: 'error',
                                                                message: 'System cannot create offer. Contact to administrator!'
                                                            });
                                                        } else {

                                                            /** Obtaining trade ID **/
                                                            tradeID = response.tradeofferid;

                                                            /** Accept offer */
                                                            offer[index].acceptOffer({
                                                                tradeOfferId: tradeID,
                                                                partnerSteamId: filteredURLData.partner[0]
                                                            }, function () {
                                                                if (itemsToSend.length === assetIDs.length) {
                                                                    socket.emit('botMessage', {
                                                                        type: 'success',
                                                                        message: '<a target="_blank" href="https://steamcommunity.com/tradeoffer/' + tradeID + '">Deposit created! Go to trade now!</a>'
                                                                    });
                                                                } else {
                                                                    socket.emit('botMessage', {
                                                                        type: 'success',
                                                                        message: '<a target="_blank" href="https://steamcommunity.com/tradeoffer/' + tradeID + '">Deposit created, but not all items will be included because of bot limitation! Go to trade now!</a>'
                                                                    });
                                                                }
                                                            });

                                                            logger.info('[BOT] Sending trade (' + tradeID + ') to [U:1:' + filteredURLData.partner[0] + ']');
                                                        }
                                                    });
                                                }
                                            }
                                        } else {
                                            socket.emit('botMessage', {
                                                type: 'error',
                                                message: 'Bot limitation!'
                                            });
                                        }
                                    }, 5000);
                                }
                            });

                            break;
                        }
                    }
                });

                /** User withdraw request **/
                socket.on('userWithdrawRequest', function (data) {
                    var filteredURLData = self.parseURLParams(data.userTradeLink);
                    var tradeItems = [];
                    var assetIDs = [];

                    marketNames = data.market_names;

                    var botInventory = [];

                    for (var index in connected) {
                        if (connected[index]) {
                            (function (bot) {
                                offer[bot].loadMyInventory({
                                    appId: 730,
                                    contextId: 2,
                                    language: 'en'
                                }, function (error, items) {
                                    if (typeof botInventory[bot] === 'undefined') {
                                        botInventory[bot] = [];
                                    }

                                    for (var index in items) {
                                        botInventory[bot].push(items[index]);
                                    }
                                });
                            })(index);
                        }
                    }

                    setTimeout(function () {
                        var itemsPush = [];
                        var items = {};

                        for (var index in marketNames) {
                            items[marketNames[index]] = items[marketNames[index]] ? items[marketNames[index]] + 1 : 1;
                        }

                        for (var name in items) {
                            for (var item in botInventory) {
                                for (var index in botInventory[item]) {
                                    if (name === botInventory[item][index].market_name) {
                                        if (items[botInventory[item][index].market_name] > 0) {
                                            if (assetIDs.indexOf(botInventory[item][index].market_name) === -1) {
                                                if (itemsPush.indexOf(botInventory[item][index].id) === -1) {
                                                    assetIDs.push({
                                                        id: botInventory[item][index].id,
                                                        bot: item
                                                    });
                                                    itemsPush.push(botInventory[item][index].id);
                                                }
                                            }

                                            items[botInventory[item][index].market_name]--;
                                        }
                                    }
                                }
                            }
                        }

                        for (var assetID in assetIDs) {

                            if (typeof tradeItems[assetIDs[assetID].bot] === 'undefined') {
                                tradeItems[assetIDs[assetID].bot] = [];
                            }

                            tradeItems[assetIDs[assetID].bot].push({
                                appid: 730,
                                contextid: 2,
                                amount: 1,
                                assetid: assetIDs[assetID].id
                            });
                        }

                        for (var index in tradeItems) {

                            (function (bot) {

                                /** Create offer */
                                offer[bot].makeOffer({
                                    partnerAccountId: filteredURLData.partner[0],
                                    accessToken: filteredURLData.token[0],
                                    itemsFromMe: tradeItems[bot],
                                    itemsFromThem: [],
                                    message: configOfBot.bots[bot].config.trade_message
                                }, function (error, response) {
                                    if (error) {
                                        logger.error(error);
                                        socket.emit('botMessage', {
                                            type: 'error',
                                            message: 'System cannot create offer. Contact to administrator!'
                                        });
                                    } else {

                                        /** Obtaining trade ID **/
                                        tradeID[bot] = response.tradeofferid;

                                        (function (bot) {

                                            /** Accept offer */
                                            offer[bot].acceptOffer({
                                                tradeOfferId: tradeID[bot],
                                                partnerSteamId: filteredURLData.partner[0]
                                            }, function () {
                                                socket.emit('botMessage', {
                                                    type: 'success',
                                                    message: 'Witdraw offer created!'
                                                });

                                                setTimeout(function () {

                                                    /** Init confirmation because bot requires 2FA Code **/
                                                    self.initConfirmation(tradeID[bot], socket, bot);
                                                }, 5000);

                                                logger.info('[BOT] Sending withdraw trade (' + tradeID[bot] + ') to [U:1:' + filteredURLData.partner[0] +
                                                    '] with request to take items');
                                            });
                                        })(bot);
                                    }
                                });
                            })(index);
                        }
                    }, 3000);
                });

                /** User buy request **/
                socket.on('userBuyRequest', function (data) {
                    var filteredURLData = self.parseURLParams(data.userTradeLink);
                    var tradeItems = [];
                    var assetIDs = [];

                    marketNames = data.market_names;

                    var botInventory = [];

                    for (var index in connected) {
                        if (connected[index]) {
                            (function (bot) {
                                offer[bot].loadMyInventory({
                                    appId: 730,
                                    contextId: 2,
                                    language: 'en'
                                }, function (error, items) {
                                    if (typeof botInventory[bot] === 'undefined') {
                                        botInventory[bot] = [];
                                    }

                                    for (var index in items) {
                                        botInventory[bot].push(items[index]);
                                    }
                                });
                            })(index);
                        }
                    }

                    setTimeout(function () {
                        var itemsPush = [];
                        var items = {};

                        for (var index in marketNames) {
                            items[marketNames[index]] = items[marketNames[index]] ? items[marketNames[index]] + 1 : 1;
                        }

                        for (var name in items) {
                            for (var item in botInventory) {
                                for (var index in botInventory[item]) {
                                    if (name === botInventory[item][index].market_name) {
                                        if (items[botInventory[item][index].market_name] > 0) {
                                            if (assetIDs.indexOf(botInventory[item][index].market_name) === -1) {
                                                if (itemsPush.indexOf(botInventory[item][index].id) === -1) {
                                                    assetIDs.push({
                                                        id: botInventory[item][index].id,
                                                        bot: item
                                                    });
                                                    itemsPush.push(botInventory[item][index].id);
                                                }
                                            }

                                            items[botInventory[item][index].market_name]--;
                                        }
                                    }
                                }
                            }
                        }

                        for (var assetID in assetIDs) {

                            if (typeof tradeItems[assetIDs[assetID].bot] === 'undefined') {
                                tradeItems[assetIDs[assetID].bot] = [];
                            }

                            tradeItems[assetIDs[assetID].bot].push({
                                appid: 730,
                                contextid: 2,
                                amount: 1,
                                assetid: assetIDs[assetID].id
                            });
                        }

                        for (var index in tradeItems) {

                            (function (bot) {
                                /** Create offer */
                                offer[bot].makeOffer({
                                    partnerAccountId: filteredURLData.partner[0],
                                    accessToken: filteredURLData.token[0],
                                    itemsFromMe: tradeItems[bot],
                                    itemsFromThem: [],
                                    message: configOfBot.bots[bot].config.trade_message + ' [AFFILIATES]: ' + filteredURLData.partner[0] + ')'
                                }, function (error, response) {
                                    if (error) {
                                        logger.error(error);
                                        socket.emit('botMessage', {
                                            type: 'error',
                                            message: 'System cannot create offer. Contact to administrator!'
                                        });
                                    } else {

                                        /** Obtaining trade ID **/
                                        tradeID[bot] = response.tradeofferid;

                                        (function (bot) {
                                            /** Accept offer */
                                            offer[bot].acceptOffer({
                                                tradeOfferId: tradeID[bot],
                                                partnerSteamId: filteredURLData.partner[0]
                                            }, function () {
                                                socket.emit('botMessage', {
                                                    type: 'success',
                                                    message: 'Buy trade created!'
                                                });

                                                setTimeout(function () {

                                                    /** Init confirmation because bot requires 2FA Code **/
                                                    self.initConfirmation(tradeID[bot], socket, bot);
                                                }, 5000);

                                                logger.info('[BOT] Sending buy trade (' + tradeID[bot] + ') to [U:1:' + filteredURLData.partner[0] +
                                                    '] with request to take items');
                                            });
                                        })(bot);
                                    }
                                });
                            })(index);
                        }
                    }, 3000);
                });
			})(this);

			return this;
		};

        /**
         * @param socket
         * @returns {Bot}
         */
		this.loadBotInventoryEvent = function (socket) {

			/** Request to load inventory **/
			(function (self) {
				socket.on('loadBotInventoryRequest', function () {
				    var itemsInventory = [];
				    var botItems = [];

				    for (var index in connected) {
				        if (connected[index]) {
				            itemsInventory[index] = [];

                            offer[index].loadMyInventory({
                                appId: 730,
                                contextId: 2,
                                language: 'en'
                            }, function (error, items) {
                                if (error) {

                                } else {
                                    for (var i = 0; i < items.length; i++) {
                                        if (items[i].tradable) {
                                            itemsInventory[index].push(items[i]);
                                        }
                                    }
                                }
                            });
                        }
                    }

                    setTimeout(function () {
                        for (var index in itemsInventory) {
                            for (var i in itemsInventory[index]) {
                                botItems.push(itemsInventory[index][i]);
                            }
                        }

                        socket.emit('itemsFromBotLoaded', {
                            'items': botItems
                        });
                    }, 3000);
				});
			})(this);

			return this;
		};

        /**
         * @param bot
         * @param index
         * @returns {boolean}
         */
		this.steamUserInitialize = function (bot, index) {
			connected[index] = false;

			logger.info('[BOT] Logging ' + bot.label + ' to steam for 5 seconds!');

			client.push(new steamUser[index]());
			trade.push(new steamTrade[index]());
			offer.push(new steamTradeOffers[index]());
			community.push(new steamCommunity[index]());

			try {

				/** Get steam guard **/
				steamGuard.push(steamTotp[index].generateAuthCode(configOfBot.bots[index].config.shared_secret));

				setTimeout(function () {
					client[index].logOn({
						accountName: configOfBot.bots[index].login.username,
						password: configOfBot.bots[index].login.password,
						twoFactorCode: steamGuard[index]
					});
				}, 5000);

				(function (self) {
                    client[index].on('loggedOn', function (details) {
						var steamID = client[index].steamID.getSteam3RenderedID();

						logger.info('[BOT] Logged ' + bot.label + ' with Steam ID: ' + steamID);
                        client[index].setPersona(steamUser[index].EPersonaState.Online);
                        client[index].setNickname(steamID, configOfBot.bots[index].login.nickname);

						logger.info('[BOT] Set name to: ' + configOfBot.bots[index].login.nickname);
                        client[index].gamesPlayed(304930);

                        connected[index] = true;

					}).on('error', function (error) {
						logger.error('[BOT] Cannot log to ' + bot.label + ' steam because of error: ' + error);
                        connected[index] = false;
					}).on('webSession', function (sessionID, cookies) {

						trade[index].sessionID = sessionID;

						cookies.forEach(function (cookie) {
							trade[index].setCookie(cookie.trim());
						});

                        community[index].setCookies(cookies);

						manager.push(new steamTradeManager[index]({
							'steam': client[index],
							'domain': configOfSteam.steam.config.domain_name,
							'pollInterval': 1000,
							'language': 'en'
						}));

						offer[index].setup({
							sessionID: sessionID,
							webCookie: cookies,
							APIKey: configOfSteam.steam.config.BOTAPIKEYS[index].APIKEY
						});

						fs.readFile('bot/polls/' + (parseInt(index) + 1) + '/polldata.json', function (error, data) {
							if (error) {
								logger.warn('[BOT] Error reading polldata.json in ' + bot.label + '. If this is the first run, this is expected behavior and ignore it: ' + error);
							} else {
								logger.debug('[BOT] Found previous trade offer poll data in ' + bot.label + '. Importing it to keep things running smoothly.');
								manager[index].pollData = JSON.parse(data);
							}
						});

						manager[index].setCookies(cookies, function () {
							logger.info('[BOT] Got web session from steamcommunity.com for trading and offering in ' + bot.label + '');
							logger.info('[BOT] Got API key in ' + bot.label + ': ' + manager[index].apiKey);


							setTimeout(function() {
                                self.tradeEvents(bot, index);
                            }, 3000);

							client[index].setPersona(steam[index].EPersonaState.LookingToTrade);
						});

					}).on('accountLimitations', function (limited, communityBanned, locked, canInviteFriends) {
						if (limited) {
							logger.warn('[BOT] Our account is limited in ' + bot.label + '. We cannot send friend invites, use the market, open group chat, or access the web API.');
						}
						if (communityBanned) {
							logger.warn('[BOT] Our account is banned in ' + bot.label + ' from Steam Community');
						}
						if (locked) {
							logger.error('[BOT] Our account is locked in ' + bot.label + '. We cannot trade/gift/purchase items, play on VAC servers, or access Steam Community.  Shutting down.');
							process.exit(1);
						}
						if (!canInviteFriends) {
							logger.warn('[BOT] Our account is unable to send friend requests in ' + bot.label + '.');
						}
					});

				})(this);

			} catch (error) {
				logger.error('[BOT] Error resolved in ' + bot.label + ': ' + error);
                connected[index] = false;
			}

			/** Synchronized event **/
			while (!connected[index]) {
				deasync.runLoopOnce();
			}

			return connected[index];
		};

        /**
         * @param socket
         * @param index
         * @returns {Bot}
         */
		this.gameEvents = function (socket, index) {
			(function (bot) {
				socket.on('gameAdditionRequest', function () {
				    if (connected[index]) {
                        io[bot].sockets.emit('gameAdded');
                    }
				});
			})(index);

			return this;
		};

        /**
         * @param socket
         * @param index
         * @returns {Bot}
         */
		this.messengerEvents = function (socket, index) {
			(function (bot) {
				socket.on('messageAdditionRequest', function () {
                    if (connected[index]) {
                        io[bot].sockets.emit('messageAdded');
                    }
				});
			})(index);

			return this;
		};

        /**
         * @param socket
         * @param index
         * @returns {Bot}
         */
        this.statisticsEvents = function (socket, index) {
            (function (bot) {
                socket.on('updateWonTodayRequest', function (price) {
                    if (connected[index]) {
                        io[bot].sockets.emit('updateWonToday', price);
                    }
                });
            })(index);

            return this;
        };

        /**
         * @param bot
         * @param index
         * @returns {boolean}
         */
		this.steamInitialize = function (bot, index) {

			logger.info('[BOT] Steam initialization!');

			/** Steam modules initialize **/
			steam.push(require('steam'));
			steamID.push(require('steamid'));
			steamTrade.push(require('steam-trade'));
			steamUser.push(require('steam-user'));
			steamTradeOffers.push(require('steam-tradeoffers'));
			steamTradeManager.push(require('steam-tradeoffer-manager'));
			steamMobileConfirmation.push(require('steam-mobile-confirmations'));
            steamCommunity.push(require('steamcommunity'));
			steamTotp.push(require('steam-totp'));

			return this.steamUserInitialize(bot, index);
		};

		/**
		 * @returns {Bot}
		 */
		this.init = function () {

			/** Starting bot **/
			logger = new (winston.Logger)({
				transports: [
					new (winston.transports.Console)({
						colorize: true,
						level: 'debug'
					}),
					new (winston.transports.File)({
						level: 'info',
						timestamp: true,
						filename: './bot/logs/bot.log',
						json: true
					})
				]
			});

			logger.info('[BOT] Starting CSGO bots!');

            configOfSteamFile = fs.readFileSync('./config/steam-config.yaml', 'utf8');
            configOfBotFile = fs.readFileSync('./config/steam-bot.yaml', 'utf8');

            configOfBot = yaml.safeLoad(configOfBotFile);
            configOfSteam = yaml.safeLoad(configOfSteamFile);

            var bots = [];

            for (var index in configOfBot.bots) {
                bots.push(configOfBot.bots[index].parameters);
            }

			/** Connect to sockets **/
			this.connectSocket(bots);

			return this;
		};
	};

	/**
	 * @returns {Bot}
	 */
	this.start = function () {
		new Bot().init();
		return this;
	};
};