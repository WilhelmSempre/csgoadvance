var mode = process.argv[2];

if (mode === 'steamguard') {
	var steamGuard = require('./steamguard.js');

	/** Steam Guard **/
	var steamGuardInstance = new steamGuard();
	steamGuardInstance.start();

} else {

	/** Dependencies **/
	var CSBOT = require('./bot.js');

	/** BOT **/
	var CSBOTInstance = new CSBOT();
	CSBOTInstance.start();
}