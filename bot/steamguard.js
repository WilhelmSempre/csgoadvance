/**
 *
 */
module.exports = function () {

	'use strict';

	/** Init variables **/
	var steam, steamTotp;

	/**
	 * @constructor
	 */
	var SteamGuard = function () {

		/**
		 * @type {string}
		 */
		this.steamGuard = '';

		/**
		 * @returns {SteamGuard}
		 */
		this.generate = function () {
			steamTotp = require('steam-totp');

			var sharedSecret = 'L9Y5oq+NPmqmXvA7UvPrXH0PEl4='; // ppszczol shared secret
			//var sharedSecret = 'WWTGXJLEkWzdVmDV3NfKch9sacs=';

			(function (self) {
				setInterval(function () {

					/** Get steam guard **/
					var steamGuard = steamTotp.generateAuthCode(sharedSecret);

					if (self.steamGuard !== steamGuard) {
						self.steamGuard = steamGuard;
						console.log(self.steamGuard);
					}
				}, 10000);
			})(this);

			return this;
		};

		this.init = function () {
			this.generate();
		};
	};

	/**
	 * @returns {exports}
	 */
	this.start = function () {
		new SteamGuard().init();
		return this;
	};
};