<?php


namespace CSGOADVANCE\core;

use CSGOADVANCE\core\Managers\Types\EnvironmentType;
use CSGOADVANCE\core\Twig\TwigSHA1Extension;
use CSGOADVANCE\core\Twig\TwigTruncateExtension;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AbstractResource
 * @package CSGOADVANCE\core
 */
abstract class AbstractResource
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * @param ContainerInterface $application
     * @return EntityManager
     */
    public function getEntityManager(ContainerInterface $application)
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->createEntityManager($application);
        }

        return $this->entityManager;
    }

    /**
     * @param $application
     * @return EntityManager
     */
    public function createEntityManager(ContainerInterface $application)
    {
        $path = [dirname(__DIR__). '/src/Entity'];

        $devMode = true;

        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode, null, null, false);

        switch (getenv('ENVIRONMENT')) {
            case EnvironmentType::PRODUCTION:
                $connectionParams = Yaml::parse(file_get_contents('config/prod.database.yaml'));
                break;
            case EnvironmentType::TEST:
                $connectionParams = Yaml::parse(file_get_contents('config/test.database.yaml'));
                break;
            case EnvironmentType::DEVELOPMENT:
                $connectionParams = Yaml::parse(file_get_contents('config/local.database.yaml'));
                break;
            default:
                $connectionParams = Yaml::parse(file_get_contents('config/prod.database.yaml'));
        }


        $connectionData = [
            'dbname' => $connectionParams['database']['db_name'],
            'user' => $connectionParams['database']['db_user'],
            'password' => $connectionParams['database']['db_password'],
            'host' => $connectionParams['database']['db_host'],
            'driver' => $connectionParams['database']['driver']
        ];

        return EntityManager::create($connectionData, $config);
    }

    /**
     * @param $controller
     * @return Twig
     */
    public function createTwig($controller)
    {
        $view = new Twig(dirname(dirname(__FILE__)) . '/src/Views/template', [
            'cache' => dirname(dirname(__FILE__)) . '/src/Views/cache',
            'debug' => true
        ]);

        $basePath = rtrim(str_ireplace('index.php', '', $controller['request']->getUri()->getBasePath()), '/');
        $view->addExtension(new TwigExtension($controller['router'], $basePath));
        $view->addExtension(new TwigTruncateExtension());
        $view->addExtension(new TwigSHA1Extension());

        return $view;
    }
}
