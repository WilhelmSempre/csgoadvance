<?php

namespace CSGOADVANCE\core;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\core\Managers\Types\EnvironmentType;
use CSGOADVANCE\src\Helpers\GameHelper;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use Interop\Container\ContainerInterface;

/**
 * Class TestFixtures
 * @package CSGOADVANCE\core
 */
class TestFixtures
{

    /**
     * @var DatabaseManager
     */
    public $databaseManager;

    /**
     * @param ContainerInterface $container
     * @return array
     */
    public function generate(ContainerInterface $container)
    {
        $this->databaseManager = $container->get('database');

        if (!getenv('ENVIRONMENT')) {
            putenv('ENVIRONMENT=' . EnvironmentType::DEVELOPMENT);
        }

        $userHelper = new UserHelper($this->databaseManager);
        $itemHelper = new ItemHelper($this->databaseManager);
        $gameHelper = new GameHelper($this->databaseManager);
        $security = new SecurityManager($container);

        $token = $security->generateSecurityToken('Chroma 3 Case', 'now');

        if (!$itemHelper->ifItemExists([
            'market_name' => 'Chroma 3 Case'
        ])) {
            $itemHelper->addItem([
                'name' => 'Chroma 3 Case',
                'market_name' => 'Chroma 3 Case',
                'icon' => sprintf('http://steamcommunity-a.akamaihd.net/economy/image/%s', '-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXU5A1PIYQNqhpOSV-fRPasw8rsUFJ5KBFZv668FFYynaSdJGhE74y0wNWIw_OlNuvXkDpSuZQmi--SrN-h3gey-Uo6YWmlIoCLMlhplhFFvwI'),
                'description' => '<br>Container Series #141<br> <br>Contains one of the following:<br>Dual Berettas | Ventilators<br>G3SG1 | Orange Crash<br>M249 | Spectre<br>MP9 | Bioleak<br>P2000 | Oceanic<br>Sawed-Off | Fubar<br>SG 553 | Atlas<br>CZ75-Auto | Red Astor<br>Galil AR | Firefight<br>SSG 08 | Ghost Crusader<br>Tec-9 | Re-Entry<br>XM1014 | Black Tie<br>AUG | Fleet Flock<br>P250 | Asiimov<br>UMP-45 | Primal Saber<br>PP-Bizon | Judgement of Anubis<br>M4A1-S | Chantico\'s Fire<br>or an Exceedingly Rare Special Item!<br> <br><br>',
                'price' => 0.03,
                'token' => $token
            ]);
        }

        if (!$userHelper->ifUserExists([
            'steamID' => 76561198060996512
        ])) {
            $steam = $container->get('steam');
            $userSteamData = $steam->getUserData(76561198060996512);
            $userHelper->addUser($userSteamData);
        }

        $user = $userHelper->getUser([
            'steamID' => 76561198060996512
        ]);

        $userHelper->updateTradeLink($user, 'https://steamcommunity.com/tradeoffer/new/?partner=67775546&token=xcFccdfk');

        $item = $itemHelper->getItem([
            'market_name' => 'Chroma 3 Case'
        ]);

        return [
            'user' => $user,
            'item' => $item
        ];
    }
}
