<?php

namespace CSGOADVANCE\core\Managers;

use CSGOADVANCE\core\Managers\Types\EnvironmentType;
use Symfony\Component\Yaml\Yaml;
use Tracy\Debugger;

/**
 * Class EnvironmentManager
 * @package CSGOADVANCE\core\Managers
 */
class EnvironmentManager
{

    /**
     * EnvironmentManager constructor.
     * @param $environmentConfigPath
     */
    public function __construct($environmentConfigPath)
    {
        if (file_exists($environmentConfigPath)) {
            $this->config = Yaml::parse(file_get_contents($environmentConfigPath));

            if (!getenv('ENVIRONMENT')) {
                switch ($this->getEnvironment()) {
                    case EnvironmentType::PRODUCTION:
                        putenv('ENVIRONMENT=' . EnvironmentType::PRODUCTION);
                        break;
                    case EnvironmentType::DEVELOPMENT:
                        putenv('ENVIRONMENT=' . EnvironmentType::DEVELOPMENT);
                        break;
                    case EnvironmentType::TEST:
                        putenv('ENVIRONMENT=' . EnvironmentType::TEST);
                        break;
                    default:
                        putenv('ENVIRONMENT=' . EnvironmentType::DEVELOPMENT);
                }
            }
        } else {
            $this->config['environment'] = EnvironmentType::PRODUCTION;
        }
    }

    /**
     * @param $state
     */
    public function setProfiler($state = false)
    {
        if ($state) {
            if (class_exists('Tracy\Debugger')) {
                Debugger::enable();
            }
        }
    }

    /**
     * @return mixed
     */
    private function getEnvironment()
    {
        return $this->config['environment'];
    }
}
