<?php

namespace CSGOADVANCE\core\Managers;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

/**
 * Class DatabaseManager
 * @package CSGOADVANCE\core\Managers
 */
class DatabaseManager
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * DatabaseManager constructor.
     * @param ContainerInterface $application
     * @param EntityManager $entityManager
     */
    public function __construct(ContainerInterface $application, EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManager|mixed
     */
    public function getManager()
    {
        return $this->entityManager;
    }
}
