<?php

namespace CSGOADVANCE\core\Managers;

use Psr\Container\ContainerInterface;

/**
 * Class SecurityManager
 * @package CSGOADVANCE\core\Managers
 */
class SecurityManager
{

    /**
     * @var BotManager
     */
    private $bot;

    /**
     * SecurityManager constructor.
     * @param ContainerInterface $application
     */
    public function __construct(ContainerInterface $application)
    {
        $this->bot = $application->get('bot');
    }

    /**
     * @param $params
     * @param bool $date
     * @return string
     */
    public function generateSecurityToken($params, $date = false)
    {
        $securityKeys = $this->bot->getBotSecurityKeys();
        $deviceID = $this->bot->getDeviceID();
        $date = !$date ? '' : (new \DateTime($date))->format('d/m/Y H:i');
        $hashString = $securityKeys['identity_secret'] . $securityKeys['shared_secret'] . $deviceID . $params . $date;

        return sha1($hashString);
    }

    /**
     * @param $params
     * @param $pattern
     * @param bool $date
     * @return bool
     */
    public function compareSecurityToken($params, $pattern, $date = false)
    {
        $token = $this->generateSecurityToken($params, $date);
        return $token === $pattern;
    }
}
