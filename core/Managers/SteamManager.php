<?php

namespace CSGOADVANCE\core\Managers;

use Symfony\Component\Yaml\Yaml;
use ErrorException;
use Slim\Http\Response;
use waylaidwanderer\SteamCommunity\SteamCommunity;

/**
 * Class SteamManager
 * @package CSGOADVANCE\core\Managers
 */
class SteamManager
{

    /**
     * @var array
     */
    private $config = [];

    /**
     * SteamManager constructor.
     * @param $steamConfigPath
     */
    public function __construct($steamConfigPath)
    {
        $this->config = Yaml::parse(file_get_contents($steamConfigPath));
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->config['steam']['config']['APIKEY'];
    }

    /**
     * @return mixed
     */
    public function getSteamlyticApiKey()
    {
        return $this->config['steam']['config']['STEAMLYTICSKEY'];
    }

    /**
     * @return bool
     */
    public function ifUserLogged()
    {
        return isset($_SESSION['steamid']);
    }


    /**
     * @param Response $response
     * @return SteamManager|Response
     */
    public function steamLogin(Response $response)
    {
        try {

            $url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
            $url .= $_SERVER['HTTP_HOST'];

            /** @var \LightOpenID $openid */
            $openid = new \LightOpenID($url);

            if (!$openid->mode) {
                $openid->identity = 'http://steamcommunity.com/openid';

                return $response->withRedirect($openid->authUrl());
            } elseif ($openid->mode == 'cancel') {
                echo 'User has canceled authentication!';
            } else {
                if ($openid->validate()) {
                    $identity = $openid->identity;
                    $pattern = '/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/';

                    preg_match($pattern, $identity, $matches);

                    $_SESSION['steamid'] = $matches[1];
                    $_SESSION['id'] = session_id();

                    return $response->withRedirect($url);
                } else {
                    echo 'User is not logged in.' . PHP_EOL;
                }
            }
        } catch (ErrorException $error) {
            echo $error->getMessage();
        }

        return $this;
    }

    /**
     * @param Response $response
     * @return SteamManager|Response
     */
    public function steamLogout(Response $response)
    {
        if (isset($_GET['logout'])) {
            $inventoryCachePath = USER_INVENTORY_CACHE_DIR . $_SESSION['steamid']. '.inventory.txt';
            unlink($inventoryCachePath);

            unset($_SESSION['steamid']);

            $url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
            $url .= $_SERVER['HTTP_HOST'];

            return $response->withRedirect($url);
        }

        return $this;
    }


    /**
     * @param null $userSteamID
     * @return array|mixed
     */
    public function getUserData($userSteamID = null)
    {
        if ($userSteamID) {
            $steamID = $userSteamID;
        } else {
            if (isset($_SESSION['steamid'])) {
                $steamID = $_SESSION['steamid'];
            } else {
                $steamID = '';
            }
        }

        if ($steamID !== '') {
            $steamPath = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s';

            $content = json_decode(file_get_contents(sprintf($steamPath, $this->config['steam']['config']['APIKEY'], $steamID)), true);

            if (empty($userData['steam_uptodate'])) {
                $userData = [
                    'steamid' => isset($content['response']['players'][0]['steamid']) ? $content['response']['players'][0]['steamid'] : '',
                    'steam_steamid' => isset($content['response']['players'][0]['steamid']) ? $content['response']['players'][0]['steamid'] : '',
                    'steam_communityvisibilitystate' => isset($content['response']['players'][0]['communityvisibilitystate']) ? $content['response']['players'][0]['communityvisibilitystate'] : '',
                    'steam_profilestate' => isset($content['response']['players'][0]['profilestate']) ? $content['response']['players'][0]['profilestate'] : '',
                    'steam_personaname' => isset($content['response']['players'][0]['personaname']) ? $content['response']['players'][0]['personaname'] : '',
                    'steam_lastlogoff' => isset($content['response']['players'][0]['lastlogoff']) ? $content['response']['players'][0]['lastlogoff'] : '',
                    'steam_profileurl' => isset($content['response']['players'][0]['profileurl']) ? $content['response']['players'][0]['profileurl'] : '',
                    'steam_avatar' => isset($content['response']['players'][0]['avatar']) ? $content['response']['players'][0]['avatar'] : '',
                    'steam_avatarmedium' => isset($content['response']['players'][0]['avatarmedium']) ? $content['response']['players'][0]['avatarmedium'] : '',
                    'steam_avatarfull' => isset($content['response']['players'][0]['avatarfull']) ? $content['response']['players'][0]['avatarfull'] : '',
                    'steam_personastate' => isset($content['response']['players'][0]['personastate']) ? $content['response']['players'][0]['personastate'] : '',
                    'steam_realname' => isset($content['response']['players'][0]['realname']) ? $content['response']['players'][0]['realname'] : '',
                    'steam_primaryclanid' => isset($content['response']['players'][0]['primaryclanid']) ? $content['response']['players'][0]['primaryclanid'] : '',
                    'steam_timecreated' => isset($content['response']['players'][0]['timecreated']) ? $content['response']['players'][0]['timecreated'] : '',
                    'steam_uptodate' => time()
                ];
            }

            return $userData;
        }

        return [];
    }

    /**
     * @param $steamID
     * @param bool $reset
     * @return array|mixed
     */
    public function getUserInventory($steamID, $reset = true)
    {
        $inventoryCachePath = USER_INVENTORY_CACHE_DIR . $steamID . '.inventory.txt';
        $inventoryFileTimeExpires = false;
        $items = [];

        if (file_exists($inventoryCachePath)) {
            $inventoryFileTimeExpires = (time() - filemtime($inventoryCachePath) > 60);

            if (!$inventoryFileTimeExpires && !$reset) {
                return unserialize(file_get_contents($inventoryCachePath));
            }
        }

        $steamCommunity = new SteamCommunity();

        $inventoryURL = sprintf('http://steamcommunity.com/profiles/%d/inventory/json/730/2?l=pl', $steamID);

        $inventory = false;

        while (!$inventory) {
            $curl = curl_init($inventoryURL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($curl, CURLOPT_TIMEOUT,10);
            $inventory = curl_exec($curl);

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            if ($httpcode === 429) {
                if (file_exists($inventoryCachePath)) {
                    return unserialize(file_get_contents($inventoryCachePath));
                } else {
                    return [];
                }
            }
        }

        $priceDatabaseFilePath = sprintf('%s/database/pricelist.json', dirname(dirname(dirname(__FILE__))));
        $priceList = json_decode(file_get_contents($priceDatabaseFilePath), true);

        $inventoryJSON = json_decode($inventory, true);

        if (isset($inventoryJSON) && $inventoryJSON['rgInventory']) {
            foreach ($inventoryJSON['rgInventory'] as $inventory) {
                $description = '';
                $price = 0;
                $item = $inventoryJSON['rgDescriptions'][$inventory['classid'] . '_' . $inventory['instanceid']];

                foreach ($item['descriptions'] as $descriptions) {
                    $description .= $descriptions['value'] . '<br>';
                }

                if (isset($priceList['items'][$item['market_name']])) {
                    $price = $priceList['items'][$item['market_name']];
                }

                if ((bool)$item['tradable'] && (bool)$item['marketable']) {
                    $items[] = [
                        'name' => $item['name'],
                        'market_name' => $item['market_name'],
                        'icon' => sprintf('http://steamcommunity-a.akamaihd.net/economy/image/%s', $item['icon_url']),
                        'description' => trim($description),
                        'price' => $price,
                        'tradable' => $item['tradable']
                    ];
                }
            }
        }

        file_put_contents($inventoryCachePath, serialize($items));

        return $items;
    }
}
