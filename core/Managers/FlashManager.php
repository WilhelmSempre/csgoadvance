<?php

namespace CSGOADVANCE\core\Managers;

use Psr\Container\ContainerInterface;
use Slim\Flash\Messages;

/**
 * Class FlashManager
 * @package CSGOADVANCE\core\Managers
 */
class FlashManager
{

    /**
     *
     */
    const NOW = 1;

    /**
     * @var ContainerInterface
     */
    private $application;

    /**
     * @var
     */
    private $messages;

    /**
     * FlashManager constructor.
     * @param ContainerInterface $application
     * @param Messages $messages
     */
    public function __construct(ContainerInterface $application, Messages $messages)
    {
        $this->messages = $messages;
        $this->application = $application;
    }

    /**
     * @param $label
     * @param $text
     * @param null $mode
     */
    public function add($label, $text, $mode = null)
    {
        if ($mode === self::NOW) {
            $this->messages->addMessageNow($label, $text);
        }

        $this->messages->addMessage($label, $text);
    }

    /**
     * @param null $key
     * @return array|mixed|null
     */
    public function get($key = null)
    {
        if ($key) {
            return $this->messages->getMessage($key);
        }

        return $this->messages->getMessages();
    }

    /**
     * @param $key
     * @return mixed
     */
    public function has($key)
    {
        return $this->messages->hasMessage($key);
    }
}
