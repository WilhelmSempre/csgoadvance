<?php

namespace CSGOADVANCE\core\Managers\Types;

/**
 * Class EnvironmentType
 * @package CSGOADVANCE\core\Managers\Types
 */
class EnvironmentType
{

    /**
     *
     */
    const DEVELOPMENT = 'development';

    /**
     *
     */
    const PRODUCTION = 'production';

    /**
     *
     */
    const TEST = 'test';

    /**
     * @var array
     */
    protected static $select = [
        self::DEVELOPMENT => 'Development',
        self::TEST => 'Production',
        self::PRODUCTION => 'Production'
    ];
}
