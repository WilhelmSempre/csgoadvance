<?php

namespace CSGOADVANCE\core\Managers;

use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use Slim\Http\Response;

/**
 * Class ViewManager
 * @package CSGOADVANCE\core\Managers
 */
class ViewManager
{

    /**
     * @var Twig
     */
    private $twig;

    public function __construct(ContainerInterface $application, Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param Response $response
     * @param $template
     * @param array $args
     */
    public function render(Response $response, $template, $args = [])
    {
        $this->twig->render($response, $template, $args);
    }

    /**
     * @param $template
     * @param array $args
     * @return string
     */
    public function dump($template, $args = [])
    {
        return $this->twig->fetch($template, $args);
    }
}
