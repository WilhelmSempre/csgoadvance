<?php

namespace CSGOADVANCE\core\Managers;

use Psr\Container\ContainerInterface;

/**
 * Class MailManager
 * @package CSGOADVANCE\core\Managers
 */
class MailManager
{

    /**
     * @var mixed
     */
    private $registry;

    /**
     * @var \Swift_Mailer
     */
    private $mailer = null;

    /**
     * MailManager constructor.
     * @param ContainerInterface $application
     */
    public function __construct(ContainerInterface $application)
    {

        /** @var RegistryManager registry */
        $this->registry = $application->get('application');

        $mail = $this->registry->getMail();

        $transport = new \Swift_SmtpTransport($mail['host'], $mail['port'], $mail['encryption']);
        $transport = $transport->setUsername($mail['username'])->setPassword($mail['password']);

        $this->mailer = new \Swift_Mailer($transport);
    }

    /**
     * @return null|\Swift_Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param $subject
     * @param $from
     * @param $to
     * @param $body
     * @return \Swift_Message
     */
    public function createMessage($subject, $from, $to, $body)
    {
        $message = new \Swift_Message($subject);
        $message->setFrom($from)->setTo($to)->setBody($body, 'text/html');

        return $message;
    }

    /**
     * @param \Swift_Message $message
     * @return int
     */
    public function sendMessage(\Swift_Message $message)
    {
        return $this->mailer->send($message);
    }

    /**
     * @return mixed
     */
    public function getMailSubject()
    {
        $mail = $this->registry->getMail();
        return $mail['mailBody']['mailSubject'];
    }

    /**
     * @return mixed
     */
    public function getMailAddress()
    {
        $mail = $this->registry->getMail();
        return $mail['mailBody']['mailAddress'];
    }
}
