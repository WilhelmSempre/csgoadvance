<?php

namespace CSGOADVANCE\core\Managers;

use Symfony\Component\Yaml\Yaml;

/**
 * Class BotManager
 * @package CSGOADVANCE\core\Managers
 */
class BotManager
{

    /**
     * @var
     */
    private $config = [];


    /**
     * BotManager constructor.
     * @param $botConfigPath
     */
    public function __construct($botConfigPath)
    {
        $this->config = Yaml::parse(file_get_contents($botConfigPath));
    }

    /**
     * @return mixed
     */
    public function getBots()
    {
        return $this->config['bots'];
    }

    /**
     * @param int $bot
     * @return array
     */
    public function getBotSecurityKeys($bot = 0)
    {
        return [
            'identity_secret' => $this->config['bots'][$bot]['config']['identity_secret'],
            'shared_secret' => $this->config['bots'][$bot]['config']['shared_secret']
        ];
    }

    /**
     * @param int $bot
     * @return mixed
     */
    public function getDeviceID($bot = 0)
    {
        return $this->config['bots'][$bot]['config']['device_id'];
    }

    /**
     * @param int $bot
     * @return mixed
     */
    public function getSteamID($bot = 0)
    {
        return $this->config['bots'][$bot]['config']['steam_id'];
    }
}
