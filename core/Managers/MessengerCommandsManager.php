<?php

namespace CSGOADVANCE\core\Managers;

use CSGOADVANCE\src\Helpers\BanHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Types\UserRoleType;
use Psr\Container\ContainerInterface;

/**
 * Class MessengerCommandsManager
 * @package CSGOADVANCE\core\Managers
 */
class MessengerCommandsManager
{

    /**
     * @var array
     *
     * /mute [steamID] [duration] [reason]
     * /unmute [steamID]
     */
    private $commandsList = ['/mute', '/unmute'];

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->databaseManager = $this->container->get('database');
    }

    /**
     * @param $message
     * @param User $user
     * @return array
     */
    public function parseMessageForCommands($message, User $user) {
        $messageArray = explode(' ', $message);
        $output = false;
        $error = false;
        $userHelper = new UserHelper($this->databaseManager);
        $banHelper = new BanHelper($this->databaseManager);

        if (in_array($messageArray[0], $this->commandsList)) {
            if (!$userHelper->isRole($user, UserRoleType::ADMINISTRATOR) && !$userHelper->isRole($user, UserRoleType::MODERATOR)) {
                $output = 'Chat commands are only for administrators or moderators!' ;
                return ['commands' => true, 'error' => true, 'command_output' => $output];
            }

            switch (ltrim($messageArray[0], '/')) {
                case 'mute':
                    if (count($messageArray) > 4) {
                        $output = 'Too many arguments!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    if (!isset($messageArray[1])){
                        $output = 'User steam ID required!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    if (!intval($messageArray[1])) {
                        $output = 'Invalid steam ID format!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    $period = false;
                    $date = new \DateTime();

                    if (!isset($messageArray[2])){
                        $output = 'Hours period required!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    if (!intval($messageArray[2])) {
                        $output = 'Invalid hours period format!' ;
                        return ['commands' => true,  'error' => true, 'command_output' => $output];
                    }

                    $period = $date->modify('+' . (int) $messageArray[2] . ' minutes');

                    if ((int) $messageArray[2] > 60) {
                        if ($userHelper->isRole($user, UserRoleType::MODERATOR)) {
                            $output = 'Moderator cannot mute more on 60 minutes!' ;
                            return ['commands' => true,  'error' => true, 'command_output' => $output];
                        }
                    }

                    $reason = null;

                    if (isset($messageArray[3])){
                        $reason = str_replace('+', ' ', $messageArray[3]);
                    }

                    /** @var User $bannedUser */
                    $bannedUser = $userHelper->getUser([
                        'steamID' => $messageArray[1]
                    ]);

                    if ($userHelper->isMe($user, $bannedUser)) {
                        return ['commands' => true, 'error' => true, 'command_output' => 'You cannot mute yourself!'];
                    }

                    $bannedUserID = $bannedUser->getId();

                    if (!$banHelper->banExists([
                        'userID' => $bannedUserID
                    ])) {
                        $banHelper->addBan($bannedUser, $period, $reason);
                    } else {
                        return ['commands' => true, 'error' => true, 'command_output' => 'User arleady muted!'];
                    }

                    return ['commands' => true, 'error' => false, 'command_output' => sprintf('User with steam ID %s muted!', $messageArray[1])];

                    break;

                case 'unmute':
                    if (count($messageArray) > 2) {
                        $output = 'Too many arguments!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    if (!isset($messageArray[1])){
                        $output = 'User steam ID required!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    if (!intval($messageArray[1])) {
                        $output = 'Invalid steam ID format!' ;
                        return ['commands' => true, 'error' => true, 'command_output' => $output];
                    }

                    /** @var User $bannedUser */
                    $bannedUser = $userHelper->getUser([
                        'steamID' => $messageArray[1]
                    ]);

                    if ($userHelper->isMe($user, $bannedUser)) {
                        return ['commands' => true, 'error' => true, 'command_output' => 'You cannot mute yourself!'];
                    }

                    $bannedUserID = $bannedUser->getId();

                    if ($banHelper->banExists([
                        'userID' => $bannedUserID
                    ])) {
                        $banHelper->removeBan([
                            'userID' => $bannedUserID
                        ]);
                    } else {
                        return ['commands' => true, 'error' => true, 'command_output' => 'User arleady unmuted!'];
                    }

                    return ['commands' => true, 'error' => false, 'command_output' => sprintf('User with steam ID %s unmuted!', $messageArray[1])];

                    break;
            }
        }
    }
}