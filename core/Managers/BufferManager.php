<?php

namespace CSGOADVANCE\core\Managers;

/**
 * Class BufferManager
 * @package CSGOADVANCE\core\Managers
 */
final class BufferManager
{

    /**
     * @var
     */
    private $file;

    /**
     * @param $file
     */
    public function setBuffer($file)
    {
        $this->file = $file;
    }

    /**
     * @return bool
     */
    public function isBuffer()
    {
        return file_exists($this->file);
    }

    /**
     * @return bool|string
     */
    public function readBuffer()
    {
        return file_get_contents($this->file);
    }

    /**
     * @return bool
     */
    public function removeBuffer()
    {
        return unlink($this->file);
    }
}