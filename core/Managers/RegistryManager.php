<?php

namespace CSGOADVANCE\core\Managers;

use Symfony\Component\Yaml\Yaml;

/**
 * Class RegistryManager
 * @package CSGOADVANCE\core\Managers
 */
class RegistryManager
{

    /**
     * @var
     */
    private $config = [];


    /**
     * RegistryManager constructor.
     * @param $configPath
     */
    public function __construct($configPath)
    {
        $this->config = Yaml::parse(file_get_contents($configPath));
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->config['application']['version'];
    }

    /**
     * @return mixed
     */
    public function getDefaultMultiplies()
    {
        $multiplies = $this->config['multiplies'];
        $multipliesArray = [];

        foreach ($multiplies as $multiply) {
            foreach ($multiply as $key => $value) {
                $multipliesArray[$key] = $value;
            }
        }

        return $multipliesArray;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->config['mail'];
    }

    /**
     * @return float
     */
    public function getDepositPriceLimit()
    {
        return (float) $this->config['prices']['depositLimit'];
    }

    /**
     * @return float
     */
    public function getAffiliatedUserProvision()
    {
        return (float) $this->config['prices']['affiliatedUserProvision'];
    }

    /**
     * @return float
     */
    public function getAffiliatePrice()
    {
        return (float) $this->config['prices']['affiliatePrice'];
    }
}
