<?php

namespace CSGOADVANCE\core;

use Slim\App;

/**
 * Class Routes
 * @package CSGOADVANCE\core
 */
class Routes
{

    /**
     * @param App $application
     */
    public function run(App $application)
    {
        $application->get('/', 'CSGOADVANCE\src\Controllers\IndexController:indexAction')
            ->setName('index');

        $application->post('/index/deposit', 'CSGOADVANCE\src\Controllers\IndexController:depositAction')
            ->setName('index-deposit');

        $application->post('/user/settings/{id}', 'CSGOADVANCE\src\Controllers\UserProfileController:updateSettings')
            ->setName('user-settings');

        $application->get('/user/{id}', 'CSGOADVANCE\src\Controllers\UserProfileController:indexAction')
            ->setName('user-profile');

        $application->post('/user/{id}/justice', 'CSGOADVANCE\src\Controllers\UserProfileController:justiceAction')
            ->setName('justice');

        $application->get('/deposit', 'CSGOADVANCE\src\Controllers\DepositController:indexAction')
            ->setName('deposit');

        $application->post('/deposit', 'CSGOADVANCE\src\Controllers\DepositController:depositAction')
            ->setName('add-deposit');

        $application->post('/deposit/inventory', 'CSGOADVANCE\src\Controllers\DepositController:inventoryAction')
            ->setName('deposit-inventory');

        $application->post('/deposit/trade', 'CSGOADVANCE\src\Controllers\DepositController:tradeAction')
            ->setName('deposit-trade');

        $application->post('/deposit/cancel', 'CSGOADVANCE\src\Controllers\DepositController:cancelAction')
            ->setName('deposit-cancel');

        $application->post('/game/item/before', 'CSGOADVANCE\src\Controllers\GameController:loadBeforeItemAction')
            ->setName('game-load-item-before');

        $application->post('/game/item/after', 'CSGOADVANCE\src\Controllers\GameController:loadAfterItemAction')
            ->setName('game-load-item-after');

        $application->post('/game/item/play', 'CSGOADVANCE\src\Controllers\GameController:playAction')
            ->setName('game-play');

        $application->post('/game/win', 'CSGOADVANCE\src\Controllers\GameController:winAction')
            ->setName('game-win');

        $application->post('/game/token', 'CSGOADVANCE\src\Controllers\GameController:tokenAction')
            ->setName('game-token');

        $application->get('/widthdraw', 'CSGOADVANCE\src\Controllers\WithdrawController:indexAction')
            ->setName('withdraw');

        $application->post('/widthdraw/inventory', 'CSGOADVANCE\src\Controllers\WithdrawController:inventoryAction')
            ->setName('withdraw-inventory');

        $application->post('/widthdraw/payment', 'CSGOADVANCE\src\Controllers\WithdrawController:paymentAction')
            ->setName('withdraw-payment');

        $application->post('/widthdraw/trade', 'CSGOADVANCE\src\Controllers\WithdrawController:tradeAction')
            ->setName('withdraw-trade');

        $application->post('/widthdraw/cancel', 'CSGOADVANCE\src\Controllers\WithdrawController:cancelAction')
            ->setName('withdraw-cancel');

        $application->post('/index/recent', 'CSGOADVANCE\src\Controllers\IndexController:recentGamesAction')
            ->setName('index-recent');

        $application->post('/index/message', 'CSGOADVANCE\src\Controllers\IndexController:messageAction')
            ->setName('index-message');

        $application->post('/messenger/add', 'CSGOADVANCE\src\Controllers\MessengerController:addAction')
            ->setName('messenger-add');

        $application->get('/support', 'CSGOADVANCE\src\Controllers\SupportController:indexAction')
            ->setName('support');

        $application->get('/affiliates', 'CSGOADVANCE\src\Controllers\AffiliatesController:indexAction')
            ->setName('affiliates');

        $application->post('/affiliates/create', 'CSGOADVANCE\src\Controllers\AffiliatesController:createAction')
            ->setName('affiliates-create');

        $application->post('/affiliates/use', 'CSGOADVANCE\src\Controllers\AffiliatesController:useAction')
            ->setName('affiliates-use');

        $application->get('/affiliates/shop', 'CSGOADVANCE\src\Controllers\AffiliatesController:shopAction')
            ->setName('affiliates-shop');

        $application->post('/affiliates/shop/buy', 'CSGOADVANCE\src\Controllers\AffiliatesController:buyAction')
            ->setName('affiliates-shop-buy');

        $application->post('/affiliates/shop/inventory', 'CSGOADVANCE\src\Controllers\AffiliatesController:inventoryShopAction')
            ->setName('affiliates-shop-inventory');

        $application->post('/affiliates/shop/trade', 'CSGOADVANCE\src\Controllers\AffiliatesController:tradeAction')
            ->setName('affiliates-shop-trade');

        $application->post('/affiliates/shop/cancel', 'CSGOADVANCE\src\Controllers\AffiliatesController:cancelAction')
            ->setName('affiliates-shop-cancel');

        $application->post('/support/send', 'CSGOADVANCE\src\Controllers\SupportController:sendAction')
            ->setName('support-send');

        $application->get('/terms', 'CSGOADVANCE\src\Controllers\TermsController:indexAction')
            ->setName('terms');
    }
}
