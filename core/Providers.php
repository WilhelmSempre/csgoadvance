<?php

namespace CSGOADVANCE\core;

use CSGOADVANCE\core\Managers\BotManager;
use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\EnvironmentManager;
use CSGOADVANCE\core\Managers\FlashManager;
use CSGOADVANCE\core\Managers\MailManager;
use CSGOADVANCE\core\Managers\MessengerCommandsManager;
use CSGOADVANCE\core\Managers\RegistryManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\core\Managers\SteamManager;
use CSGOADVANCE\core\Managers\ViewManager;
use CSGOADVANCE\core\Managers\BufferManager;
use Psr\Container\ContainerInterface;
use Slim\Flash\Messages;

/**
 * Class Providers
 * @package CSGOADVANCE\core
 */
class Providers extends AbstractResource
{

    /**
     * @param ContainerInterface $container
     * @return ContainerInterface
     */
    public function run(ContainerInterface $container)
    {
        $container['view'] = function ($controller) use ($container) {
            return new ViewManager($container, $this->createTwig($controller));
        };

        $container['flash'] = function () use ($container) {
            return new FlashManager($container, new Messages());
        };

        $container['database'] = function () use ($container) {
            return new DatabaseManager($container, $this->getEntityManager($container));
        };

        $container['steam'] = function () use ($container) {
            return new SteamManager('config/steam-config.yaml');
        };

        $container['bot'] = function () use ($container) {
            return new BotManager('config/steam-bot.yaml');
        };

        $container['security'] = function () use ($container) {
            return new SecurityManager($container);
        };

        $container['application'] = function () use ($container) {
            return new RegistryManager('config/config.yaml');
        };

        $container['env'] = function () use ($container) {
            return new EnvironmentManager('config/environment.yaml');
        };

        $container['mailer'] = function () use ($container) {
            return new MailManager($container);
        };

        $container['buffer'] = function () use ($container) {
            return new BufferManager();
        };

        $container['commands'] = function () use ($container) {
            return new MessengerCommandsManager($container);
        };

        return $container;
    }
}
