<?php

namespace CSGOADVANCE\core\Twig;

/**
 * Class TwigTruncateExtension
 * @package CSGOADVANCE\core\Twig
 */
class TwigTruncateExtension extends \Twig_Extension
{

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('truncate', [$this, 'truncate']),
        ];
    }

    /**
     * @param $text
     * @param $length
     * @param int $pieces
     * @param string $separator
     * @return string
     */
    public function truncate($text, $length, $pieces = 0, $separator = '...')
    {
        $retval = $text;

        $textLength = strlen($text);

        if ($textLength > $length) {
            $retvalStart = substr($text, 0, ceil($textLength / 2) - $pieces);

            $retvalEnd = substr($text, ceil($textLength / 2) + $pieces, $textLength - ceil($textLength / 2));
            $retvalLength = strlen($retvalStart . $retvalEnd);

            if ($retvalLength === 0) {
                $retval = $text;
            } else {
                $retval = $retvalStart . ' ' . $separator . ' ' . $retvalEnd;
            }
        }

        return $retval;
    }
}
