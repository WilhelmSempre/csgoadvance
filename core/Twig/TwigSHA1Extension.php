<?php

namespace CSGOADVANCE\core\Twig;

/**
 * Class TwigSHA1Extension
 * @package CSGOADVANCE\core\Twig
 */
class TwigSHA1Extension extends \Twig_Extension
{

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('sha1', [$this, 'hash']),
        ];
    }

    /**
     * @param $text
     * @return string
     */
    public function hash($text)
    {
        return sha1($text);
    }
}