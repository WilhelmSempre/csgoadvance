<?php

namespace CSGOADVANCE\core;

use CSGOADVANCE\core\Managers\BotManager;
use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\EnvironmentManager;
use CSGOADVANCE\core\Managers\FlashManager;
use CSGOADVANCE\core\Managers\RegistryManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\core\Managers\SteamManager;
use CSGOADVANCE\core\Managers\ViewManager;
use Interop\Container\ContainerInterface;

/**
 * Class Controller
 * @package CSGOADVANCE\core
 */
abstract class Controller
{

    /**
     * @var ViewManager
     */
    protected $viewManager;

    /**
     * @var FlashManager
     */
    protected $flashManager;

    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var SteamManager
     */
    protected $steamManager;

    /**
     * @var SecurityManager
     */
    protected $securityManager;

    /**
     * @var BotManager
     */
    protected $botManager;

    /**
     * @var RegistryManager
     */
    protected $registryManager;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Controller constructor.
     * @param ContainerInterface $application
     */
    public function __construct(ContainerInterface $application)
    {
        $this->flashManager = $application->get('flash');
        $this->viewManager = $application->get('view');
        $this->databaseManager = $application->get('database');
        $this->steamManager = $application->get('steam');
        $this->botManager = $application->get('bot');
        $this->securityManager = $application->get('security');
        $this->registryManager = $application->get('application');
        $this->environmentManager = $application->get('env');
    }
}
