<?php

use CSGOADVANCE\core\Providers;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Slim\App;

$application = new App();

$providers = new Providers();
$container = $providers->run($application->getContainer());
$environment = $container->get('env');
$entityManager = $container->get('database')->getManager();

return ConsoleRunner::createHelperSet($entityManager);
