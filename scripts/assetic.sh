#!/bin/sh

ACTUAL_CURSOR=$(pwd)
ACTUAL_DIR=$(basename $(pwd))

if [ "$ACTUAL_DIR" != "scripts" ]; then
    ln -sf "$ACTUAL_CURSOR/src/Views/template/assets/css/dist/style.min.css" "$ACTUAL_CURSOR/web/assets/css/style.min.css"
    ln -sf "$ACTUAL_CURSOR/src/Views/template/assets/js/dist/main.min.js" "$ACTUAL_CURSOR/web/assets/js/main.min.js"
fi
