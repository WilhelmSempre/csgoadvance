#!/usr/bin/env bash

ACTUAL_CURSOR=$(pwd)
ACTUAL_DIR=$(basename $(pwd))

if [ "$ACTUAL_DIR" != "scripts" ]; then
    cd src/Views/template
    grunt

    cp -rf assets/js/dist/main.min.js ${ACTUAL_CURSOR}/web/assets/js/main.min.js
    cp -rf assets/css/dist/style.min.css ${ACTUAL_CURSOR}/web/assets/css/style.min.css
    cp -rf assets/img ${ACTUAL_CURSOR}/web/assets
    cp -rf assets/css/fonts ${ACTUAL_CURSOR}/web/assets
    cp -rf assets/sounds ${ACTUAL_CURSOR}/web/assets
fi