#!/bin/sh

ACTUAL_CURSOR=$(pwd)
ACTUAL_DIR=$(basename $(pwd))

if [ "$ACTUAL_DIR" != "scripts" ]; then
    cd src/Views/template
    if [ "$1" = "install" ]; then
        if [ "$2" != "" ]; then
            bower install "$2" --save
        else
           echo "Specify package!"
        fi
    fi

    if [ "$1" = "update" ]; then
        bower update
    fi

    if [ "$1" = "search" ]; then
        if [ "$2" != "" ]; then
            bower search "$2"
        else
           echo "Specify package!"
        fi
    fi

    if [ "$1" = "" ]; then
        echo "Specify bower action!"
    fi
fi