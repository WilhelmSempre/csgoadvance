<?php

global $container;

use CSGOADVANCE\core\Managers\SteamManager;

/** @var SteamManager $steamManager */
$steamManager = $container->get('steam');

$steamlitycsApiKey = $steamManager->getSteamlyticApiKey();

$steamlitycsPriceLink = sprintf('http://api.csgo.steamlytics.xyz/v2/pricelist/compact?key=%s', $steamlitycsApiKey);

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $steamlitycsPriceLink);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($curl);

$pricesDatabaseJSONFilePath = dirname(dirname(__FILE__)) . '/database/pricelist.json';

file_put_contents($pricesDatabaseJSONFilePath, $response);

curl_close($curl);
