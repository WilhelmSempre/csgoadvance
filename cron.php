<?php

use CSGOADVANCE\core\Providers;

require_once 'vendor/autoload.php';

$application = new Slim\App();

$providers = new Providers();
$container = $providers->run($application->getContainer());

require_once 'scripts/price.php';
