<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\Affiliates;
use CSGOADVANCE\src\Entity\Buy;
use CSGOADVANCE\src\Helpers\AffiliatesHelper;
use CSGOADVANCE\src\Helpers\DepositHelper;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use CSGOADVANCE\src\Types\BuyStatusType;
use Slim\Http\Request;
use Slim\Http\Response;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use CSGOADVANCE\src\Entity\User;
use Interop\Container\ContainerInterface;
use Slim\Router;
use CSGOADVANCE\src\Entity\Item;

/**
 * Class AffiliatesController
 * @package CSGOADVANCE\src\Controllers
 */
final class AffiliatesController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * AffiliatesController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {

        /** @var Router $router */
        $router = $this->container->get('router');

        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);

        $user = [];

        try {
            $logged = $this->steamManager->ifUserLogged();

            if (!$logged) {
                throw new \Exception('User have to be logged!');
            }

            $steamUser = $this->steamManager->getUserData();

            $winsToday = $statisticHelper->getWinsToday();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                session_destroy();
            }


            /** @var Affiliates $refferals */
            $refferals = $user->getAffiliates();

            $wallet = (double) $user->getWallet();

            $this->viewManager->render($response, 'affiliates.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'refferals' => $refferals,
                'wallet' => $wallet,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function shopAction(Request $request = null, Response $response = null, $args = [])
    {

        /** @var Router $router */
        $router = $this->container->get('router');

        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);

        $user = [];

        try {
            $logged = $this->steamManager->ifUserLogged();

            if (!$logged) {
                throw new \Exception('User have to be logged!');
            }

            $steamUser = $this->steamManager->getUserData();

            $winsToday = $statisticHelper->getWinsToday();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            $this->viewManager->render($response, 'shop.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function buyAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $tradeLink = $userHelper->ifUserHasTradelink($user);

                if (!$tradeLink) {
                    throw new \Exception('Specify trade link!');
                }

                $ids = $request->getParsedBodyParam('id');
                $tokens = $request->getParsedBodyParam('token');
                $marketNames = $request->getParsedBodyParam('marketname');
                $itemPrices = $request->getParsedBodyParam('price');
                $totalPrice = 0;

                $userWallet = (double) $user->getWallet();

                if ($totalPrice > $userWallet) {
                    throw new \Exception('Not enough wallet credits to buy this items!');
                }

                if (!$withdrawHelper->ifPendingWithdrawExists($user) &&
                    !$depositHelper->ifPendingDepositExists($user) &&
                    !$withdrawHelper->ifPendingBuysExists($user)) {

                    foreach ($ids as $id) {
                        $marketNames[$id] = isset($marketNames[$id]) ? $marketNames[$id] : '';
                        $tokens[$id] = isset($tokens[$id]) ? $tokens[$id] : '';
                        $itemPrices[$id] = isset($itemPrices[$id]) ? $itemPrices[$id] : '';

                        $totalPrice = $totalPrice + $itemPrices[$id];

                        $token = $marketNames[$id];

                        if (!$this->securityManager->compareSecurityToken($token, $tokens[$id])) {
                            throw new \Exception('Invalid token!');
                        }

                        /** @var Item $item */
                        $item = $itemHelper->getItem([
                            'market_names' => $marketNames[$id]
                        ]);

                        $withdrawHelper->addBuy([
                            'item' => $item,
                            'user' => $user
                        ]);
                    }

                    $results['success'] = [
                        'status' => true,
                        'text' => $userHelper->getTradelink($user),
                        'market_names' => $marketNames
                    ];
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'You are pending trade request!'
                    ];
                }

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            } catch (\Exception $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function inventoryShopAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);

            try {
                $items = [];

                $bots = $this->botManager->getBots();

                $refresh = (bool) $request->getParsedBodyParam('refresh');

                foreach ($bots as $index => $bot) {
                    $botInventoryData = [];

                    $botInventoryData = $depositHelper->getUserInventory($this->botManager->getSteamID($index), $refresh);

                    foreach ($botInventoryData as $botInventory) {
                        $botInventory['token'] = $this->securityManager->generateSecurityToken($botInventory['market_name']);

                        if (!$itemHelper->ifItemExists([
                            'market_name' => $botInventory['market_name']
                        ])) {
                            $itemHelper->addItem($botInventory);
                        }

                        $items[] = $itemHelper->getItem([
                            'market_name' => $botInventory['market_name']
                        ]);
                    }
                }

                $sort = (bool) $request->getParsedBodyParam('sort');
                $sortBy = $request->getParsedBodyParam('sortby');
                $order = $request->getParsedBodyParam('order');
                $searchName = $request->getParsedBodyParam('name');

                if ($sort) {
                    if ($sortBy) {
                        switch ($sortBy) {
                            case 'name':
                                usort($items, function(Item $a, Item $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getName() > $b->getName() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getName() > $b->getName() ? 1 : -1;
                                    }
                                });
                                break;
                            case 'price':
                                usort($items, function(Item $a, Item $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getPrice() > $b->getPrice() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getPrice() > $b->getPrice() ? 1 : -1;
                                    }
                                });
                                break;
                        }
                    }

                    if ($searchName) {
                        if ($searchName !== '') {
                            $items = array_filter($items, function (Item $item) use ($searchName) {
                                return preg_match('/' . $searchName . '/i', $item->getName());
                            });
                        }
                    }

                } else {
                    usort($items, function(Item $a, Item $b) use ($order) {
                        return $a->getName() > $b->getName() ? 1 : -1;
                    });
                }

                $template = $this->viewManager->dump('partials/shop-inventory.html.twig', [
                    'items' => $items
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function createAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);

            try {
                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $referralCode = strtolower($request->getParsedBodyParam('your-code'));

                if ($referralCode !== '') {
                    if (strlen($referralCode) > 3 && strlen($referralCode) < 13) {
                        if (!$userHelper->ifReferralExists($referralCode)) {
                            $userHelper->createReferral($user, $referralCode);

                            $results['success'] = [
                                'status' => true,
                                'referral' => $referralCode,
                                'text' => 'Refferal created!'
                            ];
                        } else {
                            $results['errors'] = [
                                'status' => true,
                                'text' => 'Referral exists!'
                            ];
                        }
                    } else {
                        $results['errors'] = [
                            'status' => true,
                            'text' => 'Referral code length have to contain 4-12 characters!'
                        ];
                    }
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'Referral code is empty!'
                    ];
                }

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     */
    public function useAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $affiliatesHelper = new AffiliatesHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);

            try {
                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $referralCode = strtolower($request->getParsedBodyParam('code'));

                if ($referralCode !== '') {
                    if (!$affiliatesHelper->isMyReferral($user, $referralCode)) {

                        /** @var User $promotor */
                        $promotor = $userHelper->getUser([
                            'referral' => $referralCode
                        ]);

                        if (!$promotor) {
                            $results['errors'] = [
                                'status' => true,
                                'text' => 'Referral not assigned to user!'
                            ];
                        } else {
                            if (!$affiliatesHelper->referralUsed($user)) {
                                $affiliatesHelper->addAffiliate($user, $promotor);

                                $affiliatePrice = (double) $this->registryManager->getAffiliatePrice();

                                /** @var Item $item */
                                $item = $itemHelper->getItemsByPrice($affiliatePrice,  1.0)[0];

                                $userID = $user->getId();
                                $marketName = $item->getMarketName();
                                $tradeID = rand(0, 999999);

                                $token = $userID . $marketName . $tradeID;
                                $token = $this->securityManager->generateSecurityToken($token);

                                $depositParams = [
                                    'user' => $user,
                                    'trade' => $tradeID,
                                    'item' => $item,
                                    'token' => $token
                                ];

                                $depositHelper->addDeposit($depositParams);

                                $results['success'] = [
                                    'status' => true,
                                    'text' => 'Refferal use complete!'
                                ];
                            } else {
                                $results['errors'] = [
                                    'status' => true,
                                    'text' => 'Referral arleady used!'
                                ];
                            }
                        }
                    } else {
                        $results['errors'] = [
                            'status' => true,
                            'text' => 'Referral is yours!'
                        ];
                    }
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'Referral code is empty!'
                    ];
                }

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     * @throws \Exception
     */
    public function tradeAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            $marketNames = $request->getParsedBodyParam('market_names');
            $tradeID = $request->getParsedBodyParam('trade_id');

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $userID = $user->getId();
                $userWallet = floatval($user->getWallet());

                $totalPrice = 0;

                foreach ($marketNames as $marketName) {

                    /** @var Item $item */
                    $item = $itemHelper->getItem([
                        'market_name' => $marketName
                    ]);

                    $itemPrice = floatval($item->getPrice());
                    $totalPrice = $totalPrice + $itemPrice;

                    $token = $userID . $marketName . $tradeID;
                    $token = $this->securityManager->generateSecurityToken($token);
                }

                $buys = $withdrawHelper->getPendingBuys($user);

                foreach ($buys as $buy) {
                    $withdrawHelper->changeBuyStatus($buy, BuyStatusType::PAID);
                }

                $totalPrice = $userWallet - $totalPrice;
                $userHelper->updateWallet($user, $totalPrice);

                $results['success'] = [
                    'status' => true
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container
                ->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return AffiliatesController|Response
     * @throws \Exception
     */
    public function cancelAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $buys = $withdrawHelper->getPendingBuys($user);

                foreach ($buys as $buy) {
                    $withdrawHelper->changeBuyStatus($buy, BuyStatusType::UNPAID);
                }

                $results['success'] = [
                    'status' => true
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container
                ->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
