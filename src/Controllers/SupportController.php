<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\core\Managers\MailManager;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use Interop\Container\ContainerInterface;
use CSGOADVANCE\src\Entity\User;
use Slim\Http\Request;
use Slim\Http\Response;
use CSGOADVANCE\src\Helpers\UserHelper;
use Slim\Router;

/**
 * Class SupportController
 * @package CSGOADVANCE\src\Controllers
 */
final class SupportController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * DepositController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return SupportController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {

        /** @var Router $router */
        $router = $this->container->get('router');

        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);

        try {
            $logged = $this->steamManager->ifUserLogged();

            if (!$logged) {
                throw new \Exception('User have to be logged!');
            }

            $steamUser = $this->steamManager->getUserData();

            $winsToday = $statisticHelper->getWinsToday();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                session_destroy();
            }


            $this->viewManager->render($response, 'support.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     */
    public function sendAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {

            /** @var MailManager $mailer */
            $mailer = $this->container->get('mailer');

            $subject = $request->getParsedBodyParam('subject');
            $email = $request->getParsedBodyParam('email');
            $content = $request->getParsedBodyParam('text');

            $mailContent = file_get_contents(MAILS_DIR . 'support.html');
            $mailContent = strtr($mailContent, [
                '{{ subject }}' => $subject,
                '{{ email }}' => $email,
                '{{ content }}' => $content
            ]);

            $mailSubject = $mailer->getMailSubject();
            $mailSubject = str_replace('%page%', 'Support', $mailSubject);

            $mailAddress = $mailer->getMailAddress();
            $message = $mailer->createMessage($mailSubject, $email, $mailAddress, $mailContent);
            $mailer->sendMessage($message);

            $results['success'] = [
                'status' => true,
                'text' => 'Support e-mail has been send to review by administrators!'
            ];

            echo json_encode($results);
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }
    }
}