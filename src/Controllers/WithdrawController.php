<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Withdraw;
use CSGOADVANCE\src\Helpers\DepositHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use CSGOADVANCE\src\Types\DepositStatusType;
use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;
use CSGOADVANCE\src\Types\WithdrawStatusType;

/**
 * Class WithdrawController
 * @package CSGOADVANCE\src\Controllers
 */
final class WithdrawController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return WithdrawController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {
        $logged = $this->steamManager->ifUserLogged();

        /** @var Router $router */
        $router = $this->container->get('router');

        try {

            if (!$logged) {
                throw new \Exception('User have to be logged!');
            }

            $userHelper = new UserHelper($this->databaseManager);
            $statisticHelper = new StatisticsHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            $winsToday = $statisticHelper->getWinsToday();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                session_destroy();
            }

            $tradeLink = $userHelper->getTradelink($user);

            $this->viewManager->render($response, 'withdraw.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'tradelink' => $tradeLink,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return WithdrawController|Response
     */
    public function inventoryAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();
            $withdraws = [];

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $withdraws = $user->getWithdraws([
                    'status' => WithdrawStatusType::UNPAID
                ]);

                $sort = (bool) $request->getParsedBodyParam('sort');
                $sortBy = $request->getParsedBodyParam('sortby');
                $order = $request->getParsedBodyParam('order');
                $searchName = $request->getParsedBodyParam('name');
                $withdraws = $withdraws->toArray();

                if ($sort) {
                    if ($sortBy) {
                        switch ($sortBy) {
                            case 'name':
                                usort($withdraws, function(Withdraw $a, Withdraw $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getItem()->getName() > $b->getItem()->getName() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getItem()->getName() > $b->getItem()->getName() ? 1 : -1;
                                    }
                                });
                                break;
                            case 'price':
                                usort($withdraws, function(Withdraw $a, Withdraw $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getItem()->getPrice() > $b->getItem()->getPrice() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getItem()->getPrice() > $b->getItem()->getPrice() ? 1 : -1;
                                    }
                                });
                                break;
                        }
                    }

                    if ($searchName) {
                        if ($searchName !== '') {
                            $withdraws = array_filter($withdraws, function (Withdraw $item) use ($searchName) {
                                return preg_match('/' . $searchName . '/i', $item->getItem()->getName());
                            });
                        }
                    }

                } else {
                    usort($withdraws, function(Withdraw $a, Withdraw $b) use ($order) {
                        return $a->getItem()->getName() > $b->getItem()->getName() ? 1 : -1;
                    });
                }

                $template = $this->viewManager->dump('partials/withdraw-inventory.html.twig', [
                    'withdraws' => $withdraws
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return WithdrawController|Response
     */
    public function paymentAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $tradeLink = $userHelper->ifUserHasTradelink($user);

                if (!$tradeLink) {
                    throw new \Exception('Specify trade link!');
                }

                $ids = $request->getParsedBodyParam('id');
                $itemIDs = $request->getParsedBodyParam('item');
                $gameIDs = $request->getParsedBodyParam('game');
                $depositIDs = $request->getParsedBodyParam('deposit');
                $tokens = $request->getParsedBodyParam('token');
                $marketNames = $request->getParsedBodyParam('marketname');
                $tradeIDs = $request->getParsedBodyParam('tradeID');

                if (!$ids) {
                    throw new \Exception('You have to choose one item!');
                }

                if (!$withdrawHelper->ifPendingWithdrawExists($user) &&
                    !$withdrawHelper->ifPendingWithdrawExists($user) &&
                    !$withdrawHelper->ifPendingBuysExists($user)) {

                    foreach ($ids as $id) {
                        $marketNames[$id] = isset($marketNames[$id]) ? $marketNames[$id] : '';
                        $tokens[$id] = isset($tokens[$id]) ? $tokens[$id] : '';
                        $gameIDs[$id] = isset($gameIDs[$id]) ? $gameIDs[$id] : '';
                        $itemIDs[$id] = isset($itemIDs[$id]) ? $itemIDs[$id] : '';
                        $depositIDs[$id] = isset($depositIDs[$id]) ? $depositIDs[$id] : '';
                        $tradeIDs[$id] = isset($tradeIDs[$id]) ? $tradeIDs[$id] : '';

                        $token = $itemIDs[$id] . $tradeIDs[$id] . $depositIDs[$id] . $gameIDs[$id];

                        if (!$this->securityManager->compareSecurityToken($token, $tokens[$id])) {
                            throw new \Exception('Invalid token!');
                        }

                        /** @var Withdraw $withdraw */
                        $withdraw = $withdrawHelper->getWithdraw([
                            'ID' => $id
                        ]);

                        /** @var Deposit $deposit */
                        $deposit = $depositHelper->getDeposit([
                            'ID' => $depositIDs[$id]
                        ]);

                        $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::PENDING);
                        $depositHelper->changeDepositStatus($deposit, DepositStatusType::UNACTIVE);
                    }

                    $results['success'] = [
                        'status' => true,
                        'text' => $userHelper->getTradelink($user),
                        'market_names' => $marketNames
                    ];
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'You are pending trade request!'
                    ];
                }

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            } catch (\Exception $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return WithdrawController|Response
     */
    public function tradeAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $withdrawHelper = new WithdrawHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            /** @var Withdraw[] $withdraws */
            $withdraws = $withdrawHelper->getPendingWithdraws($user);

            try {

                foreach ($withdraws as $withdraw) {
                    $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::PAID);

                    $deposit = $withdraw->getDeposit();

                    $depositHelper->changeDepositStatus($deposit, DepositStatusType::UNACTIVE);
                }

                $results['success'] = [
                    'status' => true
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return WithdrawController|Response
     */
    public function cancelAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $withdrawHelper = new WithdrawHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            /** @var Withdraw[] $withdraws */
            $withdraws = $withdrawHelper->getPendingWithdraws($user);

            foreach ($withdraws as $withdraw) {
                try {

                    $deposit = $withdraw->getDeposit();

                    $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::UNPAID);
                    $depositHelper->changeDepositStatus($deposit, DepositStatusType::ACTIVE);

                    $results['success'] = [
                        'status' => true
                    ];

                    echo json_encode($results);
                } catch (\PDOException $error) {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'System error. Contact to administrator!'
                    ];

                    echo json_encode($results);
                }
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
