<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Managers\BufferManager;
use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Types\BuyStatusType;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Helpers\MessageHelper;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use CSGOADVANCE\src\Types\GameStatusType;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use CSGOADVANCE\src\Helpers\DepositHelper;
use Slim\Http\Request;
use CSGOADVANCE\src\Entity\Withdraw;
use Slim\Http\Response;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Entity\User;
use Interop\Container\ContainerInterface;
use Slim\Router;
use CSGOADVANCE\src\Types\DepositStatusType;
use CSGOADVANCE\src\Types\WithdrawStatusType;
use CSGOADVANCE\core\Buffer;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\Ban;
use CSGOADVANCE\src\Helpers\BanHelper;

/**
 * Class IndexController
 * @package CSGOADVANCE\src\Controllers
 */
final class IndexController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return IndexController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {
        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);
        $withdrawHelper = new WithdrawHelper($this->databaseManager);
        $itemHelper = new ItemHelper($this->databaseManager);
        $depositHelper = new DepositHelper($this->container, $this->databaseManager);
        $banHelper = new BanHelper($this->databaseManager);

        $logged = $this->steamManager->ifUserLogged();
        $tradeLink = $user = [];

        /** @var Router $router */
        $router = $this->container->get('router');

        if (array_key_exists('login', $request->getQueryParams())) {
            return $this->steamManager->steamLogin($response);
        }

        if (array_key_exists('logout', $request->getQueryParams())) {

            if ($logged) {
                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $userHelper->setIsArleadyLogged($user, null);
            }

            return $this->steamManager->steamLogout($response);
        }

        $winsToday = $statisticHelper->getWinsToday();

        try {
            if ($logged) {
                $steamUser = $this->steamManager->getUserData();

                $multiplies = $this->registryManager->getDefaultMultiplies();

                if (!$userHelper->ifUserExists([
                    'steamID' => $steamUser['steamid']
                ])) {
                    $userHelper->addUser($steamUser);
                }

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                if (isset($_SESSION['id'])) {
                    $userHelper->setIsArleadyLogged($user, $_SESSION['id']);
                    unset($_SESSION['id']);
                }

                if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                    session_destroy();
                }

                if ($user) {
                    $multiplyParams = [
                        'user' => $user,
                        'multiplies' => $multiplies
                    ];

                    if (!$userHelper->ifUserHasMultiply($multiplyParams)) {
                        $userHelper->addUserMultiply($multiplyParams);
                    }

                    $tradeLink = $userHelper->getTradelink($user);

                    $userID = $user->getId();

                    if ($banHelper->banExists([
                        'userID' => $userID
                    ])) {

                        /** @var Ban $ban */
                        $ban = $banHelper->getBan([
                            'userID' => $userID
                        ]);

                        $periodDateTimestamp = strtotime($ban->getPeriod()->format('d-m-Y H:i:s'));
                        $nowDateTimestamp = strtotime((new \DateTime())->format('d-m-Y H:i:s'));

                        if ($nowDateTimestamp > $periodDateTimestamp) {
                            $banHelper->removeBan([
                                'userID' => $userID
                            ]);
                        }
                    }

                    /** @var BufferManager $buffer */
                    $buffer = $this->container->get('buffer');

                    $buffer->setBuffer(USER_BUFFER_DEPOSIT_DIR . $steamUser['steamid'] . '.txt');

                    if ($buffer->isBuffer()) {
                        $bufferDepositData = $buffer->readBuffer();
                        $bufferDepositData = json_decode($bufferDepositData, true);
                        $mode = $bufferDepositData['mode'];

                        if ($mode === 'initializeTrade') {
                            $marketNames = $bufferDepositData['market_names'];

                            foreach ($marketNames as $marketName) {

                                /** @var Item $item */
                                $item = $itemHelper->getItem([
                                    'market_name' => $marketName
                                ]);

                                $userID = $user->getId();

                                $token = $userID . $marketName . $bufferDepositData['trade_id'];
                                $token = $this->securityManager->generateSecurityToken($token);

                                $depositParams = [
                                    'user' => $user,
                                    'trade' => $bufferDepositData['trade_id'],
                                    'item' => $item,
                                    'token' => $token,
                                    'status' => DepositStatusType::ACTIVE
                                ];

                                $depositHelper->addDeposit($depositParams);
                            }
                        } else if ($mode === 'cancelTrade') {

                            /** @var Deposit[] $deposits */
                            $deposits = $depositHelper->getPendingDeposits($user);

                            foreach ($deposits as $deposit) {
                                $depositHelper->deleteDeposit($deposit);
                            }
                        }

                        $buffer->removeBuffer();
                    }

                    /** @var BufferManager $buffer */
                    $buffer = $this->container->get('buffer');

                    $buffer->setBuffer(USER_BUFFER_WITHDRAWS_DIR . $steamUser['steamid'] . '.txt');

                    if ($buffer->isBuffer()) {
                        $bufferWithdrawData = $buffer->readBuffer();
                        $bufferWithdrawData = json_decode($bufferWithdrawData, true);
                        $mode = $bufferWithdrawData['mode'];


                        if ($mode === 'initializeWithdraw') {

                            /** @var Withdraw[] $withdraws */
                            $withdraws = $withdrawHelper->getPendingWithdraws($user);

                            if (strpos($bufferWithdrawData['message'], '[AFFILIATES]') !== false) {
                                $userID = $user->getId();
                                $userWallet = (double)$user->getWallet();
                                $marketNames = $bufferWithdrawData['market_names'];
                                $tradeID = $bufferWithdrawData['trade_id'];

                                $totalPrice = 0;

                                foreach ($marketNames as $marketName) {

                                    /** @var Item $item */
                                    $item = $itemHelper->getItem([
                                        'market_name' => $marketName
                                    ]);

                                    $itemPrice = (double)$item->getPrice();
                                    $totalPrice = $totalPrice + $itemPrice;

                                    $token = $userID . $marketName . $tradeID;
                                    $token = $this->securityManager->generateSecurityToken($token);
                                }

                                $totalPrice = $userWallet - $totalPrice;
                                $userHelper->updateWallet($user, $totalPrice);
                            } else {
                                foreach ($withdraws as $withdraw) {

                                    $deposit = $withdraw->getDeposit();

                                    $depositHelper->changeDepositStatus($deposit, DepositStatusType::UNACTIVE);

                                    $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::PAID);
                                }
                            }
                        } else if ($mode === 'cancelWithdraw') {

                            /** @var Withdraw[] $withdraws */
                            $withdraws = $withdrawHelper->getPendingWithdraws($user);

                            foreach ($withdraws as $withdraw) {
                                $deposit = $withdraw->getDeposit();

                                $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::UNPAID);
                                $depositHelper->changeDepositStatus($deposit, DepositStatusType::ACTIVE);
                            }

                            $buys = $withdrawHelper->getPendingBuys($user);

                            foreach ($buys as $buy) {
                                $withdrawHelper->changeBuyStatus($buy, BuyStatusType::UNPAID);
                            }
                        }

                        $buffer->removeBuffer();
                    }
                }
            }

            $errors = $success = [];

            if ($this->flashManager->has('errors')) {
                $errors = $this->flashManager->get('errors');
            }

            if ($this->flashManager->has('success')) {
                $success = $this->flashManager->get('success');
            }

            $hash = sha1(date('d/m/Y H:i:s') . '' . rand(0, 9999));

            $this->viewManager->render($response, 'home.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'errors' => $errors,
                'successes' => $success,
                'hash' => $hash,
                'tradelink' => $tradeLink,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return IndexController|Response
     */
    public function recentGamesAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $statisticsHelper = new StatisticsHelper($this->databaseManager);

            try {
                $upgrades = $statisticsHelper->getRecentGames([
                    'win' => GameStatusType::WIN
                ]);

                $template = $this->viewManager->dump('partials/upgrades-list.html.twig', [
                    'upgrades' => $upgrades
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return IndexController|Response
     */
    public function messageAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $messageHelper = new MessageHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);

            $logged = $this->steamManager->ifUserLogged();

            $user = [];

            if ($logged) {

                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);
            }

            try {
                $messages = $messageHelper->getMessages();

                $template = $this->viewManager->dump('partials/messages-list.html.twig', [
                    'messages' => $messages,
                    'logged' => $logged,
                    'user' => $user
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return IndexController|Response
     */
    public function depositAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $items = [];

                /** @var Deposit[] $depositCollection */
                $depositCollection = $user->getDeposits([
                    'status' => DepositStatusType::ACTIVE
                ]);

                foreach ($depositCollection as $deposit) {
                    $items[] = [
                        'data' => $deposit->getItem(),
                        'tradeID' => $deposit->getTradeID(),
                        'depositID' => $deposit->getId()
                    ];
                }

                $template = $this->viewManager->dump('partials/play-inventory.html.twig', [
                    'items' => $items
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
