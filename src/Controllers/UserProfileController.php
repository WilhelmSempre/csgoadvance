<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\Game;
use Slim\Http\Request;
use Slim\Http\Response;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use CSGOADVANCE\src\Entity\User;
use Interop\Container\ContainerInterface;
use Slim\Router;

/**
 * Class UserProfileController
 * @package CSGOADVANCE\src\Controllers
 */
final class UserProfileController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * UserProfileController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response|UserProfileController
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {

        /** @var Router $router */
        $router = $this->container->get('router');

        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);

        $logged = $this->steamManager->ifUserLogged();

        $user = $tradelink = $me = $upgrades = $currentUser = [];
        $profit = 0;

        try {
            if (isset($args['id'])) {

                /** @var User $currentUser */
                $currentUser = $userHelper->getUser([
                    'ID' => (int) $args['id']
                ]);

                if (!$currentUser) {
                    $this->flashManager->add('errors', 'User does not exists!');

                    return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
                }

                /** @var Game[] $upgrades */
                $upgrades = $currentUser->getGames();

                if ($logged) {

                    $steamUser = $this->steamManager->getUserData();

                    /** @var User $user */
                    $user = $userHelper->getUser([
                        'steamID' => $steamUser['steamid']
                    ]);

                    $me = $userHelper->isMe($currentUser, $user);

                    /** @var Game $upgrade */
                    foreach ($upgrades as $upgrade) {
                        if ($upgrade->getWin()) {
                            $profit = (double) $upgrade->getAfterItem()->getPrice() + $profit;
                        } else {
                            $profit = (double) $upgrade->getAfterItem()->getPrice() - $profit;
                        }
                    }
                }

                if ($me) {
                    $tradelink = $userHelper->getTradelink($user);
                    $upgrades = $user->getGames();
                }
            }

            $errors = $success = [];

            if ($this->flashManager->has('errors')) {
                $errors = $this->flashManager->get('errors');
            }

            if ($this->flashManager->has('success')) {
                $success = $this->flashManager->get('success');
            }

            $winsToday = $statisticHelper->getWinsToday();

            $this->viewManager->render($response, 'profile.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'current_user' => $currentUser,
                'tradelink' => $tradelink,
                'me' => $me,
                'errors' => $errors,
                'successes' => $success,
                'upgrades' => $upgrades,
                'profit' => $profit,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', 'System error. Contact to administrator!');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }


    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response|UserProfileController
     * @throws \Exception
     */
    public function updateSettings(Request $request = null, Response $response = null, $args = [])
    {
        $logged = $this->steamManager->ifUserLogged();


        /** @var Router $router */
        $router = $this->container->get('router');

        if (!$logged) {
            throw new \Exception('User have to be logged!');
        }

        $tradelink = trim($request->getParsedBodyParam('tradelink'));

        $userHelper = new UserHelper($this->databaseManager);

        $steamUser = $this->steamManager->getUserData();

        try {

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            $userHelper->validateTradelink($tradelink);

            $result = $userHelper->updateTradeLink($user, $tradelink);

            if ($result) {
                $this->flashManager->add('success', 'Tradelink updated!');
            }
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
        }

        return $response->withStatus(302)->withHeader('Location', $router->pathFor('user-profile', ['id' => $args['id']]));
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response|UserProfileController
     * @throws \Exception
     */
    public function justiceAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $beforeItemID = $request->getParsedBodyParam('beforeID');
            $afterItemID = $request->getParsedBodyParam('afterID');
            $user = $request->getParsedBodyParam('userID');
            $multiply = $request->getParsedBodyParam('multiply');
            $hash = $request->getParsedBodyParam('hash');

            if ($beforeItemID === '' || $afterItemID === '' || $user === '' || $multiply === '' || $hash === '') {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'Fill all fields!'
                ];
            } else {
                if (sha1($afterItemID . $beforeItemID  . $user . $multiply) === $hash) {
                    $results['success'] = [
                        'status' => true,
                        'text' => 'Upgrade is correct and safe!'
                    ];
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'Upgrade may be manipulated!'
                    ];
                }
            }

            echo json_encode($results);
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }
    }
}
