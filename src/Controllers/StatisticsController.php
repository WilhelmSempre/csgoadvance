<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use Interop\Container\ContainerInterface;

/**
 * Class StatisticsController
 * @package CSGOADVANCE\src\Controllers
 */
class StatisticsController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }
}
