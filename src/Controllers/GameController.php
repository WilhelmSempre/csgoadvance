<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\Affiliates;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Game;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\Multiply;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Entity\Withdraw;
use CSGOADVANCE\src\Helpers\AffiliatesHelper;
use CSGOADVANCE\src\Helpers\DepositHelper;
use CSGOADVANCE\src\Helpers\GameHelper;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use CSGOADVANCE\src\Types\WithdrawStatusType;
use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;
use CSGOADVANCE\src\Types\DepositStatusType;

/**
 * Class GameController
 * @package CSGOADVANCE\src\Controllers
 */
final class GameController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * DepositController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return GameController|Response
     */
    public function loadBeforeItemAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $itemHelper = new ItemHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();
            $multipliesEnabled = [];

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $multiplies = $user->getMultiplies();
                $itemID = $request->getParsedBodyParam('id');
                $tradeID = $request->getParsedBodyParam('trade');
                $depositID = $request->getParsedBodyParam('deposit');

                /** @var Item $item */
                $item = $itemHelper->getItem([
                    'ID' => $itemID
                ]);

                /** @var Multiply $multiply */
                foreach ($multiplies as $multiply) {
                    $price = $item->getPrice();
                    $multiplyValue = $multiply->getValue();

                    $gamePrice = $item->getPrice() - $item->getPrice() * 0.1;

                    if ($itemHelper->ifItemsExistsByPrice($gamePrice * $multiplyValue, 0.05, $itemID)) {
                        $multipliesEnabled[] = 1;
                    } else {
                        $multipliesEnabled[] = 0;
                    }
                }

                $template = $this->viewManager->dump('partials/box/before-item.html.twig', [
                    'item' => $item
                ]);

                $multiplies = $this->viewManager->dump('partials/multiply-buttons.html.twig', [
                    'multiplies' => $multiplies,
                    'enabled' => $multipliesEnabled
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template,
                    'tradeID' => $tradeID,
                    'depositID' => $depositID,
                    'multiplies' => $multiplies
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return GameController|Response
     */
    public function loadAfterItemAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $itemHelper = new ItemHelper($this->databaseManager);

            $beforeID = $request->getParsedBodyParam('before_id');
            $multiplyValue = $request->getParsedBodyParam('multiply');
            $chance = $request->getParsedBodyParam('chance');

            try {

                /** @var Item $item */
                $item = $itemHelper->getItem([
                    'ID' => $beforeID
                ]);

                $price = $item->getPrice() - $item->getPrice() * 0.1;

                $gamePrice = $price * $multiplyValue;

                /** @var Item[] $items */
                $items = $itemHelper->getItemsByPrice($price * $multiplyValue, '0.05');

                if (count($items) > 0) {
                    $random = rand(0, count($items) - 1);
                    $item = $items[$random];
                } else {
                    $item = $items[0];
                }

                $template = $this->viewManager->dump('partials/box/after-item.html.twig', [
                    'item' => $item
                ]);

                $results['success'] = [
                    'status' => true,
                    'data' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return GameController|Response
     */
    public function playAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $gameHelper = new GameHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);
            $affiliatesHelper = new AffiliatesHelper($this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);

            $chance = (float) $request->getParsedBodyParam('chance');
            $beforeID = $request->getParsedBodyParam('beforeID');
            $afterID = $request->getParsedBodyParam('afterID');
            $multiply = $request->getParsedBodyParam('multiply');
            $tradeID = $request->getParsedBodyParam('tradeID');
            $depositID = (int) $request->getParsedBodyParam('depositID');
            $hash = $request->getParsedBodyParam('hash');
            $token = $request->getParsedBodyParam('token');

            $steamUser = $this->steamManager->getUserData();

            try {
                $tokenPattern = $chance . $multiply . $tradeID . $beforeID . $afterID . $depositID . $hash;

                if (!$this->securityManager->compareSecurityToken($tokenPattern, $token)) {
                    throw new \Exception('Invalid token!');
                }

                $random = $gameHelper->getRandom($chance);
                $win = $gameHelper->getWinOrLose($random);

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);


                /** @var Deposit[] $deposits */
                $deposits = $user->getDeposits([
                    'ID' => $depositID
                ]);

                /** @var Item $beforeItem */
                $beforeItem = $itemHelper->getItem([
                    'ID' => $beforeID
                ]);

                /** @var Item $afterItem */
                $afterItem = $itemHelper->getItem([
                    'ID' => $afterID
                ]);

                /** @var Affiliates[] $affiliates */
                $affiliates = $affiliatesHelper->getAffiliates([
                    'userID' => $user->getId()
                ]);

                $beforeItemPrice = (double) $beforeItem->getPrice();
                $affiliatedUserProvision = (double) $this->registryManager->getAffiliatedUserProvision();

                foreach ($deposits as $deposit) {
                    if ($deposit->getStatus() !== DepositStatusType::ACTIVE) {
                        throw new \Exception('Deposit does not exists!');
                    }
                }

                if (count($affiliates) > 0 && $affiliates) {
                    foreach ($affiliates as $affiliate) {

                        /** @var User $promotor */
                        $promotor = $affiliate->getAffilatedUser();
                        $promotorID = $promotor->getId();

                        $promotorWallet = floatval($promotor->getWallet());
                        $promotorWallet = $promotorWallet + ($affiliatedUserProvision * $beforeItemPrice);

                        $userHelper->updateWallet($promotor, $promotorWallet);
                    }
                }

                foreach ($deposits as $deposit) {
                    $token = $chance . $beforeID . $afterID . $multiply . $tradeID . $win;
                    $token = $this->securityManager->generateSecurityToken($token);

                    $gameHelper->addGame([
                        'user' => $user,
                        'deposit' => $deposit,
                        'winOrLose' => $win,
                        'multiply' => $multiply,
                        'token' => $token,
                        'afterItem' => $afterItem,
                        'beforeItem' => $beforeItem
                    ]);

                    $depositID = $deposit->getId();

                    /** @var Withdraw $withdraw */
                    $withdraw = $withdrawHelper->getWithdraw([
                        'depositID' => $depositID
                    ]);

                    if ($withdraw) {
                        $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::PAID);
                    }

                    $depositHelper->changeDepositStatus($deposit, DepositStatusType::UNACTIVE);
                }

                $afterItemPrice = (double) $afterItem->getPrice();

                $results['success'] = [
                    'status' => true,
                    'data' => [
                        'win' => $win,
                        'afterID' => $afterID,
                        'afterItemPrice' => $afterItemPrice,
                        'beforeID' => $beforeID,
                        'tradeID' => $tradeID,
                        'depositID' => $depositID
                    ]
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            } catch (\Exception $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return GameController|Response
     */
    public function winAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $itemHelper = new ItemHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);
            $gameHelper = new GameHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $afterID = $request->getParsedBodyParam('after_id');
            $beforeID = $request->getParsedBodyParam('before_id');
            $tradeID = $request->getParsedBodyParam('trade_id');
            $depositID = $request->getParsedBodyParam('deposit_id');

            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                /** @var Item $afterItem */
                $afterItem = $itemHelper->getItem([
                    'ID' => $afterID
                ]);

                /** @var Item $beforeItem */
                $beforeItem = $itemHelper->getItem([
                    'ID' => $beforeID
                ]);

                /** @var Deposit $deposit */
                $deposit = $depositHelper->getDeposit([
                    'ID' => $depositID
                ]);

                /** @var Game $game */
                $game = $gameHelper->getGame([
                    'depositID' => $depositID
                ]);

                $gameID = $game->getId();
                $userID = $game->getId();

                $token = $userID . $afterID . $tradeID;
                $token = $this->securityManager->generateSecurityToken($token);

                $gameHelper->saveWon([
                    'user' => $user,
                    'game' => $game,
                    'deposit' => $deposit,
                    'beforeItem' => $beforeItem,
                    'afterItem' => $afterItem,
                    'tradeID' => $tradeID,
                    'token' => $token
                ], $this->securityManager);

                $results['success'] = [
                    'status' => true
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return GameController|Response
     */
    public function tokenAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {

            $beforeID = $request->getParsedBodyParam('before_id');
            $afterID = $request->getParsedBodyParam('after_id');
            $tradeID = $request->getParsedBodyParam('trade_id');
            $multiply = $request->getParsedBodyParam('multiply');
            $depositID = $request->getParsedBodyParam('deposit_id');
            $chance = $request->getParsedBodyParam('chance');
            $hash = $request->getParsedBodyParam('hash');

            try {
                $token = $chance . $multiply . $tradeID . $beforeID . $afterID . $depositID . $hash;
                $token = $this->securityManager->generateSecurityToken($token);

                $results['success'] = [
                    'status' => true,
                    'token' => $token
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
