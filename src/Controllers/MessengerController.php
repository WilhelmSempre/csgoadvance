<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\core\Managers\MessengerCommandsManager;
use CSGOADVANCE\src\Entity\Ban;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Helpers\MessageHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;

/**
 * Class MessengerController
 * @package CSGOADVANCE\src\Controllers
 */
final class MessengerController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return MessengerController|Response
     */
    public function addAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $messageHelper = new MessageHelper($this->databaseManager);
            $userHelper = new UserHelper($this->databaseManager);

            try {
                $messageText = $request->getParsedBodyParam('text');

                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                /** @var Ban[] $bans */
                $bans = $user->getBans();

                if (count($bans) > 0) {
                    $results['errors'] = [
                        'status' => true,
                        'text' => sprintf('You are banned to %s for reason %s !', $bans[0]->getPeriod()->format('m-d-Y H:i:s'), $bans[0]->getReason()),
                        'command' => false
                    ];
                } else {

                    /** @var MessengerCommandsManager $commandManager */
                    $commandManager = $this->container->get('commands');

                    $commands = $commandManager->parseMessageForCommands($messageText, $user);

                    if (!$commands['commands']) {
                        $messageHelper->addMessage($messageText, $user);

                        $results['success'] = [
                            'status' => true,
                            'command' => false
                        ];
                    } else {
                        if (!$commands['error']) {
                            $results['success'] = [
                                'status' => true,
                                'text' => $commands['command_output'],
                                'command' => true
                            ];
                        } else {
                            $results['errors'] = [
                                'status' => true,
                                'text' => $commands['command_output'],
                                'command' => true
                            ];
                        }
                    }
                }
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage(),
                    'command' => false
                ];
            }

            echo json_encode($results);
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
