<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Helpers\DepositHelper;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use CSGOADVANCE\src\Types\DepositStatusType;
use Interop\Container\ContainerInterface;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Router;

/**
 * Class DepositController
 * @package CSGOADVANCE\src\Controllers
 */
final class DepositController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * DepositController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return DepositController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {

        /** @var Router $router */
        $router = $this->container->get('router');

        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);

        try {
            $logged = $this->steamManager->ifUserLogged();

            if (!$logged) {
                throw new \Exception('User have to be logged!');
            }

            $steamUser = $this->steamManager->getUserData();

            $winsToday = $statisticHelper->getWinsToday();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                session_destroy();
            }


            $tradeLink = $userHelper->ifUserHasTradelink($user);

            if (!$tradeLink) {
                throw new \Exception('Specify tradelink!');
            }

            $tradeLink = $userHelper->getTradelink($user);

            $this->viewManager->render($response, 'deposit.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'tradelink' => $tradeLink,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return DepositController|Response
     */
    public function depositAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $withdrawHelper = new WithdrawHelper($this->databaseManager);
            $steamUser = $this->steamManager->getUserData();

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                if (!$userHelper->ifUserHasTradelink($user)) {
                    throw new \Exception('Specify trade link!');
                }

                $ids = $request->getParsedBodyParam('id');

                if (!$ids) {
                    throw new \Exception('You have to choose one item!');
                }

                $tokens = $request->getParsedBodyParam('token');
                $marketNames = [];

                if (!$depositHelper->ifPendingDepositExists($user) &&
                    !$withdrawHelper->ifPendingWithdrawExists($user) &&
                    !$withdrawHelper->ifPendingBuysExists($user)) {

                    foreach ($ids as $id) {

                        /** @var Item $item */
                        $item = $itemHelper->getItem([
                            'ID' => $id
                        ]);

                        $marketName = $item ? $item->getMarketName() : '';

                        $token = isset($tokens[$id]) ? $tokens[$id] : '';

                        $itemPrice = $item ? $item->getPrice() : '';
                        $itemDepositPriceLimit = $this->registryManager->getDepositPriceLimit();

                        if ($itemPrice < $itemDepositPriceLimit) {
                            throw new \Exception('You trying deposit item less 0.30$. Choose another item below this cost!');
                        }

                        if (!$this->securityManager->compareSecurityToken($marketName, $token)) {
                            throw new \Exception('Invalid token!');
                        }

                        $marketNames[] = $marketName;

                        $depositParams = [
                            'user' => $user,
                            'trade' => 0,
                            'item' => $item,
                            'token' => 0
                        ];

                        $depositHelper->addDeposit($depositParams);
                    }

                    $results['success'] = [
                        'status' => true,
                        'text' => $userHelper->getTradelink($user),
                        'market_names' => $marketNames
                    ];

                    echo json_encode($results);
                } else {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'You are pending trade request!'
                    ];

                    echo json_encode($results);
                }
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            } catch (\Exception $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => $error->getMessage()
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return DepositController|Response
     * @throws \Exception
     */
    public function tradeAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            $marketNames = $request->getParsedBodyParam('market_names');
            $tradeID = $request->getParsedBodyParam('trade_id');

            try {

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                foreach ($marketNames as $marketName) {

                    /** @var Item $item */
                    $item = $itemHelper->getItem([
                        'market_name' => $marketName
                    ]);

                    $userID = $user->getId();

                    $token = $userID . $marketName . $tradeID;
                    $token = $this->securityManager->generateSecurityToken($token);

                    /** @var Deposit[] $deposits */
                    $deposits = $depositHelper->getPendingDeposits($user);

                    foreach ($deposits as $deposit) {
                        $depositHelper->changeDepositStatus($deposit, DepositStatusType::ACTIVE);
                        $depositHelper->changeTradeID($deposit, $tradeID);
                        $depositHelper->changeDepositToken($deposit, $token);
                    }
                }

                $results['success'] = [
                    'status' => true
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container
                ->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return DepositController|Response
     */
    public function inventoryAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);
            $itemHelper = new ItemHelper($this->databaseManager);
            $steamUser = $this->steamManager->getUserData();

            try {
                $refresh = (bool) $request->getParsedBodyParam('refresh');

                $userInventoryData = $depositHelper->getUserInventory($steamUser['steamid'], $refresh);
                $items = [];

                foreach ($userInventoryData as $userInventory) {
                    $userInventory['token'] = $this->securityManager->generateSecurityToken($userInventory['market_name']);

                    if (!$itemHelper->ifItemExists([
                        'market_name' => $userInventory['market_name']
                    ])) {
                        $itemHelper->addItem($userInventory);
                    }

                    $items[] = $itemHelper->getItem([
                        'market_name' => $userInventory['market_name']
                    ]);
                }

                $sort = (bool) $request->getParsedBodyParam('sort');
                $sortBy = $request->getParsedBodyParam('sortby');
                $order = $request->getParsedBodyParam('order');
                $searchName = $request->getParsedBodyParam('name');

                if ($sort) {
                    if ($sortBy) {
                        switch ($sortBy) {
                            case 'name':
                                usort($items, function(Item $a, Item $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getName() > $b->getName() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getName() > $b->getName() ? 1 : -1;
                                    }
                                });
                            break;
                            case 'price':
                                usort($items, function(Item $a, Item $b) use ($order) {
                                    if ($order === 'desc') {
                                        return $a->getPrice() > $b->getPrice() ? -1 : 1;
                                    } elseif ($order === 'asc') {
                                        return $a->getPrice() > $b->getPrice() ? 1 : -1;
                                    }
                                });
                            break;
                        }
                    }

                    if ($searchName) {
                        if ($searchName !== '') {
                            $items = array_filter($items, function (Item $item) use ($searchName) {
                                return preg_match('/' . $searchName . '/i', $item->getName());
                            });
                        }
                    }

                } else {
                    usort($items, function(Item $a, Item $b) use ($order) {
                        return $a->getName() > $b->getName() ? 1 : -1;
                    });
                }

                $template = $this->viewManager->dump('partials/deposit-inventory.html.twig', [
                    'items' => $items
                ]);

                $results['success'] = [
                    'status' => true,
                    'template' => $template
                ];

                echo json_encode($results);
            } catch (\PDOException $error) {
                $results['errors'] = [
                    'status' => true,
                    'text' => 'System error. Contact to administrator!'
                ];

                echo json_encode($results);
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return DepositController|Response
     */
    public function cancelAction(Request $request = null, Response $response = null, $args = [])
    {
        if ($request->isXhr()) {
            $userHelper = new UserHelper($this->databaseManager);
            $depositHelper = new DepositHelper($this->container, $this->databaseManager);

            $steamUser = $this->steamManager->getUserData();

            /** @var User $user */
            $user = $userHelper->getUser([
                'steamID' => $steamUser['steamid']
            ]);

            /** @var Deposit[] $deposits */
            $deposits = $depositHelper->getPendingDeposits($user);

            foreach ($deposits as $deposit) {
                try {
                    $depositHelper->deleteDeposit($deposit);

                    $results['success'] = [
                        'status' => true
                    ];

                    echo json_encode($results);
                } catch (\PDOException $error) {
                    $results['errors'] = [
                        'status' => true,
                        'text' => 'System error. Contact to administrator!'
                    ];

                    echo json_encode($results);
                }
            }
        } else {

            /** @var Router $router */
            $router = $this->container->get('router');

            $this->flashManager->add('errors', 'Unknown error');

            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }

        return $this;
    }
}
