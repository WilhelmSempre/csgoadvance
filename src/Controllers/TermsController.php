<?php

namespace CSGOADVANCE\src\Controllers;

use CSGOADVANCE\core\Managers\BufferManager;
use CSGOADVANCE\core\Controller;
use CSGOADVANCE\src\Entity\Ban;
use CSGOADVANCE\src\Helpers\BanHelper;
use CSGOADVANCE\src\Types\BuyStatusType;
use CSGOADVANCE\src\Helpers\StatisticsHelper;
use Slim\Http\Request;
use Slim\Http\Response;
use CSGOADVANCE\src\Helpers\UserHelper;
use CSGOADVANCE\src\Entity\User;
use Interop\Container\ContainerInterface;
use CSGOADVANCE\src\Entity\Withdraw;
use CSGOADVANCE\core\Buffer;
use CSGOADVANCE\src\Helpers\ItemHelper;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Helpers\DepositHelper;
use CSGOADVANCE\src\Helpers\WithdrawHelper;
use Slim\Router;

/**
 * Class TremsController
 * @package CSGOADVANCE\src\Controllers
 */
final class TermsController extends Controller
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * IndexController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return TermsController|Response
     */
    public function indexAction(Request $request = null, Response $response = null, $args = [])
    {
        $userHelper = new UserHelper($this->databaseManager);
        $statisticHelper = new StatisticsHelper($this->databaseManager);
        $withdrawHelper = new WithdrawHelper($this->databaseManager);
        $itemHelper = new ItemHelper($this->databaseManager);
        $depositHelper = new DepositHelper($this->container, $this->databaseManager);
        $banHelper = new BanHelper($this->databaseManager);

        $logged = $this->steamManager->ifUserLogged();
        $user = [];

        /** @var Router $router */
        $router = $this->container->get('router');

        if (array_key_exists('login', $request->getQueryParams())) {
            return $this->steamManager->steamLogin($response);
        }

        if (array_key_exists('logout', $request->getQueryParams())) {

            if ($logged) {
                $steamUser = $this->steamManager->getUserData();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                $userHelper->setIsArleadyLogged($user, null);
            }

            return $this->steamManager->steamLogout($response);
        }

        $winsToday = $statisticHelper->getWinsToday();

        try {
            if ($logged) {
                $steamUser = $this->steamManager->getUserData();

                $multiplies = $this->registryManager->getDefaultMultiplies();

                /** @var User $user */
                $user = $userHelper->getUser([
                    'steamID' => $steamUser['steamid']
                ]);

                if (isset($_SESSION['id'])) {
                    $userHelper->setIsArleadyLogged($user, $_SESSION['id']);
                    unset($_SESSION['id']);
                }

                if (!$userHelper->checkIfIsYouArleadyLogged($user)) {
                    session_destroy();
                }
                $userID = $user->getId();

                if ($banHelper->banExists([
                    'userID' => $userID
                ])) {

                    /** @var Ban $ban */
                    $ban = $banHelper->getBan([
                        'userID' => $userID
                    ]);

                    $periodDateTimestamp = $ban->getPeriod()->getTimestamp();
                    $nowDateTimestamp = (new \DateTime())->getTimestamp();

                    if ($nowDateTimestamp > $periodDateTimestamp) {
                        $banHelper->removeBan([
                            'userID' => $userID
                        ]);
                    }
                }

                /** @var BufferManager $buffer */
                $buffer = $this->container->get('buffer');

                $buffer->setBuffer(USER_BUFFER_DEPOSIT_DIR . $steamUser['steamid'] . '.txt');

                if ($buffer->isBuffer()) {
                    $bufferDepositData = $buffer->readBuffer();
                    $bufferDepositData = json_decode($bufferDepositData, true);
                    $mode = $bufferDepositData['mode'];

                    if ($mode === 'initializeTrade') {
                        $marketNames = $bufferDepositData['market_names'];

                        foreach ($marketNames as $marketName) {

                            /** @var Item $item */
                            $item = $itemHelper->getItem([
                                'market_name' => $marketName
                            ]);

                            $userID = $user->getId();

                            $token = $userID . $marketName . $bufferDepositData['trade_id'];
                            $token = $this->securityManager->generateSecurityToken($token);

                            $depositParams = [
                                'user' => $user,
                                'trade' => $bufferDepositData['trade_id'],
                                'item' => $item,
                                'token' => $token,
                                'status' => DepositStatusType::ACTIVE
                            ];

                            $depositHelper->addDeposit($depositParams);
                        }
                    } else if ($mode === 'cancelTrade') {

                        /** @var Deposit[] $deposits */
                        $deposits = $depositHelper->getPendingDeposits($user);

                        foreach ($deposits as $deposit) {
                            $depositHelper->deleteDeposit($deposit);
                        }
                    }

                    $buffer->removeBuffer();
                }

                /** @var BufferManager $buffer */
                $buffer = $this->container->get('buffer');

                $buffer->setBuffer(USER_BUFFER_WITHDRAWS_DIR . $steamUser['steamid'] . '.txt');

                if ($buffer->isBuffer()) {
                    $bufferWithdrawData = $buffer->readBuffer();
                    $bufferWithdrawData = json_decode($bufferWithdrawData, true);
                    $mode = $bufferWithdrawData['mode'];


                    if ($mode === 'initializeWithdraw') {

                        /** @var Withdraw[] $withdraws */
                        $withdraws = $withdrawHelper->getPendingWithdraws($user);

                        if (strpos($bufferWithdrawData['message'], '[AFFILIATES]') !== false) {
                            $userID = $user->getId();
                            $userWallet = (double)$user->getWallet();
                            $marketNames = $bufferWithdrawData['market_names'];
                            $tradeID = $bufferWithdrawData['trade_id'];

                            $totalPrice = 0;

                            foreach ($marketNames as $marketName) {

                                /** @var Item $item */
                                $item = $itemHelper->getItem([
                                    'market_name' => $marketName
                                ]);

                                $itemPrice = (double)$item->getPrice();
                                $totalPrice = $totalPrice + $itemPrice;

                                $token = $userID . $marketName . $tradeID;
                                $token = $this->securityManager->generateSecurityToken($token);
                            }

                            $totalPrice = $userWallet - $totalPrice;
                            $userHelper->updateWallet($user, $totalPrice);
                        } else {
                            foreach ($withdraws as $withdraw) {

                                $deposit = $withdraw->getDeposit();

                                $depositHelper->changeDepositStatus($deposit, DepositStatusType::UNACTIVE);

                                $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::PAID);
                            }
                        }
                    } else if ($mode === 'cancelWithdraw') {

                        /** @var Withdraw[] $withdraws */
                        $withdraws = $withdrawHelper->getPendingWithdraws($user);

                        foreach ($withdraws as $withdraw) {
                            $deposit = $withdraw->getDeposit();

                            $withdrawHelper->changeWithdrawStatus($withdraw, WithdrawStatusType::UNPAID);
                            $depositHelper->changeDepositStatus($deposit, DepositStatusType::ACTIVE);
                        }

                        $buys = $withdrawHelper->getPendingBuys($user);

                        foreach ($buys as $buy) {
                            $withdrawHelper->changeBuyStatus($buy, BuyStatusType::UNPAID);
                        }
                    }

                    $buffer->removeBuffer();
                }
            }

            $this->viewManager->render($response, 'terms.html.twig', [
                'logged' => $logged,
                'user' => $user,
                'winstoday' => $winsToday
            ]);
        } catch (\PDOException $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        } catch (\Exception $error) {
            $this->flashManager->add('errors', $error->getMessage());
            return $response->withStatus(302)->withHeader('Location', $router->pathFor('index'));
        }
        return $this;
    }
}
