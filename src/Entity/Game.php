<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Game
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\GameRepository")
 * @ORM\Table(name="Game")
 */
class Game
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Deposit")
     */
    private $deposit;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="boolean")
     */
    private $win;

    /**
     * @ORM\Column(type="decimal")
     */
    private $multiply;

    /**
     * @ORM\OneToMany(targetEntity="Withdraw", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $withdraws;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     */
    private $beforeItem;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     */
    private $afterItem;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->withdraws = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User|integer $user
     * @return Game
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set deposit
     *
     * @param Deposit|integer $deposit
     * @return Game
     */
    public function setDeposit(Deposit $deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return Deposit
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Game
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set win
     *
     * @param boolean $win
     * @return Game
     */
    public function setWin($win)
    {
        $this->win = $win;

        return $this;
    }

    /**
     * Get win
     *
     * @return boolean
     */
    public function getWin()
    {
        return $this->win;
    }

    /**
     * Set multiply
     *
     * @param float $multiply
     * @return Game
     */
    public function setMultiply($multiply)
    {
        $this->multiply = $multiply;

        return $this;
    }

    /**
     * Get multiply
     *
     * @return float
     */
    public function getMultiply()
    {
        return $this->multiply;
    }

    /**
     * Add withdraws
     *
     * @param Withdraw $withdraws
     * @return Game
     */
    public function addWithdraw(Withdraw $withdraws)
    {
        $this->withdraws[] = $withdraws;

        return $this;
    }

    /**
     * Remove withdraws
     *
     * @param Withdraw $withdraws
     */
    public function removeWithdraw(Withdraw $withdraws)
    {
        $this->withdraws->removeElement($withdraws);
    }

    /**
     * Get withdraws
     *
     * @return Collection|Withdraw[]
     */
    public function getWithdraws()
    {
        return $this->withdraws;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Game
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set beforeItem
     *
     * @param Item $beforeItem
     * @return Game
     */
    public function setBeforeItem(Item $beforeItem = null)
    {
        $this->beforeItem = $beforeItem;

        return $this;
    }

    /**
     * Get beforeItem
     *
     * @return Item
     */
    public function getBeforeItem()
    {
        return $this->beforeItem;
    }

    /**
     * Set afterItem
     *
     * @param Item $afterItem
     * @return Game
     */
    public function setAfterItem(Item $afterItem = null)
    {
        $this->afterItem = $afterItem;

        return $this;
    }

    /**
     * Get afterItem
     *
     * @return Item
     */
    public function getAfterItem()
    {
        return $this->afterItem;
    }
}
