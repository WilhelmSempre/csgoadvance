<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Affiliates
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\AffiliatesRepository")
 * @ORM\Table(name="Affiliates")
 */
class Affiliates
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $affilatedUser;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Affiliates
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Affiliates
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set affilatedUser
     *
     * @param User $affilatedUser
     * @return Affiliates
     */
    public function setAffilatedUser(User $affilatedUser = null)
    {
        $this->affilatedUser = $affilatedUser;

        return $this;
    }

    /**
     * Get affilatedUser
     *
     * @return User
     */
    public function getAffilatedUser()
    {
        return $this->affilatedUser;
    }
}
