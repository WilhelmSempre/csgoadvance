<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Item
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\ItemRepository")
 * @ORM\Table(name="Item")
 */
class Item
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marketName;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $icon;

    /**
     * @ORM\OneToMany(targetEntity="Deposit", mappedBy="item", cascade={"persist"}, orphanRemoval=true)
     */
    private $deposits;

    /**
     * @ORM\OneToMany(targetEntity="Withdraw", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $withdraws;

    /**
     * @ORM\OneToMany(targetEntity="Buy", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $buys;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deposits = new ArrayCollection();
        $this->withdraws = new ArrayCollection();
        $this->buys = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Item
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set marketName
     *
     * @param string $marketName
     * @return Item
     */
    public function setMarketName($marketName)
    {
        $this->marketName = $marketName;

        return $this;
    }

    /**
     * Get marketName
     *
     * @return string
     */
    public function getMarketName()
    {
        return $this->marketName;
    }

    /**
     * Add deposits
     *
     * @param Deposit $deposits
     * @return Item
     */
    public function addDeposit(Deposit $deposits)
    {
        $deposits->setItem($this);

        $this->deposits[] = $deposits;

        return $this;
    }

    /**
     * Remove deposits
     *
     * @param Deposit $deposits
     */
    public function removeDeposit(Deposit $deposits)
    {
        $this->deposits
            ->removeElement($deposits);
    }

    /**
     * Get deposits
     *
     * @param array $params
     * @return Collection|Deposit[]
     */
    public function getDeposits($params = [])
    {
        if (array_key_exists('user', $params)) {
            $user = $params['user']->getId();

            return $this->deposits->filter(function (Deposit $deposit) use ($user) {
                return $deposit->getUser()->getId() === $user;
            });
        }

        return $this->deposits;
    }

    /**
     * Add withdraws
     *
     * @param Withdraw $withdraws
     * @return Item
     */
    public function addWithdraw(Withdraw $withdraws)
    {
        $this->withdraws[] = $withdraws;

        return $this;
    }

    /**
     * Remove withdraws
     *
     * @param Withdraw $withdraws
     */
    public function removeWithdraw(Withdraw $withdraws)
    {
        $this->withdraws->removeElement($withdraws);
    }

    /**
     * Get withdraws
     *
     * @return Collection|Withdraw[]
     */
    public function getWithdraws()
    {
        return $this->withdraws;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Item
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        $price = number_format($this->price, 2);
        return $price;
    }

    /**
     * Add buys
     *
     * @param Buy $buys
     * @return Item
     */
    public function addBuy(Buy $buys)
    {
        $this->buys[] = $buys;

        return $this;
    }

    /**
     * Remove buys
     *
     * @param Buy $buys
     */
    public function removeBuy(Buy $buys)
    {
        $this->buys->removeElement($buys);
    }

    /**
     * Get buys
     *
     * @return Collection
     */
    public function getBuys()
    {
        return $this->buys;
    }
}
