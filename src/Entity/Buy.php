<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\ORM\Mapping as ORM;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;

/**
* Class Buy
* @package CSGOADVANCE\src\Entity
*
* @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\BuyRepository")
* @ORM\Table(name="Buy")
*/
class Buy
{

    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy = "AUTO")
    */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="buys", cascade={"persist"})
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="buys", cascade={"persist"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
    * @ORM\Column(type="datetime")
    */
    private $created;

    /**
    * @ORM\Column(type="integer", length=1)
    */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Buy
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Buy
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Buy
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set item
     *
     * @param Item $item
     * @return Buy
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
