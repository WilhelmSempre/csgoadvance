<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Multiply
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\MultiplyRepository")
 * @ORM\Table(name="Multiply")
 */
class Multiply
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $value;

    /**
     * @ORM\Column(type="decimal",scale=2)
     */
    private $chance;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Multiply
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Multiply
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set chance
     *
     * @param string $chance
     * @return Multiply
     */
    public function setChance($chance)
    {
        $this->chance = $chance;

        return $this;
    }

    /**
     * Get chance
     *
     * @return string
     */
    public function getChance()
    {
        return $this->chance;
    }
}
