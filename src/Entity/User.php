<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\UserRepository")
 * @ORM\Table(name="User")
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=24)
     */
    private $steamid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $realname;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $wallet;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tradelink;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="Deposit", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $deposits;

    /**
     * @ORM\OneToMany(targetEntity="Multiply", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $multiplies;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $games;

    /**
     * @ORM\OneToMany(targetEntity="Withdraw", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $withdraws;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="Affiliates", mappedBy="affilatedUser", cascade={"persist"}, orphanRemoval=true)
     */
    private $affiliates;

    /**
     * @ORM\OneToMany(targetEntity="Ban", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $bans;

    /**
     * @ORM\OneToMany(targetEntity="Buy", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
     */
    private $buys;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referral;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isLogged;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->deposits = new ArrayCollection();
        $this->multiplies = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->withdraws = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->affiliates = new ArrayCollection();
        $this->bans = new ArrayCollection();
        $this->buys = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set steamid
     *
     * @param string $steamid
     * @return User
     */
    public function setSteamid($steamid)
    {
        $this->steamid = $steamid;

        return $this;
    }

    /**
     * Get steamid
     *
     * @return string
     */
    public function getSteamid()
    {
        return $this->steamid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set realname
     *
     * @param string $realname
     * @return User
     */
    public function setRealname($realname)
    {
        $this->realname = $realname;

        return $this;
    }

    /**
     * Get realname
     *
     * @return string
     */
    public function getRealname()
    {
        return $this->realname;
    }

    /**
     * Set created
     *
     * @param string $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set tradelink
     *
     * @param string $tradelink
     * @return User
     */
    public function setTradelink($tradelink)
    {
        $this->tradelink = $tradelink;

        return $this;
    }

    /**
     * Get tradelink
     *
     * @return string
     */
    public function getTradelink()
    {
        return $this->tradelink;
    }

    /**
     * Set role
     *
     * @param integer $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add deposits
     *
     * @param Deposit $deposits
     * @return User
     */
    public function addDeposit(Deposit $deposits)
    {
        $deposits->setUser($this);

        $this->deposits[] = $deposits;

        return $this;
    }

    /**
     * Remove deposits
     *
     * @param Deposit $deposits
     */
    public function removeDeposit(Deposit $deposits)
    {
        $this->deposits->removeElement($deposits);
    }

    /**
     * Get deposits
     *
     * @param array $params
     * @return Collection|Deposit[]
     */
    public function getDeposits($params = [])
    {
        if (array_key_exists('ID', $params) && array_key_exists('status', $params)) {
            $id = $params['ID'];
            $status = $params['status'];

            return $this->deposits->filter(function (Deposit $deposit) use ($id, $status) {
                return $deposit->getId() === $id && $deposit->getStatus() === $status;
            });
        } else if (array_key_exists('ID', $params)) {
            $id = $params['ID'];

            return $this->deposits->filter(function (Deposit $deposit) use ($id) {
                return $deposit->getId() === $id;
            });
        } elseif (array_key_exists('status', $params)) {
            $status = $params['status'];

            return $this->deposits->filter(function (Deposit $deposit) use ($status) {
                return $deposit->getStatus() === $status;
            });
        } elseif (array_key_exists('market_name', $params)) {
            $marketName = $params['market_name'];

            return $this->deposits->filter(function (Deposit $deposit) use ($marketName) {
                return $deposit->getItem()->getMarketName() === $marketName;
            });
        } elseif (array_key_exists('tradeID', $params)) {
            $tradeID = $params['tradeID'];

            return $this->deposits->filter(function (Deposit $deposit) use ($tradeID) {
                return $deposit->getTradeID() === $tradeID;
            });
        }

        return $this->deposits;
    }

    /**
     * Add multiplies
     *
     * @param Multiply $multiplies
     * @return User
     */
    public function addMultiply(Multiply $multiplies)
    {
        $this->multiplies[] = $multiplies;

        return $this;
    }

    /**
     * Remove multiplies
     *
     * @param Multiply $multiplies
     */
    public function removeMultiply(Multiply $multiplies)
    {
        $this->multiplies->removeElement($multiplies);
    }

    /**
     * Get multiplies
     *
     * @return Collection|Multiply[]
     */
    public function getMultiplies()
    {
        return $this->multiplies;
    }

    /**
     * Add games
     *
     * @param Game $games
     * @return User
     */
    public function addGame(Game $games)
    {
        $this->games[] = $games;

        return $this;
    }

    /**
     * Remove games
     *
     * @param Game $games
     */
    public function removeGame(Game $games)
    {
        $this->games->removeElement($games);
    }

    /**
     * Get games
     *
     * @return Collection|Game[]
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Add withdraws
     *
     * @param Withdraw $withdraws
     * @return User
     */
    public function addWithdraw(Withdraw $withdraws)
    {
        $this->withdraws[] = $withdraws;

        return $this;
    }

    /**
     * Remove withdraws
     *
     * @param Withdraw $withdraws
     */
    public function removeWithdraw(Withdraw $withdraws)
    {
        $this->withdraws->removeElement($withdraws);
    }

    /**
     * Get withdraws
     *
     * @param array $withdrawParams
     * @return Collection|Withdraw[]
     */
    public function getWithdraws($withdrawParams = [])
    {
        if (array_key_exists('status', $withdrawParams)) {
            $status = $withdrawParams['status'];

            return $this->withdraws->filter(function (Withdraw $withdraw) use ($status) {
                return $withdraw->getStatus() === $status;
            });
        }

        return $this->withdraws;
    }

    /**
     * Add messages
     *
     * @param Message $messages
     * @return User
     */
    public function addMessage(Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param Message $messages
     */
    public function removeMessage(Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @param array $messagesParams
     * @return Message[]|Collection
     */
    public function getMessages($messagesParams = [])
    {
        if (array_key_exists('status', $messagesParams)) {
            $status = $messagesParams['status'];

            return $this->messages->filter(function (Message $message) use ($status) {
                return $message->getStatus() === $status;
            });
        }

        return $this->messages;
    }

    /**
     * Set referral
     *
     * @param string $referral
     * @return User
     */
    public function setReferral($referral)
    {
        $this->referral = $referral;

        return $this;
    }

    /**
     * Get referral
     *
     * @return string 
     */
    public function getReferral()
    {
        return $this->referral;
    }

    /**
     * Add affiliates
     *
     * @param Affiliates $affiliates
     * @return User
     */
    public function addAffiliate(Affiliates $affiliates)
    {
        $this->affiliates[] = $affiliates;

        return $this;
    }

    /**
     * Remove affiliates
     *
     * @param Affiliates $affiliates
     */
    public function removeAffiliate(Affiliates $affiliates)
    {
        $this->affiliates->removeElement($affiliates);
    }

    /**
     * Get affiliates
     *
     * @return Collection
     */
    public function getAffiliates()
    {
        return $this->affiliates;
    }

    /**
     * Set wallet
     *
     * @param string $wallet
     * @return User
     */
    public function setWallet($wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * Get wallet
     *
     * @return string
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * Add bans
     *
     * @param Ban $bans
     * @return User
     */
    public function addBan(Ban $bans)
    {
        $this->bans[] = $bans;

        return $this;
    }

    /**
     * Remove bans
     *
     * @param Ban $bans
     */
    public function removeBan(Ban $bans)
    {
        $this->bans->removeElement($bans);
    }

    /**
     * Get bans
     *
     * @return Collection
     */
    public function getBans()
    {
        return $this->bans;
    }

    /**
     * Set isLogged
     *
     * @param string $isLogged
     * @return User
     */
    public function setIsLogged($isLogged)
    {
        $this->isLogged = $isLogged;

        return $this;
    }

    /**
     * Get isLogged
     *
     * @return string
     */
    public function getIsLogged()
    {
        return $this->isLogged;
    }

    /**
     * Add buys
     *
     * @param Buy $buys
     * @return User
     */
    public function addBuy(Buy $buys)
    {
        $this->buys[] = $buys;

        return $this;
    }

    /**
     * Remove buys
     *
     * @param Buy $buys
     */
    public function removeBuy(Buy $buys)
    {
        $this->buys->removeElement($buys);
    }

    /**
     * Get buys
     *
     * @return Collection
     */
    public function getBuys()
    {
        return $this->buys;
    }
}
