<?php

namespace CSGOADVANCE\src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Deposit
 * @package CSGOADVANCE\src\Entity
 *
 * @ORM\Entity(repositoryClass="CSGOADVANCE\src\Repository\DepositRepository")
 * @ORM\Table(name="Deposit")
 */
class Deposit
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy = "AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="deposits", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="deposits", cascade={"persist"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="deposit", cascade={"persist"}, orphanRemoval=true)
     */
    private $games;

    /**
     * @ORM\OneToMany(targetEntity="Withdraw", mappedBy="deposit", cascade={"persist"}, orphanRemoval=true)
     */
    private $withdraws;

    /**
     * @ORM\Column(type="bigint")
     */
    private $tradeID;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->withdraws = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Deposit
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param User|int $user
     * @return Deposit
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Item
     * @return $this
     */
    public function setItem(Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Deposit
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tradeID
     *
     * @param integer $tradeID
     * @return Deposit
     */
    public function setTradeID($tradeID)
    {
        $this->tradeID = $tradeID;

        return $this;
    }

    /**
     * Get tradeID
     *
     * @return integer
     */
    public function getTradeID()
    {
        return $this->tradeID;
    }

    /**
     * Add games
     *
     * @param Game $games
     * @return Deposit
     */
    public function addGame(Game $games)
    {
        $this->games[] = $games;

        return $this;
    }

    /**
     * Remove games
     *
     * @param Game $games
     */
    public function removeGame(Game $games)
    {
        $this->games->removeElement($games);
    }

    /**
     * Get games
     *
     * @return Collection|Game[]
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Add withdraws
     *
     * @param Withdraw $withdraws
     * @return Deposit
     */
    public function addWithdraw(Withdraw $withdraws)
    {
        $this->withdraws[] = $withdraws;

        return $this;
    }

    /**
     * Remove withdraws
     *
     * @param Withdraw $withdraws
     */
    public function removeWithdraw(Withdraw $withdraws)
    {
        $this->withdraws->removeElement($withdraws);
    }

    /**
     * Get withdraws
     *
     * @return Collection|Withdraw[]
     */
    public function getWithdraws()
    {
        return $this->withdraws;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Deposit
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
