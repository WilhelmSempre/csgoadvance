<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\Core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Repository\ItemRepository;

/**
 * Class ItemHelper
 * @package CSGOADVANCE\src\Helpers
 */
final class ItemHelper
{

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * ItemHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->itemRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Item');
    }

    /**
     * @param array $itemParams
     * @return bool
     */
    public function addItem($itemParams = [])
    {
        $item = new Item();
        $item->setName($itemParams['name'])
            ->setMarketName($itemParams['market_name'])
            ->setDescription($itemParams['description'])
            ->setIcon($itemParams['icon'])
            ->setToken($itemParams['token'])
            ->setPrice($itemParams['price']);

        $this->itemRepository->addItem($item);

        return true;
    }

    /**
     * @param array $itemParams
     * @return bool
     */
    public function ifItemExists($itemParams = [])
    {

        /** @var Item[] $itemData */
        $itemData = $this->itemRepository->getItem($itemParams);

        return (count($itemData)) > 0;
    }

    /**
     * @param array $itemParams
     * @return bool|Item
     */
    public function getItem($itemParams = [])
    {

        /** @var Item[] $item */
        $item = $this->itemRepository->getItem($itemParams);

        if ($item) {
            return $item[0];
        }

        return false;
    }

    /**
     * @param $price
     * @param float $offset
     * @return bool|Item[]
     */
    public function getItemsByPrice($price, $offset = 0.0)
    {

        /** @var Item[] $items */
        $items = $this->itemRepository->getItemsByPrice($price, $offset);

        if ($items) {
            return $items;
        }

        return false;
    }

    /**
     * @param $price
     * @param float $offset
     * @param null $itemID
     * @return bool
     */
    public function ifItemsExistsByPrice($price, $offset = 0.0, $itemID = null)
    {

        /** @var Item[] $items */
        $items = $this->itemRepository->getItemsByPrice($price, $offset, $itemID);

        return count($items) > 0;
    }
}
