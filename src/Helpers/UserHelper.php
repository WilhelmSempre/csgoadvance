<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\Core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Multiply;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Repository\MultiplyRepository;
use CSGOADVANCE\src\Repository\UserRepository;
use CSGOADVANCE\src\Types\UserRoleType;

/**
 * Class UserHelper
 * @package CSGOADVANCE\src\Helpers
 */
final class UserHelper
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var MultiplyRepository
     */
    private $multiplyRepository;

    /**
     * UserHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->userRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\User');
        $this->multiplyRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Multiply');
    }

    /**
     * @param array $steamUser
     * @return bool
     */
    public function addUser($steamUser = [])
    {
        $user = new User();
        $user->setAvatar($steamUser['steam_avatar'])
            ->setSteamid($steamUser['steamid'])
            ->setRole(UserRoleType::USER)
            ->setWallet(0)
            ->setName($steamUser['steam_personaname']);

        if ($steamUser['steam_realname'] !== '') {
            $user->setRealname($steamUser['steam_realname']);
        }

        $user->setCreated((new \DateTime()));

        $this->userRepository->addUser($user);

        return true;
    }

    /**
     * @param array $steamUser
     * @return bool
     */
    public function ifUserExists($steamUser = [])
    {

        /** @var User[] $user */
        $user = $this->userRepository->getUser($steamUser);

        return count($user) > 0;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getTradelink(User $user)
    {
        return $user->getTradelink();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ifUserHasTradelink(User $user)
    {
        return $this->getTradelink($user) !== '';
    }

    /**
     * @param User $user
     * @param $newTradelink
     * @return bool
     */
    public function updateTradeLink(User $user, $newTradelink)
    {
        $user->setTradelink($newTradelink);

        $this->userRepository->updateUser($user);

        return true;
    }

    /**
     * @param array $steamUser
     * @return bool|User
     */
    public function getUser($steamUser = [])
    {

        /** @var User[] $user */
        $user = $this->userRepository->getUser($steamUser);

        if ($user) {
            return $user[0];
        }

        return false;
    }

    /**
     * @param $tradelink
     * @return UserHelper
     * @throws \Exception
     */
    public function validateTradelink($tradelink)
    {
        if (empty($tradelink)) {
            throw new \Exception('Fill this field!');
        }

        $pattern = '/^https:\/\/steamcommunity.com\/tradeoffer\/new\/\?partner=([0-9]{8,9})&token=([a-zA-Z0-9_-]{8})$/';

        if (!filter_var($tradelink, FILTER_VALIDATE_URL) ||
            !preg_match($pattern, $tradelink)) {
            throw new \Exception('Incorrect link! Correct that!');
        }

        return $this;
    }

    /**
     * @param User $user
     * @return array|bool|Multiply[]
     */
    public function getUserMultiplies(User $user)
    {

        /** @var Multiply[] $multiply */
        $multiply = $this->multiplyRepository->getMultiply([
            'userID' => $user
        ]);

        if ($multiply) {
            return $multiply;
        }

        return false;
    }

    /**
     * @param array $multiplyParams
     * @return bool
     */
    public function addUserMultiply($multiplyParams = [])
    {

        /** @var User $user */
        $user = $multiplyParams['user'];

        $multiplyArray = $multiplyParams['multiplies'];

        foreach ($multiplyArray as $multiplyParam => $multiplyChance) {
            $multiply = new Multiply();

            $multiply->setValue($multiplyParam)
                ->setChance($multiplyChance)
                ->setUser($user);

            $user->addMultiply($multiply);

            $this->multiplyRepository->addMultiply($multiply);
        }

        return true;
    }

    /**
     * @param array $multiplyParams
     * @return bool
     */
    public function ifUserHasMultiply($multiplyParams = [])
    {

        /** @var User $user */
        $user = $multiplyParams['user'];

        $multipliesArray = $multiplyParams['multiplies'];

        $multiplies = $this->getUserMultiplies($user);
        $multipliesValues = [];

        if ($multiplies && count($multiplies) > 0) {

            /** @var Multiply $multiply */
            foreach ($multiplies as $multiply) {
                $multipliesValues[] = $multiply->getValue();
            }

            return count(array_diff($multipliesValues, $multipliesArray)) > 0;
        }

        return false;
    }

    /**
     * @param User $user
     * @param User $targetUser
     * @return bool
     */
    public function isMe(User $user, User $targetUser) {

        return $user->getId() === $targetUser->getId();
    }

    /**
     * @param User $user
     * @param $referralCode
     * @return bool
     */
    public function createReferral(User $user, $referralCode)
    {
        $user->setReferral($referralCode);

        $this->userRepository->updateUser($user);

        return true;
    }

    /**
     * @param $referralCode
     * @return bool
     */
    public function ifReferralExists($referralCode)
    {

        /** @var User[] $user */
        $user = $this->userRepository->getUser([
            'referral' => $referralCode
        ]);

        return count($user) > 0;
    }

    /**
     * @param User $user
     * @param $count
     * @return bool
     */
    public function updateWallet(User $user, $count)
    {
        $user->setWallet($count);

        $this->userRepository->updateUser($user);

        return true;
    }

    /**
     * @param User $user
     * @param $logged
     * @return bool
     */
    public function setIsArleadyLogged(User $user, $logged)
    {
        $user->setIsLogged($logged);

        $this->userRepository->updateUser($user);

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function checkIfIsYouArleadyLogged(User $user)
    {
        $logged = $user->getIsLogged();

        return $logged === session_id();
    }

    /**
     * @param User $user
     * @param int $role
     * @return bool
     */
    public function isRole(User $user, $role)
    {
        return $user->getRole() === $role;
    }
}
