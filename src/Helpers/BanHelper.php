<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Ban;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Repository\BanRepository;

/**
 * Class BanHelper
 * @package CSGOADVANCE\src\Helpers
 */
class BanHelper
{

    /**
     * @var BanRepository
     */
    private $banRepository;

    /**
     * BanHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->banRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Ban');
    }

    /**
     * @param User $user
     * @param \DateTime $period
     * @param null $reason
     * @return bool
     */
    public function addBan(User $user, \DateTime $period, $reason = null)
    {
        $ban = new Ban();
        $ban->setPeriod($period)
            ->setReason($reason)
            ->setUser($user)
            ->setCreated(new \DateTime());

        $user->addBan($ban);

        $this->banRepository->addBan($ban);

        return true;
    }

    /**
     * @param array $banParams
     * @return bool|Ban
     */
    public function getBan($banParams = [])
    {

        /** @var Ban[] $ban */
        $bans = $this->banRepository->getBans($banParams);

        if ($bans) {
            return $bans[0];
        }

        return false;
    }

    /**
     * @return bool|Ban[]
     */
    public function getBans()
    {

        /** @var Ban[] $ban */
        $ban = $this->banRepository->getBans();

        if ($ban) {
            return $ban;
        }

        return false;
    }

    /**
     * @param $banParams
     * @return bool
     */
    public function banExists($banParams)
    {
        /** @var Ban[] $banData */
        $banData = $this->banRepository->getBans($banParams);

        return (count($banData)) > 0;
    }

    /**
     * @param array $banParams
     */
    public function removeBan($banParams = [])
    {

        /** @var Ban $ban */
        $ban = $this->getBan($banParams);

        $this->banRepository->removeBan($ban);
    }
}
