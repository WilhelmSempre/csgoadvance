<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Message;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Repository\MessageRepository;
use CSGOADVANCE\src\Types\MessageStatusType;

/**
 * Class MessageHelper
 * @package CSGOADVANCE\src\Helpers
 */
class MessageHelper
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * MessageHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->messageRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Message');
    }

    /**
     * @param $text
     * @param User $user
     * @return bool
     */
    public function addMessage($text, User $user)
    {
        $message = new Message();
        $message->setCreated(new \DateTime())
            ->setStatus(MessageStatusType::ACTIVE)
            ->setUser($user)
            ->setMessage($text);

        $this->messageRepository->addMessage($message);

        return true;
    }

    /**
     * @param array $messageParams
     * @return bool|Message
     */
    public function getMessage($messageParams = [])
    {

        /** @var Message[] $message */
        $message = $this->messageRepository->getMessage($messageParams);

        if ($message) {
            return $message[0];
        }

        return false;
    }

    /**
     * @return bool|Message[]
     */
    public function getMessages()
    {

        /** @var Message[] $message */
        $message = $this->messageRepository->getMessage();

        if ($message) {
            return $message;
        }

        return false;
    }
}
