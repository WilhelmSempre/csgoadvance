<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\Core\Managers\DatabaseManager;
use CSGOADVANCE\Core\Managers\SteamManager;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use Interop\Container\ContainerInterface;
use CSGOADVANCE\src\Repository\DepositRepository;
use CSGOADVANCE\src\Types\DepositStatusType;

/**
 * Class DepositHelper
 * @package CSGOADVANCE\src\Helpers
 */
final class DepositHelper
{

    /**
     * @var SteamManager
     */
    private $steamManager;

    /**
     * @var DepositRepository
     */
    private $depositRepository;

    /**
     * DepositHelper constructor.
     * @param ContainerInterface $application
     * @param DatabaseManager $databaseManager
     */
    public function __construct(ContainerInterface $application, DatabaseManager $databaseManager)
    {
        $this->steamManager = $application->get('steam');

        $this->depositRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Deposit');
    }

    /**
     * @param $steamID
     * @param $reset
     * @return array|mixed
     */
    public function getUserInventory($steamID, $reset)
    {
        return $this->steamManager->getUserInventory($steamID, $reset);
    }

    /**
     * @param array $depositParams
     * @return bool
     */
    public function addDeposit($depositParams = [])
    {
        $deposit = new Deposit();

        /** @var User $user */
        $user = $depositParams['user'];

        /** @var Item $item */
        $item = $depositParams['item'];

        $token = $depositParams['token'];
        $trade = $depositParams['trade'];

        $status = isset($depositParams['status']) ? $depositParams['status'] : DepositStatusType::PENDING;

        $deposit->setStatus($status)
            ->setCreated(new \DateTime())
            ->setItem($item)
            ->setUser($user)
            ->setToken($token)
            ->setTradeID($trade);

        $user->addDeposit($deposit);
        $item->addDeposit($deposit);

        $this->depositRepository->addDeposit($deposit);

        return true;
    }

    /**
     * @param array $depositParams
     * @return Deposit|bool
     */
    public function getDeposit($depositParams = [])
    {

        /** @var Deposit[] $deposit */
        $deposit = $this->depositRepository->getDeposit($depositParams);

        if ($deposit) {
            return $deposit[0];
        }

        return false;
    }

    /**
     * @param Deposit $deposit
     * @param $status
     */
    public function changeDepositStatus(Deposit $deposit, $status)
    {
        $deposit->setStatus($status);
        $this->depositRepository->updateDeposit($deposit);
    }

    /**
     * @return bool|int
     */
    public function getDepositLastID()
    {
        return $this->depositRepository->getDepositLastID();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ifPendingDepositExists(User $user)
    {
        return $this->depositRepository->isPendingDeposit($user);
    }

    /**
     * @param User $user
     * @return Withdraw[]
     */
    public function getPendingDeposits(User $user)
    {
        return $this->depositRepository->getPendingDeposits($user);
    }

    /**
     * @param Deposit $deposit
     * @param $token
     */
    public function changeDepositToken(Deposit $deposit, $token)
    {
        $deposit->setToken($token);
        $this->depositRepository->updateDeposit($deposit);
    }

    /**
     * @param Deposit $deposit
     * @param $tradeID
     */
    public function changeTradeID(Deposit $deposit, $tradeID)
    {
        $deposit->setTradeID($tradeID);
        $this->depositRepository->updateDeposit($deposit);
    }

    /**
     * @param Deposit $deposit
     * @return mixed
     */
    public function deleteDeposit(Deposit $deposit)
    {
        return $this->depositRepository->deleteDeposit($deposit);
    }
}
