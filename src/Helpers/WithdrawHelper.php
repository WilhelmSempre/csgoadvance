<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\Core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Buy;
use CSGOADVANCE\src\Entity\Game;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Entity\Withdraw;
use CSGOADVANCE\src\Repository\BuyRepository;
use CSGOADVANCE\src\Repository\WithdrawRepository;
use CSGOADVANCE\src\Types\BuyStatusType;
use CSGOADVANCE\src\Types\WithdrawStatusType;

/**
 * Class WithdrawHelper
 * @package CSGOADVANCE\src\Helpers
 */
class WithdrawHelper
{

    /**
     * @var WithdrawRepository
     */
    private $withdrawRepository;

    /**
     * @var BuyRepository
     */
    private $buyRepository;

    /**
     * DepositHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->withdrawRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Withdraw');
        $this->buyRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Buy');
    }

    /**
     * @param array $withdrawParams
     * @return bool
     */
    public function addWithdraw($withdrawParams = [])
    {
        $withdraw = new Withdraw();

        /** @var User $user */
        $user = $withdrawParams['user'];

        /** @var Game $game */
        $game = $withdrawParams['game'];

        /** @var Item $item */
        $item = $withdrawParams['item'];

        $tradeID = $withdrawParams['tradeID'];

        $withdraw->setCreated(new \DateTime())
            ->setUser($user)
            ->setGame($game)
            ->setItem($item)
            ->setTradeID($tradeID)
            ->setStatus(WithdrawStatusType::UNPAID);

        $user->addWithdraw($withdraw);
        $item->addWithdraw($withdraw);
        $game->addWithdraw($withdraw);

        $this->withdrawRepository
            ->addWithdraw($withdraw);

        return true;
    }

    /**
     * @param array $withdrawParams
     * @return bool|Withdraw
     */
    public function getWithdraw($withdrawParams = [])
    {
        $withdraw = $this->withdrawRepository->getWithdraw($withdrawParams);

        if ($withdraw) {
            return $withdraw[0];
        }

        return false;
    }

    /**
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function isWithdraw(User $user, Item $item)
    {
        return $this->withdrawRepository->isWithdraw($user, $item);
    }

    /**
     * @param Withdraw $withdraw
     * @param integer $status
     */
    public function changeWithdrawStatus(Withdraw $withdraw, $status)
    {
        $withdraw->setStatus($status);
        $this->withdrawRepository->updateWithdraw($withdraw);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ifPendingWithdrawExists(User $user)
    {
        return $this->withdrawRepository->isPendingWithdraw($user);
    }

    /**
     * @param User $user
     * @return Withdraw[]
     */
    public function getPendingWithdraws(User $user)
    {
        return $this->withdrawRepository->getPendingWithdraws($user);
    }

    /**
     * @param array $buyParams
     */
    public function addBuy($buyParams = [])
    {
        $buy = new Buy();

        /** @var User $user */
        $user = $buyParams['user'];

        /** @var Item $item */
        $item = $buyParams['item'];

        $buy->setUser($user)
            ->setItem($item)
            ->setCreated(new \DateTime())
            ->setStatus(BuyStatusType::PENDING);

        $user->addBuy($buy);
        $item->addBuy($buy);

        $this->buyRepository->addBuy($buy);
    }

    /**
     * @param User $user
     * @return Buy[]
     */
    public function getPendingBuys(User $user)
    {
        return $this->buyRepository->getPendingBuys($user);
    }

    /**
     * @param Buy $buy
     * @param $status
     */
    public function changeBuyStatus(Buy $buy, $status)
    {
        $buy->setStatus($status);
        $this->buyRepository->updateBuy($buy);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function ifPendingBuysExists(User $user)
    {
        return $this->buyRepository->isPendingBuy($user);
    }
}
