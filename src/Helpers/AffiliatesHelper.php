<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\src\Repository\AffiliatesRepository;
use CSGOADVANCE\src\Entity\Affiliates;
use CSGOADVANCE\src\Entity\User;

/**
 * Class AffiliatesHelper
 * @package CSGOADVANCE\src\Helpers
 */
class AffiliatesHelper
{

    /**
     * @var AffiliatesRepository
     */
    private $affiliatesRepository;

    /**
     * AffiliatesHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->affiliatesRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Affiliates');
    }

    /**
     * @param User $user
     * @param User $affilatedUser
     * @return bool
     */
    public function addAffiliate(User $user, User $affilatedUser)
    {
        $affiliate = new Affiliates();
        $affiliate->setUser($user)
            ->setAffilatedUser($affilatedUser)
            ->setCreated(new \DateTime());

        $user->addAffiliate($affiliate);

        $this->affiliatesRepository->addAffiliate($affiliate);

        return true;
    }

    /**
     * @param array $affiliatesParams
     * @return bool|Affiliates[]
     */
    public function getAffiliates($affiliatesParams = [])
    {

        /** @var Affiliates[] $affiliates */
        $affiliates = $this->affiliatesRepository->getAffiliates($affiliatesParams);

        if ($affiliates) {
            return $affiliates;
        }

        return false;
    }

    /**
     * @param array $affiliateParams
     * @return bool|Affiliates
     */
    public function getAffiliate($affiliateParams = [])
    {
        $affiliates = $this->getAffiliates($affiliateParams);

        return $affiliates[0];
    }

    /**
     * @param User $user
     * @param $referral
     * @return bool
     */
    public function isMyReferral(User $user, $referral)
    {
        return $user->getReferral() === $referral;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function referralUsed(User $user)
    {
        $affiliates = $this->getAffiliates([
            'userID' => $user->getId()
        ]);

        return count($affiliates) > 0;
    }
}