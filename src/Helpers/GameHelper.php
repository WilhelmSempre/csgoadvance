<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\Core\Managers\DatabaseManager;
use CSGOADVANCE\core\Managers\SecurityManager;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Game;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Entity\Withdraw;
use CSGOADVANCE\src\Repository\GameRepository;
use CSGOADVANCE\src\Types\DepositStatusType;
use CSGOADVANCE\src\Types\WithdrawStatusType;

/**
 * Class GameHelper
 * @package CSGOADVANCE\src\Helpers
 */
final class GameHelper
{

    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * GameHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->gameRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Game');
    }

    /**
     * @param $chance
     * @return array
     */
    public function getRandom($chance)
    {
        $los = [];

        for ($i = 0; $i < 100; $i++) {
            $los[] = 0;
        }

        for ($i = 0; $i < $chance * 100; $i++) {
            $random = rand(0, 99);

            while ($los[$random] === 1) {
                $random = rand(0, 99);
            }

            $los[$random] = 1;
        }

        return $los;
    }

    /**
     * @param array $los
     * @return bool
     */
    public function getWinOrLose($los = [])
    {
        $random = rand(0, 99);
        return (bool) $los[$random];
    }

    /**
     * @param array $gameParams
     * @return bool|Game
     */
    public function getGame($gameParams = [])
    {

        /** @var Game[] $game */
        $game = $this->gameRepository->getGame($gameParams);

        if ($game) {
            return $game[0];
        }

        return false;
    }

    /**
     * @param array $gameParams
     */
    public function addGame($gameParams = [])
    {

        /** @var Deposit $deposit */
        $deposit = $gameParams['deposit'];

        /** @var User $user */
        $user = $gameParams['user'];

        $multiply = $gameParams['multiply'];
        $winOrLose = $gameParams['winOrLose'];
        $token = $gameParams['token'];
        $beforeItem = $gameParams['beforeItem'];
        $afterItem = $gameParams['afterItem'];

        $game = new Game();

        $game->setCreated(new \DateTime())
            ->setMultiply($multiply)
            ->setDeposit($deposit)
            ->setUser($user)
            ->setToken($token)
            ->setAfterItem($afterItem)
            ->setBeforeItem($beforeItem)
            ->setWin($winOrLose);

        $user->addGame($game);
        $deposit->addGame($game);

        $this->gameRepository->addGame($game);
    }

    /**
     * @param array $wonParams
     * @param SecurityManager $securityManager
     */
    public function saveWon($wonParams = [], SecurityManager $securityManager)
    {

        /** @var User $userData */
        $userData = $wonParams['user'];

        /** @var Game $gameData */
        $gameData = $wonParams['game'];

        /** @var Deposit $depositData */
        $depositData = $wonParams['deposit'];

        /** @var Item $afterItemData */
        $afterItemData = $wonParams['afterItem'];

        $tradeID = $wonParams['tradeID'];
        $token = $wonParams['token'];

        $deposit = new Deposit();

        $deposit->setUser($userData)
            ->setItem($afterItemData)
            ->setCreated(new \DateTime())
            ->setToken($token)
            ->setStatus(DepositStatusType::ACTIVE)
            ->setTradeID($tradeID);

        $withdraw = new Withdraw();

        $withdraw->setTradeID($tradeID)
            ->setStatus(WithdrawStatusType::UNPAID)
            ->setCreated(new \DateTime())
            ->setItem($afterItemData)
            ->setGame($gameData)
            ->setUser($userData);

        $this->gameRepository->addWonDeposit($deposit);
        $this->gameRepository->addWonWithDraw($withdraw);

        $withdrawWonLastID = $this->gameRepository->getWithdrawWonLastID();
        $depositWonLastID = $this->gameRepository->getDepositWonLastID();

        /** @var Deposit $deposit */
        $deposit = $this->gameRepository->getWonDeposit($depositWonLastID);

        /** @var Withdraw $withdraw */
        $withdraw = $this->gameRepository->getWonWithdraw($withdrawWonLastID);

        $token = $afterItemData->getId() . $tradeID . $deposit->getId() . $gameData->getId();
        $token = $securityManager->generateSecurityToken($token);

        $withdraw->setToken($token);

        $deposit->addWithdraw($withdraw);

        $this->gameRepository->updateWonDeposit($deposit);

        $withdraw->setDeposit($deposit);

        $this->gameRepository->updateWonWithdraw($withdraw);
    }
}
