<?php

namespace CSGOADVANCE\src\Helpers;

use CSGOADVANCE\core\Managers\DatabaseManager;
use CSGOADVANCE\src\Entity\Game;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Repository\GameRepository;
use CSGOADVANCE\src\Types\GameStatusType;

/**
 * Class StatisticsHelper
 * @package CSGOADVANCE\src\Helpers
 */
final class StatisticsHelper
{

    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * StatisticsHelper constructor.
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager)
    {
        $this->gameRepository = $databaseManager->getManager()->getRepository('CSGOADVANCE\src\Entity\Game');
    }

    /**
     * @param array $statisticParams
     * @return array|Game[]
     */
    public function getRecentGames($statisticParams = [])
    {

        /** @var Game[] $games */
        $games = $this->gameRepository->getGame($statisticParams, 10);

        return $games;
    }

    /**
     * @return float|int
     */
    public function getWinsToday()
    {

        /** @var Game[] $game */
        $games = $this->gameRepository->getGame([
            'win' => GameStatusType::WIN
        ]);

        $price = 0;

        /** @var Game $game */
        foreach ($games as $game) {

            /** @var Item $item */
            $item = $game->getAfterItem();

            $date = $game->getCreated();

            if ((int) $date->format('dmy') === (int) date('dmy')) {
                $price += (float) $item->getPrice();
            }
        }

        return $price;
    }
}
