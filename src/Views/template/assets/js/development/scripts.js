(function ($) {

    'use strict';

    var CSGO = function () {

        /**
         * @type {number}
         */
        this.timeout = 0;

        /**
         * @type {Array}
         */
        this.botSockets = [];

        /**
         * @type {number}
         */
        this.steamID = $('body').attr('data-steamid');

        /**
         * @type {Array}
         */
        this.connected = [];

        /**
         * @type {*}
         */
        this.gameTokenPath = $('*[data-game-token]');

        /**
         * @type {*}
         */
        this.inventoryCountBlock = $('*[data-inventory-count]');

        /**
         * @type {*}
         */
        this.inventoryPriceBlock = $('*[data-inventory-price]');

        /**
         * @type {*}
         */
        this.inventorySelectedCountBlock = $('*[data-inventory-selected-count]');

        /**
         * @type {*}
         */
        this.inventorySelectedPriceBlock = $('*[data-inventory-selected-price]');

        /**
         * @type {*}
         */
        this.lastUpgradeBlock = $('*[data-last-upgrade]');


        /**
         * @type {*}
         */
        this.inventoryBlock = $('*[data-inventory]');

        /**
         * @type {*}
         */
        this.depositForm = $('*[data-deposit-form]');

        /**
         * @type {*}
         */
        this.countupBlock = $('*[data-countup]');

        /**
         *
         */
        this.countupBlockValue = this.countupBlock.attr('data-countup-value');

        /**
         * @type {*}
         */
        this.userInventoryBlock = $('*[data-user-inventory]');

        /**
         * @type {*}
         */
        this.depositInventoryBlock = $('*[data-deposit-inventory]');

        /**
         * @type {*}
         */
        this.withdrawInventoryBlock = $('*[data-withdraw-inventory]');

        /**
         * @type {*}
         */
        this.shopInventoryBlock = $('*[data-shop-inventory]');

        /**
         * @type {*}
         */
        this.beforeItemBlock = $('*[data-item-before]');

        /**
         * @type {*}
         */
        this.afterItemBlock = $('*[data-item-after]');

        /**
         * @type {*}
         */
        this.multipliesBlock = $('*[data-multiplies]');

        /**
         * @type {*}
         */
        this.chanceText = $('*[data-chance]');

        /**
         * @type {*}
         */
        this.chanceInput = $('*[data-play-chance]');

        /**
         * @type {*}
         */
        this.multiplyInput = $('*[data-play-multiply]');

        /**
         * @type {*}
         */
        this.tradeIDInput = $('*[data-play-trade-id]');

        /**
         * @type {*}
         */
        this.depositIDInput = $('*[data-play-deposit-id]');

        /**
         * @type {*}
         */
        this.hashInput = $('*[data-play-hash]');

        /**
         * @type {*}
         */
        this.beforeIDInput = $('*[data-play-before-id]');

        /**
         * @type {*}
         */
        this.afterIDInput = $('*[data-play-after-id]');

        /**
         * @type {*}
         */
        this.multiplyButtons = $('button[data-multiple-button]');

        /**
         * @type {*}
         */
        this.gameForm = $('*[data-game-form]');

        /**
         *
         */
        this.inventoryPath = this.inventoryBlock.attr('data-inventory-path');

        /**
         *
         */
        this.gameFormSubmit = this.gameForm.find('button[type="submit"]');

        /**
         *
         */
        this.formWinPath = this.gameFormSubmit.attr('data-win-path');

        /**
         * @type {*}
         */
        this.lastUpgradePath = this.lastUpgradeBlock.attr('data-last-upgrade-path');

        /**
         * @type {*}
         */
        this.gameFormToken = $('*[data-play-token]');

        /**
         * @type {*}
         */
        this.botAlert = $('*[data-bot-alert]');

        /**
         * @type {*}
         */
        this.messengerBlock = $('*[data-messenger]');

        /**
         * @type {*}
         */
        this.messengerTextInput = $('*[data-messenger-text]');

        /**
         * @type {*}
         */
        this.messengerTextForm = $('*[data-messenger-form]');

        /**
         * @type {*}
         */
        this.messengerPath = this.messengerBlock.attr('data-messenger-path');

        /**
         * @type {*}
         */
        this.reloadInventoryButton = $('*[data-reload-inventory]');

        /**
         * @type {*}
         */
        this.sortForm = $('*[data-sort-form]');

        /**
         * @type {*}
         */
        this.supportForm = $('*[data-support-form]');

        /**
         * @type {*}
         */
        this.sortInput = this.sortForm.find('*[data-sort]');

        /**
         * @type {*}
         */
        this.searchInput = this.sortForm.find('*[data-search]');

        /**
         * @type {*}
         */
        this.profileBlock = $('*[data-profile]');

        /**
         * @type {*}
         */
        this.profileDropdown = $('*[data-profile-dropdown]');

        /**
         * @type {*}
         */
        this.validateForm = $('*[data-validate-form]');

        /**
         * @type {*}
         */
        this.createReferralCodeForm = $('*[data-create-referral-form]');

        /**
         * @type {*}
         */
        this.useReferralCodeForm = $('*[data-use-referral-form]');

        /**
         * @type {*}
         */
        this.referralCode = $('*[data-referral]');

        /**
         * @type {*}
         */
        this.createReferralInput = $('*[data-create-referral-input]');

        /**
         * @type {*}
         */
        this.useReferralInput = $('*[data-use-referral-input]');

        /**
         * @type {*}
         */
        this.playFormSelect = $('*[data-play-select]');

        /**
         * @type {*}
         */
        this.multiplesButton = $('*[data-multiple-button]');

        /**
         * @type {*}
         */
        this.noconnectionBlock = $('*[data-noconnection]');

        /**
         * @type {boolean}
         */
        this.owl = false;

        /**
         * @type {*}
         */
        this.justiceForm = $('*[data-justice-form]');

        /**
         * @type {*}
         */
        this.justiceFormAlert = $('*[data-justice-form-alert]');

        /**
         * @type {*}
         */
        this.sounds = $('*[data-sounds]');

        /**
         * @type {*}
         */
        this.soundsOff = $('*[data-sounds-off]');

        /**
         * @type {*}
         */
        this.soundsOn = $('*[data-sounds-on]');

        /**
         * @param index
         * @returns {CSGO}
         */
        this.initBotsSockets = function(index) {
            (function(self) {
                self.botSockets[index].on('connect', function () {
                    if (self.noconnectionBlock.is(':visible')) {
                        self.noconnectionBlock.fadeOut(500);
                    }

                    self.connected[index] = true;

                    if (self.steamID !== '') {
                        self.botSockets[index].emit('userJoinGame', self.steamID);
                    }
                }).on('connect_error', function () {
                    if (!self.noconnectionBlock.is(':visible')) {
                        self.noconnectionBlock.fadeIn(500);
                    }

                    self.connected[index] = false;

                    if (self.steamID !== '') {
                        self.botSockets[index].emit('userLeftGame', self.steamID);
                    }
                }).on('reconnect', function () {
                    if (self.steamID !== '') {
                        self.botSockets[index].emit('userLeftGame', self.steamID);
                    }
                });

                $(window).on('beforeunload', function() {
                    self.botSockets[index].emit('userLeftGame', self.steamID);
                });
            })(this);

            return this;
        };

        /**
         * @param bots
         * @returns {CSGO}
         */
        this.socketConfiguration = function (bots) {

            for (var index in bots) {
                this.botSockets[index] = io('http://' + window.location.hostname + ':' + bots[index].parameters.port, { 'sync disconnect on unload': true });
            }

            (function (self) {

                for (var index in self.botSockets) {
                    self.initBotsSockets(index);
                }

                setTimeout(function() {

                    /** Deposit load **/
                    for (var index in self.connected) {
                        if (self.connected[index]) {
                            self.tradeManager(index)
                                .messengerEvent(index)
                                .messageEvent(index)
                                .recentGamesEvent(index)
                                .statisticsManager(index)
                                .depositEvent(index)
                                .withdrawEvent(index)
                                .buyEvent(index)
                                .inventoryFormCheckboxesEvent()
                                .inventorySortFormEvent()
                                .profileEvents()
                                .supportFormEvents()
                                .referralFormEvents()
                                .chatboxFeature()
                                .playEvent(index)
                                .justiceEvent()
                                .depositLoad('', '', index, false);

                            break;
                        }
                    }
                }, 1000);
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.statisticsManager = function (index) {
            (function(self, index) {
                self.botSockets[index].on('updateWonToday', function (price) {
                    var todayStart = parseFloat($('#today').text());
                    self.countUp(todayStart, todayStart + price, 'today');
                });
            })(this, index);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.tradeManager = function (index) {
            (function(self, index) {
                self.botSockets[index].on('initializeTrade', function (data) {
                    var returnPath = $('body').attr('data-deposit-return');
                    var mode = mode || self.inventoryBlock.data('mode') || '';

                    $.when($.ajax({
                        url: returnPath,
                        data: {
                            trade_id: data.trade_id,
                            market_names: data.market_names
                        },
                        dataType: 'json',
                        method: 'post'
                    })).done(function (response) {
                        self.depositLoad(mode, '' , index, true);
                    });
                }).on('initializeWithdraw', function (data) {
                    var returnPath = $('body').attr('data-withdraw-return');
                    var mode = mode || self.inventoryBlock.data('mode') || '';

                    console.log(data);

                    if (data.message.indexOf('[AFFILIATES]') !== -1) {
                        returnPath = $('body').attr('data-buy-return');
                    }

                    $.when($.ajax({
                        url: returnPath,
                        data: {
                            trade_id: data.trade_id,
                            market_names: data.market_names
                        },
                        dataType: 'json',
                        method: 'post'
                    })).done(function (response) {
                        self.depositLoad(mode, '', index, true);
                    });
                }).on('cancelWithdraw', function (data) {
                    var cancelPath = $('body').attr('data-withdraw-cancel');
                    var mode = mode || self.inventoryBlock.data('mode') || '';

                    if (data.message.indexOf('[AFFILIATES]') !== -1) {
                        cancelPath = $('body').attr('data-buy-cancel');
                    }

                    $.when($.ajax({
                        url: cancelPath,
                        dataType: 'json',
                        method: 'post'
                    })).done(function (response) {
                        self.depositLoad(mode, '', index, false);
                    });
                }).on('cancelTrade', function () {
                    var cancelPath = $('body').attr('data-deposit-cancel');
                    var mode = mode || self.inventoryBlock.data('mode') || '';

                    $.when($.ajax({
                        url: cancelPath,
                        dataType: 'json',
                        method: 'post'
                    })).done(function (response) {
                        self.depositLoad(mode, '', index, false);
                    });
                });
            })(this, index);

            return this;
        };

        /**
         * @param block
         * @returns {CSGO}
         */
        this.loader = function (block) {
            block.html('<div class="loader loader--style2 spinner">' +
                '<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">' +
                '<path fill="#ff6b2f" d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z">' +
                '<animateTransform attributeType="xml"' +
                ' attributeName="transform"' +
                ' type="rotate"' +
                ' from="0 25 25"' +
                ' to="360 25 25"' +
                ' dur="0.6s"' +
                ' repeatCount="indefinite"/>' +
                '</path>' +
                '</svg>' +
                '</div>');
            return this;
        };

        /**
         * @param template
         * @returns {CSGO}
         */
        this.depositLoadAfterEvents = function (template) {
            var count = $(template).find('[data-item]').length;
            this.inventoryCountBlock.text(count);

            $(document).on('click', '[data-play-select]', function () {
                var form = $(this).closest('[data-play-form]');
                form.trigger('submit');
            });

            return this;
        };

        /**
         * @param mode
         * @param sort
         * @param index
         * @param refresh
         * @returns {CSGO}
         */
        this.depositLoad = function (mode, sort, index, refresh) {
            (function (self) {
                if (self.inventoryBlock.length !== 0) {
                    self.inventoryBlock.html('');

                    self.loader(self.inventoryBlock);

                    sort = sort || '';
                    mode = mode || self.inventoryBlock.data('mode') || '';
                    refresh = refresh || false;

                    var data = '';

                    if (sort !== '') {
                        data = sort + '&sort=true';
                    }

                    if (refresh) {
                        //data = data + '&refresh=' + refresh;
                        data = data + '&refresh=true';
                    }

                    $.ajax({
                        url: self.inventoryPath,
                        method: 'post',
                        data: data,
                        dataType: 'json',
                        cache: true,
                        success: function (responseJSON) {
                            if (responseJSON.success.status) {
                                if (mode === 'withdraw') {

                                    /** Emit withdraw request data **/
                                    self.botSockets[index].emit('loadBotInventoryRequest');

                                    /** Items loaded to proccessing **/
                                    self.botSockets[index].on('itemsFromBotLoaded', function (data) {
                                        var itemsResponseHTML = $(responseJSON.success.template),
                                            items = itemsResponseHTML.find('[data-item]');

                                        var itemsMarketNames = [];

                                        for (var item in data.items) {
                                            itemsMarketNames.push(data.items[item].market_name);
                                        }

                                        for (var i = 0; i < items.length; i++) {
                                            $(items[i]).addClass('unavailable');

                                            var marketName = $(items[i]).find('[data-marketname]').val();

                                            for (var index in itemsMarketNames) {
                                                if (marketName === itemsMarketNames[index]) {
                                                    $(items[i]).removeClass('unavailable').addClass('available');
                                                    itemsMarketNames[index] = false;
                                                    break;
                                                }
                                            }

                                            itemsResponseHTML.find('[data-item]').eq(i).replaceWith(items[i]);
                                        }

                                        self.inventoryBlock.html(itemsResponseHTML);
                                        self.depositLoadAfterEvents(responseJSON.success.template);

                                        self.slimscroll(self.inventoryBlock);
                                    });
                                } else {
                                    self.inventoryBlock.html(responseJSON.success.template);
                                    self.depositLoadAfterEvents(responseJSON.success.template);
                                }

                                self.slimscroll(self.inventoryBlock);

                            } else {
                                self.inventoryBlock.html('Cannot load inventory!');
                            }
                        }
                    });
                }
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.loadGamesRecent = function () {
            (function (self) {
                var recentLength = self.lastUpgradeBlock.find('.csgoadvance-games-row').length;

                $.ajax({
                    url: self.lastUpgradePath,
                    method: 'post',
                    dataType: 'json',
                    cache: true,
                    success: function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.lastUpgradeBlock.html(responseJSON.success.template);

                                if (recentLength > 0) {
                                    var recentRow = $('.csgoadvance-games-row:first-child');
                                    recentRow.addClass('animated bounceInRight');
                                }

                                if (!self.lastUpgradeBlock.hasClass('active')) {
                                    self.lastUpgradeBlock.slimScroll({
                                        start: 'top',
                                        scrollTo: 0
                                    });
                                } else {
                                    self.lastUpgradeBlock.slimScroll();
                                }
                            }
                        }
                    }
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.recentGamesEvent = function (index) {
            this.loadGamesRecent();

            (function (self) {
                self.botSockets[index].on('gameAdded', function () {
                    self.loadGamesRecent();
                });

                self.lastUpgradeBlock.on('mouseenter', function () {
                    $(this).addClass('active');
                }).on('mouseleave', function () {
                    $(this).removeClass('active');
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.depositEvent = function (index) {

            (function (self) {
                self.userInventoryBlock.on('submit', 'form', function (event) {
                    event = event || window.event;

                    self.showAlert('info', 'Deposit in progress..');

                    var eventTarget = $(event.target),
                        userInventoryFormData = eventTarget.serialize(),
                        userInventoryActionForm = eventTarget.attr('action');

                    self.inventorySelectedCountBlock.text(0);
                    self.inventorySelectedPriceBlock.text(0);

                    var rowCheckboxes = self.inventoryBlock.find('[data-select]').find('input[type="checkbox"]');

                    rowCheckboxes.prop('checked', false);

                    self.inventoryBlock.find('[data-select]').find('[data-select-image]').removeClass('selected');

                    $.when($.ajax({
                        url: userInventoryActionForm,
                        data: userInventoryFormData,
                        method: 'post',
                        dataType: 'json'
                    })).done(function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                if (responseJSON.success.market_names && responseJSON.success.text) {
                                    self.botSockets[index].emit('userTradeRequest', {
                                        'userTradeLink': responseJSON.success.text,
                                        'market_names': responseJSON.success.market_names
                                    });
                                }
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.showAlert('error', responseJSON.errors.text);
                            }
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.playEvent = function (index) {
            (function (self) {
                self.depositInventoryBlock.on('submit', 'form', function (event) {
                    event = event || window.event;

                    self.loader(self.beforeItemBlock);

                    var closeGameSign = self.afterItemBlock.find('.x');

                    if (closeGameSign.length <= 0) {
                        self.afterItemBlock.html('<span class="x"></span>');
                        self.gameFormSubmit.prop('disabled', true);
                    }

                    var eventTarget = $(event.target),
                        depositInventoryFormData = eventTarget.serialize(),
                        depositInventoryActionForm = eventTarget.attr('action');

                    $.when($.ajax({
                        url: depositInventoryActionForm,
                        data: depositInventoryFormData,
                        method: 'post',
                        dataType: 'json'
                    })).done(function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.beforeItemBlock.html(responseJSON.success.template);

                                /** Add multiplies **/
                                self.multipliesBlock.html(responseJSON.success.multiplies).fadeIn(500);
                                self.tradeIDInput.val(responseJSON.success.tradeID);
                                self.depositIDInput.val(responseJSON.success.depositID);
                            }
                        }
                    });

                    event.preventDefault();
                });

                /** Multiplies event **/
                self.multipliesBlock.on('click', 'button[data-multiple-button]', function (event) {
                    event = event || window.event;

                    var buttonChance = parseFloat($(this).attr('data-chance')),
                        buttonValue = parseFloat($(this).attr('data-value')),
                        beforeID = self.beforeItemBlock.find('[data-item]').attr('data-id'),
                        afterItemPath = $(this).attr('data-path'),
                        gameFormToken = $(this).attr('data-game-token');

                    self.loader(self.afterItemBlock);
                    self.gameFormSubmit.prop('disabled', true);

                    $.when($.ajax({
                        url: afterItemPath,
                        data: {
                            multiply: buttonValue,
                            before_id: beforeID
                        },
                        method: 'post',
                        dataType: 'json'
                    })).then(function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.afterItemBlock.html(responseJSON.success.data);

                                var afterID = self.afterItemBlock.find('[data-item]').attr('data-id'),
                                    tradeID = self.tradeIDInput.val(),
                                    depositID = self.depositIDInput.val(),
                                    hash = self.hashInput.val();

                                $.when($.ajax({
                                    url: gameFormToken,
                                    data: {
                                        after_id: afterID,
                                        trade_id: tradeID,
                                        deposit_id: depositID,
                                        before_id: beforeID,
                                        multiply: buttonValue,
                                        chance: buttonChance,
                                        hash: hash
                                    },
                                    method: 'post',
                                    dataType: 'json'
                                })).then(function(responseJSON) {
                                    if (responseJSON.success.status) {
                                        self.chanceInput.val(buttonChance);
                                        self.beforeIDInput.val(beforeID);
                                        self.afterIDInput.val(afterID);
                                        self.multiplyInput.val(buttonValue);
                                        self.gameFormToken.val(responseJSON.success.token);

                                        self.gameFormSubmit.prop('disabled', false);
                                        self.gameEvent(index);
                                    }
                                });
                            }
                        }
                    }).done(function () {
                        var chance = parseFloat(buttonChance) * 100, chanceText;

                        if (buttonValue === 1.5) {
                            self.chanceText.removeClass('green').removeClass('orange')
                                .removeClass('red').removeClass('purple').addClass('blue');
                        } else if (buttonValue === 2.0) {
                            self.chanceText.removeClass('blue').removeClass('orange')
                                .removeClass('red').removeClass('purple').addClass('green');
                        } else if (buttonValue === 4.0) {
                            self.chanceText.removeClass('blue').removeClass('green')
                                .removeClass('red').removeClass('purple').addClass('orange');
                        } else if (buttonValue === 10.0) {
                            self.chanceText.removeClass('blue').removeClass('green')
                                .removeClass('orange').removeClass('purple').addClass('red');
                        } else if (buttonValue === 20.0) {
                            self.chanceText.removeClass('blue').removeClass('green')
                                .removeClass('orange').removeClass('red').addClass('purple');
                        }

                        if (chance > 0 && chance <= 30) {
                            chanceText = 'VERY LOW (' + chance + '%)';
                            self.chanceText.html(chanceText).closest('.chance').show();
                        } else if (chance > 30 && chance <= 80) {
                            chanceText = 'MEDIUM (' + chance + '%)';
                            self.chanceText.html(chanceText).closest('.chance').show();
                        } else if (chance > 80 && chance <= 100) {
                            chanceText = 'VERY HIGH (' + chance + '%)';
                            self.chanceText.html(chanceText).closest('.chance').show();
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @param start
         * @param to
         * @param element
         * @returns {CSGO}
         */
        this.countUp = function(start, to, element)
        {
            var easingFn = function (t, b, c, d) {
                var ts = (t /= d) * t;
                var tc = ts * t;
                return b + c * (tc * ts + -5 * ts * ts + 10 * tc + -10 * ts + 5 * t);
            };

            var options = {
                useEasing : true,
                easingFn: easingFn,
                useGrouping : true,
                separator : '',
                decimal : '.'
            };

            var count = new CountUp(element, start, parseFloat(to), 2, 2.5, options);
            count.start();

            return this;
        };

        /**
         * @param countDownBlock
         * @param time
         * @param callback
         * @returns {CSGO}
         */
        this.countDown = function(countDownBlock, time, callback)
        {
            (function(self) {
                countDownBlock.text(time);
                document.getElementById('count-audio').play();

                var x = setInterval(function() {
                    time--;

                    document.getElementById('count-audio').play();

                    countDownBlock.text(time);

                    if (time <= 0) {
                        clearInterval(x);
                        callback();
                    }
                }, 1000);
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.gameEvent = function (index) {
            (function (self) {
                self.chanceText.closest('.chance').fadeIn(200);

                self.gameForm.off('submit').on('submit', function (event) {
                    event = event || window.event;

                    self.multipliesBlock.html('');
                    self.gameFormSubmit.prop('disabled', true);
                    self.depositInventoryBlock.find('input').prop('disabled', true);

                    document.getElementById('game-audio').play();

                    var gameFormAction = $(this).attr('action'),
                        gameFormData = $(this).serialize(),
                        afterItem = self.afterItemBlock.html();


                    setTimeout(function() {
                        self.afterItemBlock.html('<span data-countdown class="countdown"></span>');

                        var countDownBlock = $('*[data-countdown]');

                        self.countDown(countDownBlock, 3, function () {
                            $.when($.ajax({
                                url: gameFormAction,
                                data: gameFormData,
                                method: 'post',
                                dataType: 'json'
                            })).done(function (responseJSON) {
                                if (responseJSON.success) {
                                    if (responseJSON.success.status) {

                                        countDownBlock.remove();

                                        self.chanceText.removeClass('medium').removeClass('high').addClass('low').html('VERY LOW (0%)').closest('.chance').hide();

                                        if (responseJSON.success.data.win) {
                                            self.showAlert('success', 'Wygrałeś!');
                                            self.afterItemBlock.html(afterItem);
                                            self.particles();

                                            document.getElementById('win-audio').play();

                                            self.saveWon(responseJSON.success.data, self.formWinPath);
                                            self.botSockets[index].emit('gameAdditionRequest');
                                            self.botSockets[index].emit('updateWonTodayRequest', parseFloat(responseJSON.success.data.afterItemPrice));

                                            var x = setTimeout(function() {
                                                self.beforeItemBlock.html('<span class="x"></span>');
                                                self.afterItemBlock.html('<span class="x"></span>');
                                                self.depositLoad('', '', index, true);
                                            }, 2000);

                                        } else {
                                            self.showAlert('error', 'Przegrałeś!');

                                            document.getElementById('lose-audio').play();

                                            self.beforeItemBlock.html('<span class="x"></span>');
                                            self.afterItemBlock.html('<span class="x"></span>');

                                            setTimeout(function() {
                                                self.depositLoad('', '', index, true);
                                                self.depositInventoryBlock.find('input').prop('disabled', false);
                                            }, 1000);
                                        }
                                    }
                                } else if (responseJSON.errors) {
                                    if (responseJSON.errors.status) {
                                        self.showAlert('error', responseJSON.errors.text);

                                        self.beforeItemBlock.html('<span class="x"></span>');
                                        self.afterItemBlock.html('<span class="x"></span>');

                                        self.depositLoad('', '', index, true);
                                        self.depositInventoryBlock.find('input').prop('disabled', false);
                                    }
                                }
                            });
                        });
                    }, 1000);

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.loadMessages = function () {
            (function (self) {
                var messagesLength = self.messengerBlock.find('.csgoadvance-messenger-row').length;

                $.ajax({
                    url: self.messengerPath,
                    method: 'post',
                    dataType: 'json',
                    cache: true,
                    success: function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.messengerBlock.html(responseJSON.success.template);

                                var messengerBlockHeight = self.messengerBlock[0].scrollHeight;

                                if (messagesLength > 0) {
                                    var lastMessageRow = $('.csgoadvance-messenger-row:last-child');
                                    lastMessageRow.addClass('animated bounceInLeft');
                                }

                                if (!self.messengerBlock.hasClass('active')) {
                                    self.messengerBlock.slimScroll({
                                        start: 'bottom',
                                        scrollTo: messengerBlockHeight
                                    });
                                } else {
                                    self.messengerBlock.slimScroll();
                                }

                            }
                        }
                    }
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.messengerEvent = function (index) {
            this.loadMessages();

            (function (self) {
                self.botSockets[index].on('messageAdded', function () {
                    self.loadMessages();
                });

                self.messengerBlock.on('mouseenter', function () {
                    $(this).addClass('active');
                }).on('mouseleave', function () {
                    $(this).removeClass('active');
                });

                self.messengerTextForm.on('submit', function (event) {
                    event = event || window.event;

                    if (self.messengerTextInput.val().length !== 0) {
                        var messengerFormAction = $(this).attr('action'),
                            messengerFormData = $(this).serialize();

                        self.messengerTextInput.val('');

                        $.when($.ajax({
                            url: messengerFormAction,
                            data: messengerFormData,
                            method: 'post',
                            dataType: 'json'
                        })).done(function (responseJSON) {
                            if (responseJSON.success) {
                                if (responseJSON.success.status) {
                                    if (!responseJSON.success.command) {
                                        self.botSockets[index].emit('messageAdditionRequest');
                                    } else {
                                        self.showAlert('success', responseJSON.success.text);
                                    }
                                }
                            } else if (responseJSON.errors) {
                                if (responseJSON.errors.status) {
                                    self.showAlert('error', responseJSON.errors.text);
                                }
                            }
                        });
                    }

                    event.preventDefault();
                });

                self.messengerTextInput.on('keydown', function (event) {
                    event = event || window.event;

                    if (event.which === 13) {
                        if (self.messengerTextInput.val().length !== 0) {
                            self.messengerTextForm.trigger('submit');
                        }
                        event.preventDefault();
                    }
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.withdrawEvent = function (index) {
            (function (self) {
                self.withdrawInventoryBlock.on('submit', 'form', function (event) {
                    event = event || window.event;

                    self.showAlert('info', 'Withdraw in progress..');

                    var eventTarget = $(event.target),
                        withdrawInventoryFormData = eventTarget.serialize(),
                        withdrawInventoryActionForm = eventTarget.attr('action');

                    self.inventorySelectedCountBlock.text(0);
                    self.inventorySelectedPriceBlock.text(0);

                    var rowCheckboxes = self.inventoryBlock.find('[data-select]').find('input[type="checkbox"]');
                    rowCheckboxes.prop('checked', false);

                    self.inventoryBlock.find('[data-select]').find('[data-select-image]').removeClass('selected');

                    $.when($.ajax({
                        url: withdrawInventoryActionForm,
                        data: withdrawInventoryFormData,
                        method: 'post',
                        dataType: 'json'
                    })).done(function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                if (responseJSON.success.market_names && responseJSON.success.text) {
                                    self.botSockets[index].emit('userWithdrawRequest', {
                                        'userTradeLink': responseJSON.success.text,
                                        'market_names': responseJSON.success.market_names
                                    });
                                }
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.showAlert('error', responseJSON.errors.text);
                            }
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.buyEvent = function (index) {
            (function (self) {
                self.shopInventoryBlock.on('submit', 'form', function (event) {
                    event = event || window.event;

                    self.showAlert('info', 'Buy in progress..');

                    var eventTarget = $(event.target),
                        shopInventoryFormData = eventTarget.serialize(),
                        shopInventoryActionForm = eventTarget.attr('action');

                    self.inventorySelectedCountBlock.text(0);
                    self.inventorySelectedPriceBlock.text(0);

                    var rowCheckboxes = self.inventoryBlock.find('[data-select]').find('input[type="checkbox"]');
                    rowCheckboxes.prop('checked', false);

                    self.inventoryBlock.find('[data-select]').find('[data-select-image]').removeClass('selected');

                    $.when($.ajax({
                        url: shopInventoryActionForm,
                        data: shopInventoryFormData,
                        method: 'post',
                        dataType: 'json'
                    })).done(function (responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                if (responseJSON.success.market_names && responseJSON.success.text) {
                                    self.botSockets[index].emit('userBuyRequest', {
                                        'userTradeLink': responseJSON.success.text,
                                        'market_names': responseJSON.success.market_names
                                    });
                                }
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.showAlert('error', responseJSON.errors.text);
                            }
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @param data
         * @param wonPath
         * @returns {CSGO}
         */
        this.saveWon = function (data, wonPath) {
            $.when($.ajax({
                url: wonPath,
                data: {
                    after_id: data.afterID,
                    before_id: data.beforeID,
                    trade_id: data.tradeID,
                    deposit_id: data.depositID
                },
                method: 'post',
                dataType: 'json'
            })).done(function (response) {});

            return this;
        };

        /**
         * @param type
         * @param message
         * @returns {CSGO}
         */
        this.showAlert = function (type, message) {
            var content = $('<div class="message-content"></div>');

            if (type === 'error') {
                content.addClass('danger');
            } else if (type === 'success') {
                content.addClass('success');
            } else if (type === 'info') {
                content.addClass('info');
            }

            var slideDuration = $(self.botAlert).data('duration') || 200;

            content.html(message);

            this.botAlert.append(content);

            if (!this.botAlert.is(':visible')) {
                this.botAlert.slideDown(slideDuration);
            }

            (function (self) {
                clearTimeout(self.timeout);
                self.timeout = setTimeout(function () {
                    self.botAlert.slideUp(slideDuration, function () {
                        self.botAlert.html('');
                    });
                }, 15000);
            })(this);

            return this;
        };

        /**
         * @param index
         * @returns {CSGO}
         */
        this.messageEvent = function (index) {
            (function (self) {
                self.botSockets[index].on('botMessage', function (data) {
                    self.showAlert(data.type, data.message);
                });
            })(this);
            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.inventoryFormCheckboxesEvent = function () {
            (function (self) {
                self.inventoryBlock.on('click', '[data-select]', function (event) {
                    var rowCheckboxes = $(this).find('input[type="checkbox"]'),
                        rowAvatarBlock = $(this).find('[data-select-image]');

                    if (!$(this).hasClass('unavailable')) {
                        rowCheckboxes.each(function () {
                            if (!$(this).is(':checked')) {
                                rowAvatarBlock.addClass('selected');
                                $(this).prop('checked', true);
                            } else {
                                rowAvatarBlock.removeClass('selected');
                                $(this).prop('checked', false);
                            }
                        });

                        var rowAvatarSelectedBlockLength = self.inventoryBlock.find('.selected[data-select-image]').length;
                        self.inventorySelectedCountBlock.text(rowAvatarSelectedBlockLength);

                        var rowAvatarSelectedBlockPrices = self.inventoryBlock.find('.selected[data-select-image]').find('[data-price]');
                        var totalPrice = 0;

                        if (rowAvatarSelectedBlockLength > 0) {
                            $.each(rowAvatarSelectedBlockPrices, function() {
                                totalPrice = totalPrice + parseFloat($(this).attr('data-price'));
                                self.inventorySelectedPriceBlock.text(totalPrice.toFixed(2));
                            });
                        } else {
                            totalPrice = 0;
                            self.inventorySelectedPriceBlock.text(totalPrice);
                        }
                    }
                });
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.inventorySortFormEvent = function () {
            (function (self) {
                self.reloadInventoryButton.on('click', function (event) {
                    event = event || window.event;

                    self.sortForm.trigger('submit');

                    event.preventDefault();
                });

                self.sortInput.on('change', function () {
                    self.sortForm.trigger('submit');
                });

                self.sortForm.on('submit', function (event) {
                    event = event || window.event;

                    var sortData = $(this).serialize();

                    for (var index in self.connected) {
                        self.depositLoad('', sortData, index, false);
                        break;
                    }

                    event.preventDefault();
                });

                self.searchInput.on('keyup', function () {
                    var inputValue = $(this).val(),
                        inputValueLength = inputValue.length;

                    if (inputValueLength > 2) {
                        self.sortForm.trigger('submit');
                    }

                    if (inputValueLength <= 0) {
                        self.sortForm.trigger('submit');
                    }
                });

            })(this);

            return this;
        };


        /**
         * @returns {CSGO}
         */
        this.chatboxFeature = function () {
            $('.chat-post').mouseover(function () {
                $(this).addClass('chat-setting');
            }).mouseout(function () {
                $(this).removeClass('chat-setting').delay(400);
            });

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.profileEvents = function () {
            (function (self) {
                self.profileBlock.on('mouseenter', function (event) {
                    event = event || window.event;

                    if (!self.profileDropdown.is(':visible')) {
                        $(this).addClass('active');
                        self.profileDropdown.slideDown(200);
                    } else {
                        $(this).removeClass('active');
                        self.profileDropdown.slideUp(200);
                    }

                    event.preventDefault();
                }).on('mouseleave', function () {
                    $(this).removeClass('active');
                    self.profileDropdown.slideUp(200);
                });
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.supportFormEvents = function () {
            (function (self) {
                self.supportForm.on('submit', function (event) {
                    event = event || window.event;

                    if (self.formValidate() === 0) {
                        var supportFormPath = $(this).attr('action'),
                            data = $(this).serialize(),
                            validateFormInputs = self.validateForm.find('input[data-input], textarea[data-input]');

                        validateFormInputs.val('');

                        $.when($.ajax({
                            url: supportFormPath,
                            method: 'post',
                            dataType: 'json',
                            data: data
                        })).done(function(responseJSON) {
                            if (responseJSON.success) {
                                if (responseJSON.success.status) {
                                    self.showAlert('success', responseJSON.success.text);
                                }
                            } else if (responseJSON.errors) {
                                if (responseJSON.errors.status) {
                                    self.showAlert('error', responseJSON.errors.text);
                                }
                            }
                        });
                    }

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.referralFormEvents = function () {
            (function (self) {
                self.createReferralCodeForm.on('submit', function (event) {
                    event = event || window.event;

                    var createReferralCodePath = $(this).attr('action'),
                        data = $(this).serialize();

                    self.createReferralInput.val('');

                    $.when($.ajax({
                        url: createReferralCodePath,
                        method: 'post',
                        dataType: 'json',
                        data: data
                    })).done(function(responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.showAlert('success', responseJSON.success.text);
                                self.referralCode.text(responseJSON.success.referral);
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.showAlert('error', responseJSON.errors.text);
                            }
                        }
                    });

                    event.preventDefault();
                });

                self.useReferralCodeForm.on('submit', function (event) {
                    event = event || window.event;

                    var useReferralCodePath = $(this).attr('action'),
                        data = $(this).serialize();

                    self.useReferralInput.val('');

                    $.when($.ajax({
                        url: useReferralCodePath,
                        method: 'post',
                        dataType: 'json',
                        data: data
                    })).done(function(responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.showAlert('success', responseJSON.success.text);
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.showAlert('error', responseJSON.errors.text);
                            }
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @returns {number}
         */
        this.formValidate = function () {
            var validateFormInputs = this.validateForm.find('input[data-input], textarea[data-input]'),
                errorsLength = 0;

            validateFormInputs.each(function () {
                var inputRequired = $(this).attr('data-required') || false,
                    inputValue = $(this).val(),
                    inputMatch = $(this).attr('data-match');

                $(this).removeClass('failed').siblings('.form-message').html('');

                if (inputRequired !== false) {
                    if (inputValue.length === 0) {
                        $(this).addClass('failed').siblings('.form-message').html('Fill required field!');
                        errorsLength++;
                    }
                }

                if (inputMatch !== false) {
                    var regex = new RegExp(inputMatch);

                    if (!regex.test(inputValue)) {
                        $(this).addClass('failed').siblings('.form-message').html('Incorrect data!');
                        errorsLength++;
                    }
                }
            });

            return errorsLength;
        };

        /**
         * @returns {CSGO}
         */
        this.particles = function () {

            (function (self) {
                var center = {
                    x: self.afterItemBlock.width() / 2,
                    y: self.afterItemBlock.height() / 2
                }, particles = [];

                function create () {
                    for (var i = 0; i < 40; i++) {
                        var element = $('<div class="particle"></div>');
                        self.afterItemBlock.append(element);
                        particles.push(element);
                    }
                }

                function spray (element) {
                    var xVariance = Math.random() * 40 * (Math.random() < 0.5 ? -1 : 1),
                        yVariance = Math.random() * 5 * (Math.random() < 0.5 ? -1 : 1),
                        scale = Math.random();

                    element.css({
                        opacity: 0,
                        transform: 'scale(' + scale + ',' + scale + ')'
                    }).delay(1000 * Math.random()).animate({
                        top: center.y + yVariance,
                        left: center.x + xVariance,
                        opacity: 0.5
                    }, 0).animate({
                        top: Math.random() * self.afterItemBlock.height(),
                        left: Math.random() * self.afterItemBlock.width(),
                        opacity: 0
                    }, {
                        duration: 800,
                        complete: function () {
                            spray(element);
                        }, ease: 'ease-out'
                    });
                }

                function start () {
                    create();
                    for (var i = 0; i < particles.length; i++) {
                        spray(particles[i]);
                    }
                }

                start();
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.slimscroll = function()
        {
            (function() {
                var slimScroll = $('.slimscroll');

                slimScroll.each(function() {
                    var slimScrollHeight = $(this).height();

                    $(this).slimScroll({
                        height: slimScrollHeight,
                        color: 'rgba(255, 255, 255, .3)',
                        size: '6px',
                        railVisible: true,
                        alwaysVisible: true,
                        allowPageScroll: true,
                        wheelStep: 5
                    });
                });
            })();

            return this;
        };

        /**
         *
         * @returns {CSGO}
         */
        this.justiceEvent = function() {

            (function(self) {
                self.justiceForm.on('submit', function (event) {
                    event = event || window.event;

                    var justiceFormPath = $(this).attr('action'),
                        data = $(this).serialize();

                    self.justiceFormAlert.hide(0);

                    $.when($.ajax({
                        url: justiceFormPath,
                        method: 'post',
                        dataType: 'json',
                        data: data
                    })).done(function(responseJSON) {
                        if (responseJSON.success) {
                            if (responseJSON.success.status) {
                                self.justiceFormAlert.removeClass('error').addClass('success')
                                    .html(responseJSON.success.text).slideDown(500);
                            }
                        } else if (responseJSON.errors) {
                            if (responseJSON.errors.status) {
                                self.justiceFormAlert.removeClass('success').addClass('error')
                                    .html(responseJSON.errors.text).slideDown(500);
                            }
                        }
                    });

                    event.preventDefault();
                });
            })(this);

            return this;
        };

        /**
         * @returns {CSGO}
         */
        this.init = function () {

            (function(self) {
                $.get('/iosocket.php').done(function(data) {
                    var bots = window.atob(data);
                    var botConfigJSON = JSON.parse(bots);

                    self.socketConfiguration(botConfigJSON);
                    self.countUp(0, self.countupBlockValue, 'today');

                    $('select').selectric();

                    self.slimscroll();

                    if (!localStorage.getItem('sounds')) {
                        localStorage.setItem('sounds', 'on');
                    } else {
                        if (localStorage.getItem('sounds') === 'on') {
                            self.soundsOff.show(0);
                            self.soundsOn.hide(0);

                            localStorage.setItem('sounds', 'off');
                        } else if (localStorage.getItem('sounds') === 'off') {
                            self.soundsOff.hide(0);
                            self.soundsOn.show(0);
                        }
                    }

                    self.sounds.on('click', function () {
                        if (localStorage.getItem('sounds') === 'on') {
                            document.getElementById('game-audio').volume = 0;
                            document.getElementById('win-audio').volume = 0;
                            document.getElementById('lose-audio').volume = 0;
                            document.getElementById('count-audio').volume = 0;

                            self.soundsOff.hide(0);
                            self.soundsOn.show(0);

                            localStorage.setItem('sounds', 'off');
                        } else if (localStorage.getItem('sounds') === 'off') {
                            document.getElementById('game-audio').volume = 1;
                            document.getElementById('win-audio').volume = 1;
                            document.getElementById('lose-audio').volume = 1;
                            document.getElementById('count-audio').volume = 1;

                            self.soundsOff.show(0);
                            self.soundsOn.hide(0);

                            localStorage.setItem('sounds', 'on');
                        }
                    });
                });
            })(this);

            return this;
        };
    };

    $(document).ready(function() {
        new CSGO().init();
    });

})(jQuery);