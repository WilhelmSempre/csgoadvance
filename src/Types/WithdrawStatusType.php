<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class WithdrawStatusType
 * @package CSGOADVANCE\src\Types
 */
class WithdrawStatusType
{

    /**
     *
     */
    const UNPAID = 0;

    /**
     *
     */
    const PENDING = 1;

    /**
     *
     */
    const PAID = 2;

    /**
     * @var array
     */
    protected static $select = [
        self::UNPAID => 'Niewypłacone',
        self::PENDING => 'Oczekujący',
        self::PAID => 'Wypłacone'
    ];
}
