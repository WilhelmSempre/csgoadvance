<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class GameStatusType
 * @package CSGOADVANCE\src\Types
 */
class GameStatusType
{
    /**
     *
     */
    const LOSE = 0;

    /**
     *
     */
    const WIN = 1;

    /**
     * @var array
     */
    protected static $select = [
        self::LOSE => 'Przegrane',
        self::WIN => 'Wygrane'
    ];
}
