<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class UserRoleType
 * @package CSGOADVANCE\src\Types
 */
class UserRoleType
{
    /**
     *
     */
    const ADMINISTRATOR = 0;

    /**
     *
     */
    const USER = 1;

    /**
     *
     */
    const MODERATOR = 2;

    /**
     *
     */
    const BANNED = 3;

    /**
     * @var array
     */
    protected static $select = [
        self::ADMINISTRATOR => 'Administrator',
        self::USER => 'Użytkownik',
        self::MODERATOR => 'Moderator',
        self::BANNED => 'Zbanowany'
    ];
}
