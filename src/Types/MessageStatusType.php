<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class MessageStatusType
 * @package CSGOADVANCE\src\Types
 */
class MessageStatusType
{
    /**
     *
     */
    const ACTIVE = 0;

    /**
     *
     */
    const ARCHIVED = 1;

    /**
     * @var array
     */
    protected static $select = [
        self::ACTIVE => 'Aktywne',
        self::ARCHIVED => 'Zarchiwizowane'
    ];
}
