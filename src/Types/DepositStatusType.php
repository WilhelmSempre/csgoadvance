<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class DepositStatusType
 * @package CSGOADVANCE\src\Types
 */
class DepositStatusType
{
    /**
     *
     */
    const ACTIVE = 0;

    /**
     *
     */
    const UNACTIVE = 1;

    /**
     *
     */
    const PENDING = 2;

    /**
     * @var array
     */
    protected static $select = [
        self::ACTIVE => 'Aktywna',
        self::UNACTIVE => 'Nieaktywny',
        self::PENDING => 'Oczekujący'
    ];
}
