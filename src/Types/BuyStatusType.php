<?php

namespace CSGOADVANCE\src\Types;

/**
 * Class BuyStatusType
 * @package CSGOADVANCE\src\Types
 */
class BuyStatusType
{

    /**
     *
     */
    const UNPAID = 0;

    /**
     *
     */
    const PENDING = 1;

    /**
     *
     */
    const PAID = 2;

    /**
     * @var array
     */
    protected static $select = [
        self::UNPAID => 'Niewypłacone',
        self::PENDING => 'Oczekujący',
        self::PAID => 'Wypłacone'
    ];
}
