<?php

namespace CSGOADVANCE\src\Repository;

use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\User;

/**
 * Class UserRepository
 * @package CSGOADVANCE\src\Repository
 */
class UserRepository extends EntityRepository
{

    /**
     * @param array $userParams
     * @return User[]|array
     */
    public function getUser($userParams = [])
    {
        $defaultParams = [
            'steamID' => 0,
            'ID' => 0,
            'referral' => ''
        ];

        $userParams = array_merge($defaultParams, $userParams);

        if ($userParams['steamID'] !== 0) {

            /** @var User[] $user */
            $user = $this->findBy([
                'steamid' => $userParams['steamID']
            ]);
        } elseif ($userParams['ID'] !== 0) {

            /** @var User[] $user */
            $user = $this->findBy([
                'id' => $userParams['ID']
            ]);
        } elseif ($userParams['referral'] !== '') {

            /** @var User[] $user */
            $user = $this->findBy([
                'referral' => $userParams['referral']
            ]);
        } else {

            /** @var User[] $user */
            $user = $this->findAll();
        }

        return $user;
    }


    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $this->_em->merge($user);
        $this->_em->flush();
    }
}
