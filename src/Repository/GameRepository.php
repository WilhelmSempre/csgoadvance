<?php

namespace CSGOADVANCE\src\Repository;

use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\Game;
use CSGOADVANCE\src\Entity\Withdraw;

/**
 * Class GameRepository
 * @package CSGOADVANCE\src\Repository
 */
class GameRepository extends EntityRepository
{

    /**
     * @var
     */
    private $depositWonLastID;

    /**
     * @var
     */
    private $withdrawWonLastID;

    /**
     * @param array $gameParams
     * @param null $limit
     * @return Game[]
     */
    public function getGame($gameParams = [], $limit = null)
    {
        $defaultParams = [
            'ID' => 0,
            'userID' => 0,
            'depositID' => 0,
            'win' => null
        ];

        $gameParams = array_merge($defaultParams, $gameParams);

        if ($gameParams['ID'] !== 0) {

            /** @var Game[] $game */
            $game = $this->findBy([
                'id' => $gameParams['ID']
            ]);
        } elseif ($gameParams['userID'] !== 0) {

            /** @var Game[] $game */
            $game = $this->findBy([
                'user' => $gameParams['userID']
            ]);
        } elseif ($gameParams['depositID'] !== 0) {

            /** @var Game[] $game */
            $game = $this->findBy([
                'deposit' => $gameParams['depositID']
            ]);
        } elseif ($gameParams['win']) {

            /** @var Game[] $game */
            $game = $this->findBy([
                'win' => $gameParams['win']
            ], ['created' => 'DESC'], $limit);
        } else {

            /** @var Game[] $game */
            $game = $this->findAll();
        }

        return $game;
    }

    /**
     * @param Game $game
     */
    public function addGame(Game $game)
    {
        $this->_em->persist($game);
        $this->_em->flush();
    }

    /**
     * @param Deposit $deposit
     */
    public function addWonDeposit(Deposit $deposit)
    {
        $this->_em->persist($deposit);
        $this->_em->flush();

        $this->depositWonLastID = $deposit->getId();
    }

    /**
     * @param Withdraw $withdraw
     */
    public function addWonWithDraw(Withdraw $withdraw)
    {
        $this->_em->persist($withdraw);
        $this->_em->flush();
        $this->withdrawWonLastID = $withdraw->getId();
    }

    /**
     * @return bool|int
     */
    public function getDepositWonLastID()
    {
        return $this->depositWonLastID ? $this->depositWonLastID : false;
    }

    /**
     * @return bool|int
     */
    public function getWithdrawWonLastID()
    {
        return $this->withdrawWonLastID ? $this->withdrawWonLastID : false;
    }

    /**
     * @param $wonDepositID
     * @return Deposit
     */
    public function getWonDeposit($wonDepositID)
    {
        return $this->_em->getRepository('CSGOADVANCE\src\Entity\Deposit')->findBy([
                'id' => $wonDepositID
            ])[0];
    }

    /**
     * @param $wonWithdrawID
     * @return Withdraw
     */
    public function getWonWithdraw($wonWithdrawID)
    {
        return $this->_em->getRepository('CSGOADVANCE\src\Entity\Withdraw')->findBy([
                'id' => $wonWithdrawID
            ])[0];
    }

    /**
     * @param Deposit $deposit
     */
    public function updateWonDeposit(Deposit $deposit)
    {
        $this->_em->merge($deposit);
        $this->_em->flush();
    }

    /**
     * @param Withdraw $withdraw
     */
    public function updateWonWithdraw(Withdraw $withdraw)
    {
        $this->_em->merge($withdraw);
        $this->_em->flush();
    }
}
