<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Entity\Message;
use Doctrine\ORM\EntityRepository;

/**
 * Class MessageRepository
 * @package CSGOADVANCE\src\Repository
 */
class MessageRepository extends EntityRepository
{

    /**
     * @param Message $message
     */
    public function addMessage(Message $message)
    {
        $this->_em->persist($message);
        $this->_em->flush();
    }

    /**
     * @param array $messageParams
     * @return array|Message[]
     */
    public function getMessage($messageParams = [])
    {
        $defaultParams = [
            'userID' => 0,
            'ID' => 0
        ];

        $messageParams = array_merge($defaultParams, $messageParams);

        if ($messageParams['userID'] !== 0) {

            /** @var Message[] $message */
            $message = $this->findBy([
                'user' => $messageParams['userID']
            ]);
        } elseif ($messageParams['ID'] !== 0) {

            /** @var Message[] $message */
            $message = $this->findBy([
                'id' => $messageParams['ID']
            ]);
        } else {

            /** @var Message[] $message */
            $message = array_reverse($this->findBy([], ['created' => 'DESC'], 20));
        }

        return $message;
    }
}
