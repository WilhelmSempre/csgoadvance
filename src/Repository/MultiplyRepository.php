<?php

namespace CSGOADVANCE\src\Repository;

use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\Multiply;

/**
 * Class MultiplyRepository
 * @package CSGOADVANCE\src\Repository
 */
class MultiplyRepository extends EntityRepository
{

    /**
     * @param array $multiplyParams
     * @return array|Multiply[]
     */
    public function getMultiply($multiplyParams = [])
    {
        $defaultParams = [
            'userID' => 0,
            'ID' => 0
        ];

        $multiplyParams = array_merge($defaultParams, $multiplyParams);

        if ($multiplyParams['userID'] !== 0) {

            /** @var Multiply[] $multiply */
            $multiply = $this->findBy([
                'user' => $multiplyParams['userID']
            ]);
        } elseif ($multiplyParams['ID'] !== 0) {

            /** @var Multiply[] $multiply */
            $multiply = $this->findBy([
                'id' => $multiplyParams['ID']
            ]);
        } else {

            /** @var Multiply[] $multiply */
            $multiply = $this->findAll();
        }

        return $multiply;
    }


    /**
     * @param Multiply $multiply
     */
    public function addMultiply(Multiply $multiply)
    {
        $this->_em->persist($multiply);
        $this->_em->flush();
    }

    /**
     * @param Multiply $multiply
     */
    public function deleteMultiply(Multiply $multiply)
    {
        $this->_em->remove($multiply);
        $this->_em->flush();
    }
}
