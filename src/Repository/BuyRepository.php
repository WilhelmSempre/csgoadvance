<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Entity\Buy;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Types\BuyStatusType;
use Doctrine\ORM\EntityRepository;

/**
 * Class BuyRepository
 * @package CSGOADVANCE\src\Repository
 */
class BuyRepository extends EntityRepository
{
    /**
     * @param Buy $buy
     */
    public function addBuy(Buy $buy)
    {
        $this->_em->persist($buy);
        $this->_em->flush();
    }

    /**
     * @param Buy $buy
     */
    public function removeBuy(Buy $buy)
    {
        $this->_em->remove($buy);
        $this->_em->flush();
    }

    /**
     * @param Buy $buy
     */
    public function updateBuy(Buy $buy)
    {
        $this->_em->merge($buy);
        $this->_em->flush();
    }

    /**
     * @param array $buyParams
     * @return array|Buy[]
     */
    public function getBuys($buyParams = [])
    {
        $defaultParams = [
            'ID' => 0,
            'userID' => 0
        ];

        $buyParams = array_merge($defaultParams, $buyParams);

        if ($buyParams['ID'] !== 0) {

            /** @var Buy[] $buys */
            $buys = $this->findBy([
                'id' => $buyParams['ID']
            ]);
        } elseif ($buyParams['userID'] !== 0) {

            /** @var Buy[] $buys */
            $buys = $this->findBy([
                'user' => $buyParams['userID']
            ]);
        } else {

            /** @var Buy[] $buys */
            $buys = $this->findAll();
        }

        return $buys;
    }

    /**
     * @param $user
     * @return Buy[]
     */
    public function getPendingBuys($user)
    {
        $buys = $this->findBy([
            'user' => $user,
            'status' => BuyStatusType::PENDING
        ]);

        return $buys;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isPendingBuy(User $user)
    {
        $buy = $this->getPendingBuys($user);

        return count($buy) > 0;
    }
}