<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Types\DepositStatusType;
use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\Deposit;
use CSGOADVANCE\src\Entity\User;

/**
 * Class DepositRepository
 * @package CSGOADVANCE\src\Repository
 */
class DepositRepository extends EntityRepository
{

    /**
     * @var null
     */
    private $lastID = null;

    /**
     * @param array $depositParams
     * @return array
     */
    public function getDeposit($depositParams = [])
    {
        $defaultParams = [
            'ID' => 0,
            'userID' => 0,
            'tradeID' => 0,
            'itemID' => 0,
            'status' => 0
        ];

        $depositParams = array_merge($defaultParams, $depositParams);

        if ($depositParams['ID'] !== 0) {

            /** @var Deposit[] $deposit */
            $deposit = $this->findBy([
                'id' => $depositParams['ID']
            ]);
        } elseif ($depositParams['userID'] !== 0) {

            /** @var Deposit[] $deposit */
            $deposit = $this->findBy([
                'user' => $depositParams['userID']
            ]);
        } elseif ($depositParams['tradeID'] !== 0) {

            /** @var Deposit[] $deposit */
            $deposit = $this->findBy([
                'trade' => $depositParams['tradeID']
            ]);
        } elseif ($depositParams['itemID'] !== 0) {

            /** @var Deposit[] $deposit */
            $deposit = $this->findBy([
                'item' => $depositParams['itemID']
            ]);
        } else {

            /** @var Deposit[] $deposit */
            $deposit = $this->findAll();
        }

        return $deposit;
    }

    /**
     * @param Deposit $deposit
     */
    public function addDeposit(Deposit $deposit)
    {
        $this->_em->persist($deposit);
        $this->_em->flush();
        $this->lastID = $deposit->getId();
    }

    /**
     * @param Deposit $deposit
     */
    public function deleteDeposit(Deposit $deposit)
    {
        $this->_em->remove($deposit);
        $this->_em->flush();
    }

    /**
     * @param Deposit $deposit
     */
    public function updateDeposit(Deposit $deposit)
    {
        $this->_em->merge($deposit);
        $this->_em->flush();
    }

    /**
     * @return bool
     */
    public function getDepositLastID()
    {
        return $this->lastID ? $this->lastID : false;
    }

    /**
     * @param $user
     * @return Deposit[]
     */
    public function getPendingDeposits(User $user)
    {
        $deposits = $this->findBy([
            'user' => $user,
            'status' => DepositStatusType::PENDING
        ]);

        return $deposits;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isPendingDeposit(User $user)
    {
        $deposit = $this->getPendingDeposits($user);

        return count($deposit) > 0;
    }
}
