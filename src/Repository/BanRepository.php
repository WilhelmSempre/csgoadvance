<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Entity\Ban;
use Doctrine\ORM\EntityRepository;

/**
 * Class BanRepository
 * @package CSGOADVANCE\src\Repository
 */
class BanRepository extends EntityRepository
{

    /**
     * @param Ban $ban
     */
    public function addBan(Ban $ban)
    {
        $this->_em->persist($ban);
        $this->_em->flush();
    }

    /**
     * @param Ban $ban
     */
    public function removeBan(Ban $ban)
    {
        $this->_em->remove($ban);
        $this->_em->flush();
    }

    /**
     * @param array $banParams
     * @return array|Ban[]
     */
    public function getBans($banParams = [])
    {
        $defaultParams = [
            'ID' => 0,
            'userID' => 0
        ];

        $banParams = array_merge($defaultParams, $banParams);

        if ($banParams['ID'] !== 0) {

            /** @var Ban[] $bans */
            $bans = $this->findBy([
                'id' => $banParams['ID']
            ]);
        } elseif ($banParams['userID'] !== 0) {

            /** @var Ban[] $bans */
            $bans = $this->findBy([
                'user' => $banParams['userID']
            ]);
        } else {

            /** @var Ban[] $bans */
            $bans = $this->findAll();
        }

        return $bans;
    }
}
