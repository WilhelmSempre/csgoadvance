<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Entity\Affiliates;
use Doctrine\ORM\EntityRepository;

/**
 * Class AffiliatesRepository
 * @package CSGOADVANCE\src\Repository
 */
class AffiliatesRepository extends EntityRepository
{

    /**
     * @param Affiliates $affiliates
     */
    public function addAffiliate(Affiliates $affiliates)
    {
        $this->_em->persist($affiliates);
        $this->_em->flush();
    }

    /**
     * @param array $affiliatesParams
     * @return array|Affiliates[]
     */
    public function getAffiliates($affiliatesParams = [])
    {
        $defaultParams = [
            'ID' => 0,
            'userID' => 0,
            'affiliatedUserID' => 0
        ];

        $affiliatesParams = array_merge($defaultParams, $affiliatesParams);

        if ($affiliatesParams['ID'] !== 0) {

            /** @var Affiliates[] $affiliates */
            $affiliates = $this->findBy([
                'id' => $affiliatesParams['ID']
            ]);
        } elseif ($affiliatesParams['userID'] !== 0) {

            /** @var Affiliates[] $affiliates */
            $affiliates = $this->findBy([
                'user' => $affiliatesParams['userID']
            ]);
        } elseif ($affiliatesParams['affiliatedUserID'] !== 0) {

            /** @var Affiliates[] $affiliates */
            $affiliates = $this->findBy([
                'affilatedUser' => $affiliatesParams['affiliatedUserID']
            ]);
        } else {

            /** @var Affiliates[] $affiliates */
            $affiliates = $this->findAll();
        }
        
        return $affiliates;
    }
}
