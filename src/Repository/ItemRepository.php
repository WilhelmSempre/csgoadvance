<?php

namespace CSGOADVANCE\src\Repository;

use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\Item;

/**
 * Class ItemRepository
 * @package CSGOADVANCE\src\Repository
 */
class ItemRepository extends EntityRepository
{

    /**
     * @param array $itemParams
     * @return Item[]|array
     */
    public function getItem($itemParams = [])
    {
        $defaultParams = [
            'market_name' => '',
            'ID' => 0
        ];

        $itemParams = array_merge($defaultParams, $itemParams);

        if ($itemParams['market_name'] !== '') {

            /** @var Item[] $item */
            $item = $this->findBy([
                'marketName' => $itemParams['market_name']
            ]);
        } elseif ($itemParams['ID'] !== 0) {

            /** @var Item[] $item */
            $item = $this->findBy([
                'id' => $itemParams['ID']
            ]);
        } else {

            /** @var Item[] $item */
            $item = $this->findAll();
        }

        return $item;
    }


    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->_em->persist($item);
        $this->_em->flush();
    }

    /**
     * @param $price
     * @param $offset
     * @param $itemID
     * @return array
     */
    public function getItemsByPrice($price, $offset, $itemID = null)
    {
        $query = $this->_em->createQueryBuilder();

        $query->select('i')->from('CSGOADVANCE\src\Entity\Item', 'i');

        if ($offset === 0.0) {
            $query->where(
                $query->expr()->eq('i.price', $price)
            );
        } else {
            $offset = $price + $offset;

            $query->where(
                $query->expr()->between('i.price', $price, $offset)
            );
        }

        if ($itemID) {
            $query->andWhere(
                $query->expr()->not(
                    $query->expr()->eq('i.id', $itemID)
                )
            );
        }

        $query = $query->getQuery();

        return $query->getResult();
    }
}
