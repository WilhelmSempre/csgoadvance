<?php

namespace CSGOADVANCE\src\Repository;

use CSGOADVANCE\src\Types\WithdrawStatusType;
use Doctrine\ORM\EntityRepository;
use CSGOADVANCE\src\Entity\Item;
use CSGOADVANCE\src\Entity\User;
use CSGOADVANCE\src\Entity\Withdraw;

/**
 * Class WithdrawRepository
 * @package CSGOADVANCE\src\Repository
 */
class WithdrawRepository extends EntityRepository
{

    /**
     * @param array $withdrawParams
     * @return Withdraw[]|array
     */
    public function getWithdraw($withdrawParams = [])
    {
        $defaultParams = [
            'gameID' => 0,
            'userID' => 0,
            'itemID' => 0,
            'depositID' => 0,
            'tradeID' => 0,
            'ID' => 0,
            'status' => -1
        ];

        $withdrawParams = array_merge($defaultParams, $withdrawParams);

        if ($withdrawParams['ID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'id' => $withdrawParams['ID']
            ]);
        } elseif ($withdrawParams['gameID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'game' => $withdrawParams['gameID']
            ]);
        } elseif ($withdrawParams['depositID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'deposit' => $withdrawParams['depositID']
            ]);
        } elseif ($withdrawParams['userID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'user' => $withdrawParams['userID']
            ]);
        } elseif ($withdrawParams['itemID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'item' => $withdrawParams['itemID']
            ]);
        } elseif ($withdrawParams['tradeID'] !== 0) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'tradeID' => $withdrawParams['tradeID']
            ]);
        } elseif ($withdrawParams['status'] !== -1) {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findBy([
                'status' => $withdrawParams['status']
            ]);
        } else {

            /** @var Withdraw[] $withdraw */
            $withdraw = $this->findAll();
        }

        return $withdraw;
    }

    /**
     * @param Withdraw $withdraw
     */
    public function addWithdraw(Withdraw $withdraw)
    {
        $this->_em->persist($withdraw);
        $this->_em->flush();
    }

    /**
     * @param Withdraw $withdraw
     */
    public function updateWithdraw(Withdraw $withdraw)
    {
        $this->_em->merge($withdraw);
        $this->_em->flush();
    }

    /**
     * @param User $user
     * @param Item $item
     * @return bool
     */
    public function isWithdraw(User $user, Item $item)
    {
        $withdraw = $this->findBy([
            'user' => $user,
            'item' => $item
        ]);

        return count($withdraw) > 0;
    }

    /**
     * @param $user
     * @return Withdraw[]
     */
    public function getPendingWithdraws($user)
    {
        $withdraws = $this->findBy([
            'user' => $user,
            'status' => WithdrawStatusType::PENDING
        ]);

        return $withdraws;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isPendingWithdraw(User $user)
    {
        $withdraw = $this->getPendingWithdraws($user);

        return count($withdraw) > 0;
    }
}
